package visitorPickers.student;

import java.util.HashMap;

import utilities.DataStore_student;
import visitables.IVisitable;
import visitables.student.Visitable_Avg_student;
import visitorPickers.IVisitorPicker;
import visitorPickers.VisitorPickerFactory;

public class VisitorPick_Avg_student implements IVisitorPicker {

	
	private  VisitorPickerFactory visitorPickerFactory;
	
	DataStore_student dataStoreStudent;
	
	public boolean debugPrint=false;
	
	public VisitorPick_Avg_student( VisitorPickerFactory visitorPickerFactory , DataStore_student dataStoreStudent ) {
		this.visitorPickerFactory = visitorPickerFactory;
		
		this.dataStoreStudent=dataStoreStudent;
	}

	@Override
	public void chooseVisitor(String measureId  , HashMap<String, String> request) {

		if( debugPrint)
			System.out.println(this.getClass().getName());
		
		IVisitable visitable;		
		visitable =  new Visitable_Avg_student( visitorPickerFactory ,dataStoreStudent  );
		
		visitable.handleRequest( measureId, request );
		
	}

}
