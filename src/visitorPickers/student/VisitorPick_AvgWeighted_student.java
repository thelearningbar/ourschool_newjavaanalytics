package visitorPickers.student;

import java.util.HashMap;

import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitables.student.Visitable_AvgWeighted_student;
import visitorPickers.IVisitorPicker;
import visitorPickers.VisitorPickerFactory;

public class VisitorPick_AvgWeighted_student implements IVisitorPicker {
	
	private  VisitorPickerFactory visitorPickerFactory;
	
	


	DataStore_school dataStoreSchool;
	DataStore_student dataStoreStudent;
	
	
	public boolean debugPrint=false;
	
	
	public VisitorPick_AvgWeighted_student( VisitorPickerFactory visitorPickerFactory ,
			DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool ) {
		
		this.visitorPickerFactory = visitorPickerFactory;
		
		

		this.dataStoreStudent=dataStoreStudent;
		this.dataStoreSchool=dataStoreSchool;
	}

	@Override
	public void chooseVisitor(String measureId  , HashMap<String, String> request) {

		if(debugPrint)
			System.out.println(this.getClass().getName());
		
		IVisitable visitable;		
		visitable =  new Visitable_AvgWeighted_student( visitorPickerFactory , dataStoreStudent , dataStoreSchool );
		
		visitable.handleRequest( measureId, request );
		
	}

}
