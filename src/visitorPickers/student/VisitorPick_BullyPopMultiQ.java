package visitorPickers.student;

import java.sql.Connection;
import java.util.HashMap;

import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitables.student.bullying.Visitable_BullyPopMulti_student;
import visitorPickers.IVisitorPicker;
import visitorPickers.VisitorPickerFactory;

public class VisitorPick_BullyPopMultiQ implements IVisitorPicker {



	private VisitorPickerFactory visitorPickerFactory;
	private DataStore_school dataStoreSchool;
	private DataStore_student dataStoreStudent;
	private Connection conn;
	public boolean debugPrint=false;
	
	
	
	public VisitorPick_BullyPopMultiQ( VisitorPickerFactory visitorPickerFactory ,  
			Connection conn , DataStore_student dataStoreStudent ,DataStore_school dataStoreSchool ) {


		this.visitorPickerFactory = visitorPickerFactory;
		this.dataStoreSchool=dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
		
	}
	
	
	@Override
	public void chooseVisitor(String measureId, HashMap<String, String> request) {

		if( debugPrint)
			System.out.println(this.getClass().getName());
		
		IVisitable visitable;		
		
		visitable = new Visitable_BullyPopMulti_student(visitorPickerFactory, conn, dataStoreStudent, dataStoreSchool);
		visitable.handleRequest( measureId, request );
		

	}

	
	
	
}
