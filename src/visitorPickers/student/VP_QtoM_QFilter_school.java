package visitorPickers.student;

import java.sql.Connection;
import java.util.HashMap;

import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitables.student.bullying.V_QtoM_QFilter_school;
import visitorPickers.IVisitorPicker;
import visitorPickers.VisitorPickerFactory;

/**
 * 
 * 
 * See also
 * V_QtoM_QFilter
 * Quest_to_Measure_QFilter
 * 
 * 
 * @author Evan K
 *
 */
public class VP_QtoM_QFilter_school implements IVisitorPicker {


	private VisitorPickerFactory visitorPickerFactory;
	private DataStore_student dataStoreStudent;
	private DataStore_school dataStoreSchool;
	private Connection conn;
	
	
	public VP_QtoM_QFilter_school(VisitorPickerFactory visitorPickerFactory ,  
			Connection conn , DataStore_student dataStoreStudent ,DataStore_school dataStoreSchool) {


		this.visitorPickerFactory = visitorPickerFactory;
		this.dataStoreSchool=dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
		
	}

	@Override
	public void chooseVisitor(String measureId, HashMap<String, String> request) {

		IVisitable visitable;
		visitable = new V_QtoM_QFilter_school(visitorPickerFactory, conn, dataStoreStudent, dataStoreSchool);
		visitable.handleRequest( measureId, request  );

	}

}
