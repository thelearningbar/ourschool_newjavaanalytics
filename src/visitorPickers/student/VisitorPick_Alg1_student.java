package visitorPickers.student;
import java.sql.Connection;
import java.util.HashMap;

import utilities.DataStore_student;
import visitables.IVisitable;
import visitables.student.Visitable_Alg1_student;
import visitorPickers.IVisitorPicker;


public class VisitorPick_Alg1_student implements IVisitorPicker{
	
	private DataStore_student dataStoreStudent;
	private Connection conn;
	
	
	public VisitorPick_Alg1_student(Connection conn , DataStore_student dataStoreStudent){
		this.conn=conn;
		this.dataStoreStudent=dataStoreStudent;
	}

	@Override
	public void chooseVisitor(String measureId, HashMap<String, String> request) {
		//System.out.println(this.getClass().getName() + "  measureId: " + measureId);
		
		IVisitable visitable;
		visitable = new Visitable_Alg1_student(conn , dataStoreStudent);
		visitable.handleRequest( measureId, request  );
		
		
	}


}