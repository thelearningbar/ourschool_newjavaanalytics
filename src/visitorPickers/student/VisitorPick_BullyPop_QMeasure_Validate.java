package visitorPickers.student;

import java.sql.Connection;
import java.util.HashMap;

import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitables.student.bullying.Visitable_BullyPop_QMeasure_Validate;
import visitorPickers.IVisitorPicker;
import visitorPickers.VisitorPickerFactory;


/**
 * 
 * See also 
 * Visitable_BullyPop_QMeasure_Validate
 * 
 * 
 * @author Evan K
 *
 */
public class VisitorPick_BullyPop_QMeasure_Validate implements IVisitorPicker {


	VisitorPickerFactory visitorPickerFactory;
	DataStore_student dataStoreStudent;
	DataStore_school dataStoreSchool;
	Connection conn;
	
	public VisitorPick_BullyPop_QMeasure_Validate(VisitorPickerFactory visitorPickerFactory ,  
			Connection conn , DataStore_student dataStoreStudent ,DataStore_school dataStoreSchool) {


		this.visitorPickerFactory = visitorPickerFactory;
		this.dataStoreSchool=dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
		
	}

	@Override
	public void chooseVisitor(String measureId, HashMap<String, String> request) {
		
		if( !request.containsKey("Bully-Category")){
			
			if( request.containsKey("schooltype")  ){
				request.put("Bully-Category", request.get("schooltype"));
			}
			else{
				System.out.println("no flags, guessing");
				
				request.put("Bully-Category","1" );
			}
		}
		
		IVisitable visitable;
		visitable = new Visitable_BullyPop_QMeasure_Validate(visitorPickerFactory, conn, dataStoreStudent, dataStoreSchool);
		visitable.handleRequest( measureId, request  );
		
	}

}
