package visitorPickers.student;

import java.sql.Connection;
import java.util.HashMap;

import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitables.student.Visitable_SubmeasuresLADvalueToScore;
import visitorPickers.IVisitorPicker;
import visitorPickers.VisitorPickerFactory;

public class VisitorPick_SubmeasuresLADvalueToScore_student implements IVisitorPicker {


	private DataStore_school dataStoreSchool;
	private DataStore_student dataStoreStudent;
	private Connection conn;
	
	public boolean debugPrint=false;	
	
	
	public VisitorPick_SubmeasuresLADvalueToScore_student(VisitorPickerFactory visitorPickerFactory ,  
			Connection conn , DataStore_student dataStoreStudent ,DataStore_school dataStoreSchool ) {

		this.dataStoreSchool=dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
		
	}

	@Override
	public void chooseVisitor(String measureId, HashMap<String, String> request) {

		if( debugPrint)
			System.out.println(this.getClass().getName());
		
		IVisitable visitable;		
		visitable = new Visitable_SubmeasuresLADvalueToScore(conn, dataStoreStudent, dataStoreSchool);
		visitable.handleRequest( measureId, request );
		
	}

}
