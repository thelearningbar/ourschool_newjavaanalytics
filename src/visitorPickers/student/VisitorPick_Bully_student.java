package visitorPickers.student;

import java.sql.Connection;
import java.util.HashMap;

import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitables.student.bullying.Visitable_Alg21_student;
import visitables.student.bullying.Visitable_Alg7_student;
import visitorPickers.IVisitorPicker;
import visitorPickers.VisitorPickerFactory;



/**
 * Picks either Alg21, or Alg7
 * 
 * @author Evan K
 *
 */
public class VisitorPick_Bully_student implements IVisitorPicker {

	VisitorPickerFactory visitorPickerFactory;
	DataStore_student dataStoreStudent;
	DataStore_school dataStoreSchool;
	Connection conn;
	
	
	public VisitorPick_Bully_student( VisitorPickerFactory visitorPickerFactory ,  
			Connection conn , DataStore_student dataStoreStudent ,DataStore_school dataStoreSchool) {


		this.visitorPickerFactory = visitorPickerFactory;
		this.dataStoreSchool=dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
		
	}
	

	/**
	 * 
	 * Pick visitor for the measure
	 * different algorithm chosen based on schooltype elementary/secondary
	 * 
	 */
	@Override
	public void chooseVisitor(String measureId, HashMap<String, String> parameters) {
		
		IVisitable visitable;
		
		
		if( parameters.containsKey("schooltype") ){
			
			
			String type = parameters.get("schooltype");
			
			if( type.equals("4")){
				//this is an elementary school
				//use elementary algorithm
				
				visitable = new Visitable_Alg21_student(visitorPickerFactory, conn, dataStoreStudent, dataStoreSchool);
			}
			else{
				
				//this is a secondary school
				//use secondary school algorithm
					
				visitable = new Visitable_Alg7_student( visitorPickerFactory , conn , dataStoreStudent, dataStoreSchool );
			}
			
		}
		else{
			//just assume this is a secondary school
			
			visitable = new Visitable_Alg7_student( visitorPickerFactory , conn , dataStoreStudent, dataStoreSchool );
		}
		
		
		visitable.handleRequest( measureId , parameters );


	}

}
