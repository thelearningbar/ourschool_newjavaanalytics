package visitorPickers.student;

import java.sql.Connection;
import java.util.HashMap;

import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitables.student.bullying.V_QtoM_NoFilter_student;
import visitorPickers.IVisitorPicker;
import visitorPickers.VisitorPickerFactory;

public class VP_QtoM_NoFilter_student implements IVisitorPicker {


	private VisitorPickerFactory visitorPickerFactory;
	private DataStore_student dataStoreStudent;
	private DataStore_school dataStoreSchool;
	private Connection conn;
	
	
	public VP_QtoM_NoFilter_student(VisitorPickerFactory vpf, Connection conn, DataStore_student dataStoreStudent, DataStore_school dataStoreSchool) {

		this.visitorPickerFactory = vpf;
		this.dataStoreSchool=dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
	}
	
	
	@Override
	public void chooseVisitor(String measureId, HashMap<String, String> request) {

		IVisitable visitable;
		visitable = new V_QtoM_NoFilter_student(visitorPickerFactory, conn, dataStoreStudent, dataStoreSchool);
		visitable.handleRequest( measureId, request  );

	}
}
