package visitorPickers.student;

import java.sql.Connection;
import java.util.HashMap;

import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitables.student.bullying.Visitable_BullyPopulation_student;
import visitorPickers.IVisitorPicker;
import visitorPickers.VisitorPickerFactory;

public class VisitorPick_BullyPopulation_student implements IVisitorPicker {


	VisitorPickerFactory visitorPickerFactory;
	DataStore_student dataStoreStudent;
	DataStore_school dataStoreSchool;
	Connection conn;
	
	
	
	public VisitorPick_BullyPopulation_student(VisitorPickerFactory visitorPickerFactory ,  
			Connection conn , DataStore_student dataStoreStudent ,DataStore_school dataStoreSchool) {


		this.visitorPickerFactory = visitorPickerFactory;
		this.dataStoreSchool=dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
		
	}
	
	

	@Override
	public void chooseVisitor(String measureId, HashMap<String, String> request) {
		
		if( !request.containsKey("Bully-Category")){
			
			if( request.containsKey("schooltype")  ){
				request.put("Bully-Category", request.get("schooltype"));
			}
			else{
				System.out.println("no flags, guessing");
				
				request.put("Bully-Category","1" );
			}
		}
		
		IVisitable visitable;
		visitable = new Visitable_BullyPopulation_student(visitorPickerFactory, conn , dataStoreStudent, dataStoreSchool);
		visitable.handleRequest( measureId, request  );
		
	}

}
