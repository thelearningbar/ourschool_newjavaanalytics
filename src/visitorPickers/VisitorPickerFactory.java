package visitorPickers;

import java.sql.Connection;

import utilities.DataStore_school;
import utilities.DataStore_student;
import visitorPickers.school.VisitorPick_ACutoffDouble_school;
import visitorPickers.school.VisitorPick_ACutoff_GreaterThan_school;
import visitorPickers.school.VisitorPick_ACutoff_school;
import visitorPickers.school.VisitorPick_AScoreMean_school;
import visitorPickers.school.VisitorPick_DoChildrenOnly;
import visitorPickers.student.VP_QtoM_NoFilter_student;
import visitorPickers.student.VP_QtoM_QFilter_school;
import visitorPickers.student.VP_QtoM_QFilter_student;
import visitorPickers.student.VisitorPick_Alg1_student;
import visitorPickers.student.VisitorPick_Alg2_student;
import visitorPickers.student.VisitorPick_AvgWeighted_student;
import visitorPickers.student.VisitorPick_Avg_student;
import visitorPickers.student.VisitorPick_BullyPopMultiQ;
import visitorPickers.student.VisitorPick_BullyPop_QMeasure_Validate;
import visitorPickers.student.VisitorPick_BullyPopulation_student;
import visitorPickers.student.VisitorPick_Bully_student;
import visitorPickers.student.VisitorPick_SubmeasuresLADvalueToScore_student;

public class VisitorPickerFactory {

	
	DataStore_student dataStoreStudent;
	DataStore_school dataStoreSchool;
	Connection conn;
	
	public boolean debugPrint = false;
	
	
	public VisitorPickerFactory( Connection conn, DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool ) {
		this.conn=conn;
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
	}
	

	/**
	 * Get the visitor picker for the algorithm
	 * @param alg
	 */
	public IVisitorPicker getVisitorAlgo(String alg ) {
		
		if( debugPrint){
			System.out.println(this.getClass().getName());
		}
		
		
		VisitorPickerFactory vpf = this;

		switch (alg){
			case "1": 				return new  VisitorPick_Alg1_student(conn , dataStoreStudent);
			case "1_11m": 			return new  VisitorPick_Alg2_student(conn, dataStoreStudent);
			case "Avg_Weighted": 	return new VisitorPick_AvgWeighted_student(this, dataStoreStudent, dataStoreSchool);
			case "Avg":				return new  VisitorPick_Avg_student(this, dataStoreStudent);
			case "ACutoff":			return new  VisitorPick_ACutoff_school( this ,conn, dataStoreStudent, dataStoreSchool );
			case "ACutoff_GreaterThan":			return new  VisitorPick_ACutoff_GreaterThan_school( this ,conn, dataStoreStudent, dataStoreSchool );
			case "AScoreMean":		return new  VisitorPick_AScoreMean_school( this , conn, dataStoreStudent, dataStoreSchool );
			case "Bully_student": 	return new  VisitorPick_Bully_student( this , conn, dataStoreStudent, dataStoreSchool );
			case "ChildrenOnly": 	return new  VisitorPick_DoChildrenOnly( this , conn, dataStoreStudent, dataStoreSchool );
			case "BullyPopulation_student": 	return new  VisitorPick_BullyPopulation_student( this , conn, dataStoreStudent, dataStoreSchool );
			case "ACutoffDouble":	return new  VisitorPick_ACutoffDouble_school( this , conn, dataStoreStudent, dataStoreSchool );
			case "BullyPop_Ques_to_Measure_student":	return new  VisitorPick_BullyPopMultiQ( this , conn, dataStoreStudent, dataStoreSchool );
			case "SubmeasuresLADvalueToScore_student":  return new VisitorPick_SubmeasuresLADvalueToScore_student(vpf, conn, dataStoreStudent, dataStoreSchool);
			case "BullyPop_Ques_to_Measure_ParentFilter_student":	return new  VisitorPick_BullyPop_QMeasure_Validate( this , conn, dataStoreStudent, dataStoreSchool );
			case "QtoM_NoFilter_student":				return new  VP_QtoM_NoFilter_student( vpf , conn, dataStoreStudent, dataStoreSchool );
			case "QtoM_QFilter_student": 				return new VP_QtoM_QFilter_student( vpf , conn, dataStoreStudent, dataStoreSchool );
			case "QtoM_QFilter_school": 				return new VP_QtoM_QFilter_school( vpf , conn, dataStoreStudent, dataStoreSchool );
			default: 				return new  VisitorPick_AlgDefault();
		}
		
		
		
	}
	
	
	
	
}