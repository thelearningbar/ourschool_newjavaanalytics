package visitorPickers.school;

import java.sql.Connection;
import java.util.HashMap;

import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitables.school.Visitable_AScoreMean_school;
import visitorPickers.IVisitorPicker;
import visitorPickers.VisitorPickerFactory;

public class VisitorPick_AScoreMean_school implements IVisitorPicker {

	
	
	private VisitorPickerFactory visitorPicker;
	DataStore_student dataStoreStudent;
	DataStore_school dataStoreSchool;
	Connection conn;
	public boolean debugPrint=false;
	
	public VisitorPick_AScoreMean_school(  VisitorPickerFactory visitorPickerFactory , Connection conn , 
			DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool  ) {
		
		this.visitorPicker = visitorPickerFactory;
		this.dataStoreStudent=dataStoreStudent;
		this.dataStoreSchool=dataStoreSchool;
		this.conn=conn;
	}

	@Override
	public void chooseVisitor(String measureId, HashMap<String, String> request) {
		if (debugPrint)
			System.out.println(this.getClass().getName());


		IVisitable visitable;
		visitable = new Visitable_AScoreMean_school( visitorPicker ,conn ,dataStoreSchool , dataStoreStudent);
		visitable.handleRequest( measureId , request );
	}

}
