package visitorPickers.school;

import java.sql.Connection;
import java.util.HashMap;

import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitables.school.cutoff.VisitableACutoff_GreaterThan_school;
import visitorPickers.IVisitorPicker;
import visitorPickers.VisitorPickerFactory;

public class VisitorPick_ACutoff_GreaterThan_school implements IVisitorPicker {


	private VisitorPickerFactory visitorPickerFactory;
	private DataStore_school dataStoreSchool;
	private DataStore_student dataStoreStudent;
	private Connection conn;
	
	public VisitorPick_ACutoff_GreaterThan_school(VisitorPickerFactory visitorPickerFactory ,  
			Connection conn , DataStore_student dataStoreStudent ,DataStore_school dataStoreSchool  ) {
		
		this.visitorPickerFactory = visitorPickerFactory;
		this.dataStoreSchool=dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
		
	}

	@Override
	public void chooseVisitor(String measureId,   HashMap<String, String> request) {
		//System.out.println(this.getClass().getName());

		IVisitable visitable;
		visitable = new VisitableACutoff_GreaterThan_school( visitorPickerFactory , conn , dataStoreStudent, dataStoreSchool );
		visitable.handleRequest( measureId , request );

	}


}
