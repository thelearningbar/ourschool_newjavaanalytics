package visitorPickers.school;

import java.sql.Connection;
import java.util.HashMap;

import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitables.school.cutoff.Visitable_ACutoffDouble_school;
import visitorPickers.IVisitorPicker;
import visitorPickers.VisitorPickerFactory;



public class VisitorPick_ACutoffDouble_school implements IVisitorPicker {


	VisitorPickerFactory visitorPickerFactory;
	DataStore_school dataStoreSchool;
	DataStore_student dataStoreStudent;
	Connection conn;
	
	public boolean debugprint=false;
	
	
	public VisitorPick_ACutoffDouble_school( VisitorPickerFactory visitorPickerFactory ,  
			Connection conn , DataStore_student dataStoreStudent ,DataStore_school dataStoreSchool ) {


		this.visitorPickerFactory = visitorPickerFactory;
		this.dataStoreSchool=dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
		
	}

	@Override
	public void chooseVisitor(String measureId, HashMap<String, String> parameters) {
		
		if( debugprint)
			System.out.println(this.getClass().getName());
		

		IVisitable visitable;
		visitable = new Visitable_ACutoffDouble_school( visitorPickerFactory , conn , dataStoreStudent, dataStoreSchool );
		visitable.handleRequest( measureId , parameters );
		
	}

}
