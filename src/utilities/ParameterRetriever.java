package utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

//import org.json.*;

public class ParameterRetriever {
	
	private static String query = "Select * from Measure where id = ?";
	
	static Connection conn=null;
	
	@SuppressWarnings("static-access")
	public ParameterRetriever( Connection conn) {
		this.conn=conn;
	}
	

	public static String getProperty( String property ){

		Properties prop = new Properties();
		InputStream input = null;

		String ret = null;

		try {

			input = new FileInputStream("config/properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			ret = prop.getProperty(  property );

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return ret;
	}



	
	public static String getMeasureFlags( String measureId ){
		ResultSet resScoreComputation = null;
		PreparedStatement stmt = null;
		
		String flags = null;
		
		try {
			stmt = conn.prepareStatement(query);
			stmt.setInt(1, Integer.parseInt(measureId));
			resScoreComputation = stmt.executeQuery(); 
			
			while (resScoreComputation.next()) {
				flags = resScoreComputation.getString(flags);
			}
						
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return flags ;
	}
	

	
	/**
	 * not finished
	 */
	public HashMap<String,String> getDefaultProperties( String measureId){
		Properties prop = new Properties();
		InputStream input = null;
		HashMap<String, String> map =  new HashMap<String,String>();
		try {

			input = new FileInputStream("config/properties");

			// load a properties file
			prop.load(input);

			String parameterslist  = prop.getProperty(measureId + "_parameters");
			String[] parameters = parameterslist.split(",");
			
			for(String x : parameters){
				System.out.println(x);
				map.put(x, prop.getProperty(x));
			}
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return map;
	}


}
