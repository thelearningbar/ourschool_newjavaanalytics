package utilities;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Stores information ABOUT measures themselves
 * ie  threshold values, submeasures measures, new-old id mapping
 * 
 * @author Evan K
 *
 */
public class MeasureInfo {
	
	private static MeasureInfo instance = new MeasureInfo();
	private Connection conn=null;
	
	
	
	//cutoff of measure  <measureId, cutoff>
	private Map<String,String> measureCutoff;
	
	//min ans of measure <measureId , minimumAnswers required>
	private Map<String,String> measureMinimumAnswer;
	
	//use when calculation of 1 measure should produce multiple other measures at the same time
	//submeasures of measure <measureId , [ submeasures ....]  no key if has no submeasures
	private Map<String,List<String>> submeasures;
	
	//maps a questionId to a new measureId (for multiple choice tallys)
	public Map<String,String> questionNewMeasure;
	
	//measure, [questionIds]
	public Map<Integer,List<Integer>> measureQuestion;
	
	
	
	
	private MeasureInfo(  ) {
		DBconnection db = new DBconnection();
		
		conn=db.getConnection();
		
		measureCutoff= new HashMap<String,String>();
		measureMinimumAnswer = new HashMap<String,String>();
		submeasures = new HashMap<String, List<String>>();
		
		measureQuestion = new HashMap<Integer, List<Integer>>();
		
		
		//question, measure to save under
		questionNewMeasure= new HashMap<String,String>();
		
		/*
		//filterquestion for 1420
		questionNewMeasure.put("3623", "1420");
		questionNewMeasure.put("3623", "1412");
		questionNewMeasure.put("3623", "1411");
		*/
		
		//1693
		questionNewMeasure.put("1290", "1695");
		questionNewMeasure.put("1291", "1698");
		questionNewMeasure.put("1292", "1701");
		questionNewMeasure.put("1293", "1704");
			
		
		
		//1412
		questionNewMeasure.put("3551", "1708");
		questionNewMeasure.put("3552", "1709");
		questionNewMeasure.put("3553", "1710");
		questionNewMeasure.put("3554", "1711");
		questionNewMeasure.put("3555", "1712");
		questionNewMeasure.put("3556", "1713");
		
		//1411
		questionNewMeasure.put("3557", "1714");
		questionNewMeasure.put("3558", "1715");
		questionNewMeasure.put("3559", "1716");
		questionNewMeasure.put("3560", "1717");
		questionNewMeasure.put("3561", "1718");
		questionNewMeasure.put("3562", "1719");
		questionNewMeasure.put("3563", "1720");
		questionNewMeasure.put("3564", "1721");
		questionNewMeasure.put("3565", "1722");
		
		//1413
		questionNewMeasure.put("3566", "1723");
		questionNewMeasure.put("3567", "1724");
		questionNewMeasure.put("3568", "1725");
		questionNewMeasure.put("3569", "1726");
		questionNewMeasure.put("3570", "1727");
		questionNewMeasure.put("3571", "1728");
		questionNewMeasure.put("3572", "1729");
		questionNewMeasure.put("3573", "1730");
		
		//1420
		questionNewMeasure.put("3574", "1731");
		questionNewMeasure.put("3575", "1732");
		questionNewMeasure.put("3576", "1733");
		questionNewMeasure.put("3577", "1734");
		questionNewMeasure.put("3578", "1735");
		questionNewMeasure.put("3579", "1736");
		questionNewMeasure.put("3580", "1737");
		questionNewMeasure.put("3581", "1738");
		questionNewMeasure.put("3582", "1739");
		questionNewMeasure.put("3583", "1740");
		questionNewMeasure.put("3584", "1741");
		
		//1416
		questionNewMeasure.put("3593", "1742");
		questionNewMeasure.put("3594", "1743");
		questionNewMeasure.put("3595", "1744");
		questionNewMeasure.put("3596", "1745");
		questionNewMeasure.put("3597", "1746");
		questionNewMeasure.put("3598", "1747");
		questionNewMeasure.put("3599", "1748");
		questionNewMeasure.put("3600", "1749");
		questionNewMeasure.put("3601", "1750");
		
		
		//TODO get the info from database
		//1255 ASumPopulationFromSubMeasuresAndScale_measuresA (1261-1268)
		ArrayList<String> m1255 = new ArrayList<String>();
		ArrayList<String> m1691 = new ArrayList<String>();
		m1255.add("1261");
		m1255.add("1262");
		m1255.add("1263");
		m1255.add("1264");
		m1255.add("1265");
		m1255.add("1266");
		m1255.add("1267");
		m1255.add("1268");
		
		m1691.add("1689");
		m1691.add("1688");
		m1691.add("1690");		
		submeasures.put("1255", m1255);
		submeasures.put("1691", m1691);
		
		//Bully-Category primary
		List<String> subM = new ArrayList<String>();
		List<String> subM2 = new ArrayList<String>();
		List<String> subM3 = new ArrayList<String>();
		List<String> subM4 = new ArrayList<String>();
		subM.add("1696");
		subM.add("1697");
		subM2.add("1699");
		subM2.add("1700");
		subM3.add("1702");
		subM3.add("1703");
		subM4.add("1705");
		subM4.add("1706");
		submeasures.put("1695", subM);
		submeasures.put("1698", subM2);
		submeasures.put("1701", subM3);
		submeasures.put("1704", subM4);
		
		//Bully-Category secondary
		List<String> subM5= new ArrayList<String>();
		List<String> subM6 = new ArrayList<String>();
		List<String> subM7 = new ArrayList<String>();
		List<String> subM8 = new ArrayList<String>();
		subM5.add("1752");
		subM5.add("1753");
		subM5.add("1754");
		subM5.add("1755");
		subM6.add("1757");
		subM6.add("1758");
		subM6.add("1759");
		subM6.add("1760");
		subM7.add("1762");
		subM7.add("1763");
		subM7.add("1764");
		subM7.add("1765");
		subM8.add("1767");
		subM8.add("1768");
		subM8.add("1769");
		subM8.add("1770");
		submeasures.put("1751", subM5);
		submeasures.put("1756", subM6);
		submeasures.put("1761", subM7);
		submeasures.put("1766", subM8);
		
		List<String> m1415 = new ArrayList<String>();
		m1415.add("1421");
		m1415.add("1422");
		m1415.add("1423");
		m1415.add("1424");
		m1415.add("1425");
		m1415.add("1426");
		m1415.add("1427");
		m1415.add("1428");
		submeasures.put("1415", m1415);
		
		
		
		List<String> m1692 = new ArrayList<String>();
		m1692.add("1688");//hi
		m1692.add("1689");//lo
		m1692.add("1690");//mean
		submeasures.put("1692", m1692);
		
		
		
		saveAllMeasureInfo();
		saveMeasureQuestion();
		
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public String getNewMeasureForQuestion(String questionId){
		return questionNewMeasure.get(questionId);
	}
	
	public List<Integer> getQuestions( int measureId ){
		return measureQuestion.get(measureId);
		//return null;
	}
	
	public List<String> getSubMeasures( String measureId ){
		if( submeasures.containsKey(measureId) ){
			return submeasures.get(measureId);
		}
		else{
			return null;
		}
	}
	
	public String getCutoff( String measureId ){
		if( measureCutoff.containsKey(measureId) ){
			return measureCutoff.get(measureId);
		}
		else
			return null;
	}
	
	public String getMinimumAnswer(String measureId){
		if( measureMinimumAnswer.containsKey(measureId) ){
			return measureMinimumAnswer.get(measureId);
		}
		else
			return null;
	}
	
	
	/**
	 * Fetch and save measurequestion table
	 */
	public void saveMeasureQuestion(){
		String qry = "SELECT * FROM MeasureQuestion order by measureId, questionId";
		Statement stmt;
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(qry);			
			int lastMeasureId = 0;
			List<Integer> questionList = new ArrayList<Integer>() ;
			int firstRound = 1;
			
			while (rs.next()){
				
				int i=1;
				int measureId = rs.getInt(i++);
				int qid = rs.getInt(i++);	
				
				if( firstRound ==1){
					lastMeasureId = measureId;
					firstRound--;
				}
				
				if( measureId!=lastMeasureId){
					measureQuestion.put(lastMeasureId, questionList);
					lastMeasureId = measureId;
					questionList= new ArrayList<Integer>() ;
					questionList.add(qid);
				}
				else{
					questionList.add(qid);
				}
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			
		}
		System.out.println("got all MeasureQuestion info");
	}
	

	/**
	 * Fetch all info on all measures from database and keep in memory
	 */
	public void saveAllMeasureInfo(){
		String qry = "select id, cutoff, minimumAnswersRequired from Measure";
		Statement stmt;
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(qry);
			while (rs.next()){
				int i=1;
				String measureId = rs.getString(i++);
				String cut = rs.getString(i++);
				String minAns = rs.getString(i++);
				measureCutoff.put(measureId, cut);
				measureMinimumAnswer.put(measureId, minAns);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			
		}
		
		System.out.println("got all measure info");
	}
	
	public void saveMeasureInfo(String measureId){
		String qry = "select cutoff, minimumAnswersRequired from Measure where id = " + measureId;
		Statement stmt;
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(qry);
			
			while (rs.next()){
				int i=1;
				String cut = rs.getString(i++);
				String minAns = rs.getString(i++);
				measureCutoff.put(measureId, cut);
				measureMinimumAnswer.put(measureId, minAns);
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			
		}
	}
	
	
	
	
	
	
	
	
	
	/*
	 * for singleton
	 */
	public static MeasureInfo getInstance(){
		if( instance==null )
			instance = new MeasureInfo();
		return instance;
	}

}
