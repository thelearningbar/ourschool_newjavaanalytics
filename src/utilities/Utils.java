package utilities;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Utils {

	public static int getInt(String string) {
		return Integer.parseInt(string);
	}

	public static void pr( String s){
		System.out.println(s);
	}

	public static String its( int x){
		return Integer.toString(x);
	}

	public static Map<Integer, Double> normalizePopulationPercentage(Map<Integer, Double> questionPopulation) {

		int totalPercentage =0;
		int maxIndex1 = -1;
		int maxIndex2 = -1;

		double newMax1 = Integer.MIN_VALUE; // First Max  
		double newMax2 =Integer.MIN_VALUE; // Second Max

		for (Integer key : questionPopulation.keySet()){
			//for (int i = 0; i<schoolScore.size(); i++){
			//System.out.println("Inside  >>" + schoolScore.get(i)); 
			totalPercentage += (int) Math.round(questionPopulation.get(key)) ;
			if( questionPopulation.get(key)>newMax1 ){ 
				newMax2 = newMax1;
				maxIndex2 = maxIndex1;
				newMax1 = questionPopulation.get(key); 
				maxIndex1 = key;
			}
			else if (questionPopulation.get(key)>newMax2){
				newMax2 = questionPopulation.get(key);
				maxIndex2 = key;
			}
		}

		//System.out.println( "total percentage gap : "  +  (totalPercentage-100)  + "  Max: " + maxIndex);
		if (totalPercentage-100 > 0)
		{
			if (totalPercentage-100 == 1)
				questionPopulation.replace(maxIndex1, questionPopulation.get(maxIndex1) - 1) ;
			else if (totalPercentage-100 == 2){
				questionPopulation.replace(maxIndex1, questionPopulation.get(maxIndex1) - 1) ;
				questionPopulation.replace(maxIndex2, questionPopulation.get(maxIndex2) - 1) ;
			}
		}
		else if (totalPercentage- 100 <0){
			if (totalPercentage-100 == -1)   
				questionPopulation.replace(maxIndex1, questionPopulation.get(maxIndex1) + 1) ;
			else if ((totalPercentage-100 == -2)){
				questionPopulation.replace(maxIndex1, questionPopulation.get(maxIndex1) + 1) ;
				questionPopulation.replace(maxIndex2, questionPopulation.get(maxIndex2) + 1) ;
			}
		}

		Map<Integer, Double>  questionPopulationNew = new HashMap<Integer, Double> ();

		for (Integer key : questionPopulation.keySet()){
			questionPopulationNew.put (key,  (double) Math.round(questionPopulation.get(key)));
		}

		return questionPopulationNew;
	}

	
	
	
	/**
	 * For debug, shows column results of a select statement in console
	 * @param rs
	 */
	public static void printResultSet(ResultSet rs){
		try {
			rs.beforeFirst();

			ResultSetMetaData rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();

			while (rs.next()) {
				for (int i = 1; i <= columnsNumber; i++) {

					if (i > 1) 
						System.out.print(" | ");

					String columnValue = rs.getString(i);
					System.out.print(columnValue + " " + rsmd.getColumnName(i));

				}
				System.out.println("");
			}

			rs.beforeFirst();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	public static boolean isNumeric(String str)
	{
		return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
	}


	public static boolean isInteger(String str) {

		if( str ==null){
			return false;
		}
		return str.matches("^-?\\d+$");
	}


	public static double round(double value, int places) {


		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}


	public static boolean boolFromMap(String param , HashMap<String,String> map){
		return Boolean.parseBoolean(map.get(param));
	}


	public static int intFromMap(String param , HashMap<String,String> map){
		return Integer.parseInt(map.get(param));
	}


	public static double doubleFromMap(String param , HashMap<String,String> map){
		return Double.parseDouble(map.get(param));
	}


}
