package utilities;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.sql.PreparedStatement;

public class MeasureArchitecture {

	private static MeasureArchitecture instance;// = new MeasureArchitecture();

	protected static MeasureTree tree;
	private Connection conn = null;
	protected static HashMap<Integer, MeasureNode> nodeMap;

	protected static Multimap<Integer, MeasureNode> multimap;


	private MeasureArchitecture(Connection conn) {
		nodeMap = new HashMap<Integer, MeasureNode>();
		multimap = ArrayListMultimap.create();

		tree = new MeasureTree();
		retrieveMeasureArchitecture();
	}


	private MeasureArchitecture() {
		DBconnection db= new DBconnection();
		conn = db.getConnection();
		nodeMap = new HashMap<Integer, MeasureNode>();
		multimap = ArrayListMultimap.create();

		tree = new MeasureTree();
		retrieveMeasureArchitecture();
	}



	public List<String> getChildMeasures(int parentId){
		List<String> childMeasures = new ArrayList<String>();
		for(Entry<Integer,Collection<MeasureNode>> e : multimap.asMap().entrySet()) {
			Collection<MeasureNode> list =    e.getValue();
			Iterator<MeasureNode> iterator = list.iterator();
			while (iterator.hasNext()) {
				MeasureNode mn = iterator.next(); 
				if (mn.parentID== parentId)
					childMeasures.add (String.valueOf(mn.ID));
			}
		}
		return childMeasures;
	}



	public void showTree(int rootMeasure) {

		// MeasureNode rootNode = nodeMap.get(rootMeasure) ;

		Collection<MeasureNode> rootNode = multimap.get(rootMeasure);
		Iterator<MeasureNode> iterator = rootNode.iterator();

		while (iterator.hasNext()) {
			tree.exploreTree(iterator.next(), "    ");
		}

		// tree.exploreTree(rootNode," ");
	}

	
	
	public Set<Integer> getListReportingMeasureInForest(int measureId) {
		// TODO Auto-generated method stub

		Set<Integer> listReportingMeasures = new HashSet<Integer>();



		Collection<MeasureNode> rootNode = multimap.get(measureId);
		Iterator<MeasureNode> iterator = rootNode.iterator();

		while (iterator.hasNext()) {

			List<Integer> listMeasures = new ArrayList<Integer>();
			listMeasures = tree.exploreTreeForComputingMeasure(iterator.next());

			for (int i = 0; i < listMeasures.size(); i++)
				listReportingMeasures.add(listMeasures.get(i));

		}

		//System.out.println("List reporting measures " + listReportingMeasures);

		return listReportingMeasures;
	}


	public Set<Integer> getListComputingMeasureInForest(Set<Integer> listReportingMeasures) {


		Set<Integer> listComputingMeasures = new HashSet<Integer>();

		for (int i : listReportingMeasures) {
			List<Integer> computingMeasure = new ArrayList<Integer>();

			getComputingMeasure(i, computingMeasure);

			for (int j = 0; j < computingMeasure.size(); j++)
				listComputingMeasures.add(computingMeasure.get(j));

		}

		//System.out.println("List computing measures " + listComputingMeasures);
		return listComputingMeasures;
	}

	
	
	
	public List<Integer> getComputingMeasure(int measureId, List<Integer> computingMeasure) {

		Collection<MeasureNode> childList = multimap.get(measureId);
		Iterator<MeasureNode> iteratorChild = childList.iterator();

		while (iteratorChild.hasNext()) {

			MeasureNode child = iteratorChild.next();

			Collection<MeasureNode> parentList = multimap.get(child.parentID);

			Iterator<MeasureNode> iteratorParent = parentList.iterator();

			if (child.computation == 0) {
				computingMeasure.add(child.ID);
				break;
			}

			while (iteratorParent.hasNext()) {
				MeasureNode parent = iteratorParent.next();

				if (parent.computation == 0)
					computingMeasure.add(parent.ID);
				else
					getComputingMeasure(parent.ID, computingMeasure);

			}

		}

		return computingMeasure;
	}

	private void retrieveMeasureArchitecture() {
		// TODO Auto-generated method stub
		String computationQuery = " Select * from MeasureArchitecture ;";

		PreparedStatement stmt;

		try {

			stmt = conn.prepareStatement(computationQuery);
			ResultSet rs = stmt.executeQuery();
			// Utils.printResultSet(rs);
			while (rs.next()) {
				int i = 1;

				int id = rs.getInt(i++);
				// measureName = rs.getString(i++); ;
				int parentId = rs.getInt(i++);
				int computation = rs.getInt(i++);
				int reporting = rs.getInt(i++);

				// nodeMap.put(id, new MeasureNode( computation, reporting, id,
				// parentId)) ;

				multimap.put(id, new MeasureNode(computation, reporting, id, parentId));

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

		}

		/*
		 * for (int node : nodeMap.keySet() ) {
		 * 
		 * 
		 * //System.out.println( list_nodes.get(i).ID +" parent>>"
		 * +list_nodes.get(i).parentID );
		 * 
		 * 
		 * MeasureNode child = nodeMap.get(node) ; MeasureNode parent =
		 * nodeMap.get(child.parentID) ;
		 * 
		 * 
		 * 
		 * if(parent==null) continue;
		 * 
		 * tree.addTreeNode3(parent, child);
		 * 
		 * }
		 */

		for (int node : multimap.keySet()) {

			// System.out.println( list_nodes.get(i).ID +" parent>>"
			// +list_nodes.get(i).parentID );

			Collection<MeasureNode> childList = multimap.get(node);

			Iterator<MeasureNode> iteratorChild = childList.iterator();

			while (iteratorChild.hasNext()) {

				MeasureNode child = iteratorChild.next();

				Collection<MeasureNode> parentList = multimap.get(child.parentID);

				Iterator<MeasureNode> iteratorParent = parentList.iterator();

				while (iteratorParent.hasNext()) {

					MeasureNode parent = iteratorParent.next();

					if (parent == null)
						continue;

					else
						tree.addTreeNode3(parent, child);

				}

			}

		}

		System.out.println("Measure Tree Created");
	}

	
	
	public static MeasureArchitecture getInstance() {
		if (instance == null)
			instance = new MeasureArchitecture();
		return instance;
	}

}
