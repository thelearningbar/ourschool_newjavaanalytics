package utilities;

import java.util.ArrayList;
import java.util.List;

public class MeasureTree {
	public MeasureNode root;

	public MeasureTree() {
	}

	public void addTreeNode3(MeasureNode parent, MeasureNode newChild) {

		if (parent == null) {
			return;
		}
		if (parent.child == null) {
			parent.child = newChild;

			// System.out.println("parent.child null " + parent.ID + " "
			// +newChild.ID );
			return;
		}

		MeasureNode temp = parent.child;

		// System.out.println("temp " + temp.ID +" " +temp.sibling + " " +
		// newChild.ID );

		while (temp.sibling != null) {

			// System.out.println("temp.sibling.not.null " + temp.ID +" "
			// +temp.sibling + " " + newChild.ID );

			temp = temp.sibling;
		}
		temp.sibling = newChild;

	}

	public MeasureNode find_Node3(ArrayList<MeasureNode> nodes, int parentID) {
		for (int i = 0; i < nodes.size(); i++) {
			if (nodes.get(i).ID == parentID)
				return nodes.get(i);
		}

		return null;
	}

	public void printNode(MeasureNode root, String appender) {

		System.out.println(appender + root.ID + "_" + root.computation + "_" + root.reporting);

	}

	public void preorder(MeasureNode root, String appender) {

		if (root == null)
			return;

		printNode(root, appender);
		preorder(root.child, appender + appender);
		preorder(root.sibling, appender);

	}

	public void preorderExplore(MeasureNode root, List<Integer> listMeasures) {

		if (root == null)
			return;

		if (root.reporting == 1) {
			listMeasures.add(root.ID);

		}
		preorderExplore(root.child, listMeasures);
		preorderExplore(root.sibling, listMeasures);

	}

	public void exploreTree(MeasureNode root, String appender) {

		if (root == null)
			return;

		printNode(root, appender);
		preorder(root.child, appender + appender);
		// preorder(root.sibling, appender );

	}

	public List<Integer> exploreTreeForComputingMeasure(MeasureNode rootNode) {

		List<Integer> listMeasures = new ArrayList<Integer>();

		if (rootNode.reporting == 1)
			listMeasures.add(rootNode.ID);

		preorderExplore(rootNode.child, listMeasures);

		return listMeasures;
	}

}