package utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataStore_school {

	
	
	
	/*
	measure results indexed by characteristic combination
	
	key: combination of characteristics
	value:  aggregated result of measure
	
	ex 
	{1=0, 2=1}= { 
					1214 [ 1.1 , 4 , 5 ],
					1215 [ 2.5  ,4 , 6 ]
				}
	
	*/
	private   HashMap< HashMap<String,String> , HashMap<String, List<String>>   > characteristicResults ; 
	
	
	//list of measures already computed for school lvl
	private  ArrayList<String> computedMeasures;
	
	
	public  void clearAllData(){
		computedMeasures.clear();
		characteristicResults.clear();
		
	}	
	
	
	public DataStore_school() {
		computedMeasures = new ArrayList<String>();
		characteristicResults = new  HashMap< HashMap<String,String> , HashMap<String, List<String>> >();
	}

	
	public  HashMap< HashMap<String,String> , HashMap<String, List<String>>   > getCharacteristicResults(){
		return characteristicResults;
	}
	
	public  HashMap<String, List<String>> getCharacteristicResults( HashMap<String,String> characteristicCombo ){
		return characteristicResults.get(characteristicCombo);
	}
	
	
	
	public  void putCharacteristicResults( HashMap<String,String> characteristicCombo , String measureId, List<String> results){
		//System.out.println( "putCharacteristicResults  " + characteristicCombo.toString() );
		
		if( characteristicResults.containsKey(characteristicCombo) ){
			characteristicResults.get( characteristicCombo ).put(measureId, results);
		}
		else{
			HashMap<String, List<String>> newResults = new HashMap<String, List<String>>();
			newResults.put(measureId, results);
			characteristicResults.put(characteristicCombo, newResults);
		}
	}

	
	public  void setMeasureComputed(String measureId) {
		computedMeasures.add(measureId);
	}

	public  boolean isComputed(String measureId) {
		return computedMeasures.contains(measureId);
	}
	
		
	
	

	public  void printCharacteristicResults(){
		System.out.println("School Level Aggregated Results:" );
		for (HashMap<String,String>  x  : characteristicResults.keySet()) {
			HashMap<String, List<String>> y = characteristicResults.get(x);
			System.out.println( x.toString() + " = " + y.toString());
		}
	}
	

}
