package utilities;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


/**
 * 
 * Maps each measure to an algorithm name
 * and maps measures to any prerequisite child measures that must be computed before their parent
 * 
 * Registers a measure with an algorithm ->MeasureRegister
 * 
 * 
 * @author Evan K
 *
 */
public class MeasureRegister {

	private static MeasureRegister instance = new MeasureRegister();
	
	//maps each measure to an algorithm
	private static HashMap<String,String> measureAlgStudent;
	private static HashMap<String,String> measureAlgSchool;
	
	
	//define any prerequisite child measures for each algorithm
	//children must be computed before the parent
	private static HashMap<String,List<String>> measureTreesStudent;
	private static HashMap<String,List<String>> measureTreesSchool;
	
	
	
	private MeasureRegister() {
		measureAlgStudent = new HashMap<String,String>();
		measureAlgSchool = new HashMap<String,String>();
		measureTreesStudent = new HashMap<String,List<String>>();
		measureTreesSchool = new HashMap<String,List<String>>();
		
		createMeasureAlgStudentLvl();
		createMeasureAlgSchoolLvl();
		
		createMeasureTreesStudentLvl();
		createMeasureTreesSchoolLvl();
		
		System.out.println("MeasureRegister registered measures");
	}

	
	public String getAlgStudentLvl(String measureId){
		return measureAlgStudent.get(measureId);
	}
	

	public String getAlgSchoolLvl(String measureId){
		return measureAlgSchool.get(measureId);
	}
	
	public List<String> getChildMeasuresStudentLvl(String measureId){
		return measureTreesStudent.get(measureId);
	}
	

	public List<String> getChildMeasuresSchoolLvl(String measureId){
		return measureTreesSchool.get(measureId);
	}
	

	/**
	 * define children of each measure
	 * STUDENT lvl
	 */
	private void createMeasureTreesStudentLvl(){
		
		measureTreesStudent.put("1214", Arrays.asList("1215","1216","1217"));
		
		measureTreesStudent.put("1022", Arrays.asList("1101","1102","1103"));
		measureTreesStudent.put("1023", Arrays.asList("1104","1105","1106"));
		measureTreesStudent.put("1049", Arrays.asList("1113","1114","1115"));
		measureTreesStudent.put("1029", Arrays.asList("1107","1108","1109"));
		measureTreesStudent.put("1521", Arrays.asList("1522","1523","1524"));
		
		
	
	}
	
	
	
	/**
	 * define children of measures
	 * children are prerequisites to computing parents, ie DFS tree
	 * SCHOOL lvl
	 */
	private void createMeasureTreesSchoolLvl(){
		
		measureTreesSchool.put("1214", Arrays.asList("1215","1216","1217"));
		
		measureTreesSchool.put("1022", Arrays.asList("1101","1102","1103"));
		measureTreesSchool.put("1023", Arrays.asList("1104","1105","1106"));
		measureTreesSchool.put("1049", Arrays.asList("1113","1114","1115"));
		measureTreesSchool.put("1029", Arrays.asList("1107","1108","1109"));
		measureTreesSchool.put("1521", Arrays.asList("1522","1523","1524"));
		
		
		//Bullying
		measureTreesSchool.put("1692", Arrays.asList("1690","1689","1688"));
		
		measureTreesSchool.put("1694", Arrays.asList("1695","1698","1701","1704" ));
		measureTreesSchool.put("1771", Arrays.asList( "1751","1756","1761","1766"));
		
		measureTreesSchool.put("1695", Arrays.asList("1696","1697"));
		measureTreesSchool.put("1698", Arrays.asList("1699","1700"));
		measureTreesSchool.put("1701", Arrays.asList("1702","1703"));
		measureTreesSchool.put("1704", Arrays.asList("1705","1706"));
		
		measureTreesSchool.put("1751", Arrays.asList("1752","1753","1754","1755"));
		measureTreesSchool.put("1756", Arrays.asList("1757","1758","1759","1760"));
		measureTreesSchool.put("1761", Arrays.asList("1762","1763","1764","1765"));
		measureTreesSchool.put("1766", Arrays.asList("1767","1768","1769","1770"));
		
		measureTreesSchool.put("1412", Arrays.asList("1708","1709","1710","1711","1712","1713"));
		measureTreesSchool.put("1411", Arrays.asList("1714","1715","1716","1717","1718","1719","1720","1721","1722"));
		measureTreesSchool.put("1413", Arrays.asList("1723","1724","1725","1726","1727","1728","1729","1730"));
		measureTreesSchool.put("1420", Arrays.asList("1731","1732","1733","1734","1735","1736","1737","1738","1739","1740","1741"));
				
		measureTreesSchool.put("1415", Arrays.asList("1421","1422","1423","1424","1425","1426","1427","1428"));
		measureTreesSchool.put("1416", Arrays.asList("1742","1743","1744","1745","1746","1747","1748","1749","1750"));
		
	}
	
	
	
	
	/**
	 * define algorithm to use for each measure
	 * STUDENT LVL
	 */
	private void createMeasureAlgStudentLvl(){
		
		measureAlgStudent.put("1521", "Avg_Weighted");
		
		measureAlgStudent.put("1029", "Avg");
		measureAlgStudent.put("1049", "Avg_Weighted");
		
		measureAlgStudent.put("1022", "Avg");
		measureAlgStudent.put("1023", "Avg");
		
		measureAlgStudent.put("1522", "1");
		measureAlgStudent.put("1523", "1");
		measureAlgStudent.put("1524", "1");
		
		measureAlgStudent.put("1113", "1");
		measureAlgStudent.put("1114", "1");
		measureAlgStudent.put("1115", "1");
		
		measureAlgStudent.put("1101", "1_11m");
		measureAlgStudent.put("1102", "1_11m");
		measureAlgStudent.put("1103", "1_11m");
		measureAlgStudent.put("1104", "1");
		measureAlgStudent.put("1105", "1");
		measureAlgStudent.put("1106", "1");
		measureAlgStudent.put("1107", "1");
		measureAlgStudent.put("1108", "1");
		measureAlgStudent.put("1109", "1");
		
		measureAlgStudent.put("1214", "Avg");
		measureAlgStudent.put("1215", "1_11m");
		measureAlgStudent.put("1216", "1_11m");
		measureAlgStudent.put("1217", "1_11m");
						
		measureAlgStudent.put(  "12", "Bully_student");
		measureAlgStudent.put("1119", "Bully_student");
		measureAlgStudent.put("1116", "Bully_student");
		
		measureAlgStudent.put("1087", "Bully_student");
		measureAlgStudent.put("1120", "Bully_student");
		
		measureAlgStudent.put("1688", "Default");
		measureAlgStudent.put("1689", "Default");
		measureAlgStudent.put("1690", "Default");
		
		measureAlgStudent.put("1692", "Bully_student");
		
		//Bully-Category
		measureAlgStudent.put("1694", "BullyPopulation_student");
		measureAlgStudent.put("1771", "BullyPopulation_student");
		
		//Bully 1695 y/n populations
		measureAlgStudent.put("1695", "BullyPopulation_student");
		measureAlgStudent.put("1698", "BullyPopulation_student");
		measureAlgStudent.put("1701", "BullyPopulation_student");
		measureAlgStudent.put("1704", "BullyPopulation_student");
		
		measureAlgStudent.put("1751", "BullyPopulation_student");
		measureAlgStudent.put("1756", "BullyPopulation_student");
		measureAlgStudent.put("1761", "BullyPopulation_student");
		measureAlgStudent.put("1766", "BullyPopulation_student");
		
		//Bully tally population
		
		measureAlgStudent.put("1415", "SubmeasuresLADvalueToScore_student");
		measureAlgStudent.put("1416", "QtoM_NoFilter_student");
		
		
		measureAlgStudent.put("1420", "QtoM_QFilter_student");
		measureAlgStudent.put("1412", "QtoM_QFilter_student");
		measureAlgStudent.put("1411", "QtoM_QFilter_student"); //BullyPop_Ques_to_Measure_student
		
		//Filter
		measureAlgStudent.put("1413", "BullyPop_Ques_to_Measure_ParentFilter_student");
	}
	
	
	/*
	 * define algorithm to use for each measure
	 * SCHOOL lvl
	 */
	private void createMeasureAlgSchoolLvl(){
		
		//AScoreMean = 1113,1114,1115,1107,1108,1109, 1522,1523,1524
		
		//ACutOff    = 1101,1102,1103,1104,1105,1106,1,1215,1216,1217,1214 	
		
		measureAlgSchool.put("1113", "AScoreMean");
		measureAlgSchool.put("1114", "AScoreMean");
		measureAlgSchool.put("1115", "AScoreMean");
		measureAlgSchool.put("1107", "AScoreMean");
		measureAlgSchool.put("1108", "AScoreMean");
		measureAlgSchool.put("1109", "AScoreMean");
		measureAlgSchool.put("1522", "AScoreMean");
		measureAlgSchool.put("1523", "AScoreMean");
		measureAlgSchool.put("1524", "AScoreMean");
		
		
		measureAlgSchool.put("1023", "ACutoff");
		measureAlgSchool.put("1022", "ACutoff");
		
		measureAlgSchool.put("1049", "AScoreMean");
		measureAlgSchool.put("1029", "AScoreMean");
		measureAlgSchool.put("1521", "AScoreMean");
				
		
		measureAlgSchool.put("1101", "ACutoff");
		measureAlgSchool.put("1102", "ACutoff");
		measureAlgSchool.put("1103", "ACutoff");
		measureAlgSchool.put("1104", "ACutoff");
		measureAlgSchool.put("1105", "ACutoff");
		measureAlgSchool.put("1106", "ACutoff");
		measureAlgSchool.put(   "1", "ACutoff");
		
		measureAlgSchool.put("1214", "ACutoff");
		measureAlgSchool.put("1215", "ACutoff");
		measureAlgSchool.put("1216", "ACutoff");
		measureAlgSchool.put("1217", "ACutoff");
		
		
		
		
		//Bully population
		measureAlgSchool.put("1693", "ChildrenOnly");
		measureAlgSchool.put("1694", "ChildrenOnly");
		measureAlgSchool.put("1695", "ChildrenOnly");
		measureAlgSchool.put("1698", "ChildrenOnly");
		measureAlgSchool.put("1701", "ChildrenOnly");
		measureAlgSchool.put("1704", "ChildrenOnly");
		
		measureAlgSchool.put("1751", "ChildrenOnly");
		measureAlgSchool.put("1756", "ChildrenOnly");
		measureAlgSchool.put("1761", "ChildrenOnly");
		measureAlgSchool.put("1766", "ChildrenOnly");
		
		measureAlgSchool.put("1771", "ChildrenOnly");
		
		
		measureAlgSchool.put("1696", "ACutoff");
		measureAlgSchool.put("1697", "ACutoff");
		measureAlgSchool.put("1699", "ACutoff");
		measureAlgSchool.put("1700", "ACutoff");
		measureAlgSchool.put("1702", "ACutoff");
		measureAlgSchool.put("1703", "ACutoff");
		measureAlgSchool.put("1705", "ACutoff");
		measureAlgSchool.put("1706", "ACutoff");
		
		measureAlgSchool.put("1752", "ACutoff");
		measureAlgSchool.put("1753", "ACutoff");
		measureAlgSchool.put("1754", "ACutoff");
		measureAlgSchool.put("1755", "ACutoff");
		
		measureAlgSchool.put("1757", "ACutoff");
		measureAlgSchool.put("1758", "ACutoff");
		measureAlgSchool.put("1759", "ACutoff");
		measureAlgSchool.put("1760", "ACutoff");
		
		measureAlgSchool.put("1762", "ACutoff");
		measureAlgSchool.put("1763", "ACutoff");
		measureAlgSchool.put("1764", "ACutoff");
		measureAlgSchool.put("1765", "ACutoff");
		
		measureAlgSchool.put("1767", "ACutoff");
		measureAlgSchool.put("1768", "ACutoff");
		measureAlgSchool.put("1769", "ACutoff");
		measureAlgSchool.put("1770", "ACutoff");
		
		//-------------
				
		measureAlgSchool.put("1689", "Default");
		measureAlgSchool.put("1688", "Default");
		measureAlgSchool.put("1690", "Default");
		
		measureAlgSchool.put("1692", "ACutoffDouble");
		
		measureAlgSchool.put("1691", "Default");
		//------------------
		
		measureAlgSchool.put("1413", "ChildrenOnly");
		measureAlgSchool.put("1416", "ChildrenOnly");
		
		
		
		measureAlgSchool.put("1411", "QtoM_QFilter_school");
		measureAlgSchool.put("1412", "QtoM_QFilter_school");
		measureAlgSchool.put("1420", "QtoM_QFilter_school");
		
		
		
		
		//1412 set
		measureAlgSchool.put("1708", "ACutoff");
		measureAlgSchool.put("1709", "ACutoff");
		measureAlgSchool.put("1710", "ACutoff");
		measureAlgSchool.put("1711", "ACutoff");
		measureAlgSchool.put("1712", "ACutoff");
		measureAlgSchool.put("1713", "ACutoff");
		
		//1411
		measureAlgSchool.put("1714", "ACutoff");
		measureAlgSchool.put("1715", "ACutoff");
		measureAlgSchool.put("1716", "ACutoff");
		measureAlgSchool.put("1717", "ACutoff");
		measureAlgSchool.put("1718", "ACutoff");
		measureAlgSchool.put("1719", "ACutoff");
		measureAlgSchool.put("1720", "ACutoff");
		measureAlgSchool.put("1721", "ACutoff");
		measureAlgSchool.put("1722", "ACutoff");
		
		
		//1413
		measureAlgSchool.put("1723", "ACutoff");
		measureAlgSchool.put("1724", "ACutoff");
		measureAlgSchool.put("1725", "ACutoff");
		measureAlgSchool.put("1726", "ACutoff");
		measureAlgSchool.put("1727", "ACutoff");
		measureAlgSchool.put("1728", "ACutoff");
		measureAlgSchool.put("1729", "ACutoff");
		measureAlgSchool.put("1730", "ACutoff");
		
		
		//1416
		measureAlgSchool.put("1742", "ACutoff");
		measureAlgSchool.put("1743", "ACutoff");
		measureAlgSchool.put("1744", "ACutoff");
		measureAlgSchool.put("1745", "ACutoff");
		measureAlgSchool.put("1746", "ACutoff");
		measureAlgSchool.put("1747", "ACutoff");
		measureAlgSchool.put("1748", "ACutoff");
		measureAlgSchool.put("1749", "ACutoff");
		measureAlgSchool.put("1750", "ACutoff");
		
		
		//1420
		measureAlgSchool.put("1731", "ACutoff");
		measureAlgSchool.put("1732", "ACutoff");
		measureAlgSchool.put("1733", "ACutoff");
		measureAlgSchool.put("1734", "ACutoff");
		measureAlgSchool.put("1735", "ACutoff");
		measureAlgSchool.put("1736", "ACutoff");
		measureAlgSchool.put("1737", "ACutoff");
		measureAlgSchool.put("1738", "ACutoff");
		measureAlgSchool.put("1739", "ACutoff");
		measureAlgSchool.put("1740", "ACutoff");
		measureAlgSchool.put("1741", "ACutoff");
		
		
		
		
		//1415 tree
		measureAlgSchool.put("1415", "ChildrenOnly");
		measureAlgSchool.put("1421", "ACutoff_GreaterThan");
		measureAlgSchool.put("1422", "ACutoff_GreaterThan");		
		measureAlgSchool.put("1423", "ACutoff_GreaterThan");		
		measureAlgSchool.put("1424", "ACutoff_GreaterThan");		
		measureAlgSchool.put("1425", "ACutoff_GreaterThan");
		measureAlgSchool.put("1426", "ACutoff_GreaterThan");
		measureAlgSchool.put("1427", "ACutoff_GreaterThan");	
		measureAlgSchool.put("1428", "ACutoff_GreaterThan");	
		
		measureAlgSchool.put("1416", "ChildrenOnly");
		
		measureAlgSchool.put("1255", "ASumPopulationFromSubMeasuresAndScale_measuresA");
		
	}
	
	
	
	
	
	/*
	 * for singleton
	 */
	public static MeasureRegister getInstance(){
		if( instance==null )
			instance = new MeasureRegister();
		return instance;
	}
	
	
	
}
