package utilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



public class ParticipantCharacteristicRetriever {

	Connection conn;
	
	DataStore_student dataStoreStudent;
	
	public boolean debugPrint = false;
	
	public ParticipantCharacteristicRetriever( Connection conn , DataStore_student dataStoreStudent) {
		this.conn=conn;
		this.dataStoreStudent=dataStoreStudent;
	}
		
	public void getAllParticipantInfo(String surveyInstanceId ){
		getAllParticipantInfo1(surveyInstanceId);
		//getAllParticipantInfo2(surveyInstanceId);
	}
	
	//part1 backtracking from strings in participant characteristic which are not ordering
	public void getAllParticipantInfo1( String surveyInstanceId ){
		
				
		//backtracking from value to ordering
		//#match participant characteristic value to characteristicResponseLang ordering, with exceptions
		
		String qry2 = "SELECT DISTINCT"
				+ "    ParticipantCharacteristic.surveyParticipantId,"
				+ "    ParticipantCharacteristic.characteristicId,"
				+ "    CharacteristicResponseLang.ordering,"
				+ "    ParticipantCharacteristic.value"
				+ "    FROM"
				+ "    ParticipantCharacteristic"
				+ "    JOIN"
				+ "    (SELECT DISTINCT"
				+ "        surveyParticipantId"
				+ "    FROM"
				+ "        Answer4Rpt2016"
				+ "    WHERE"
				+ "        surveyInstanceId = ?) AS x ON x.surveyParticipantId = ParticipantCharacteristic.surveyParticipantId"
				+ "        AND ParticipantCharacteristic.characteristicId IN (1,2,3,21,22,23,24,25,35,41,42,43,49,50,52,53,54,55,56,60)"
				+ "        LEFT JOIN"
				+ "    CharacteristicResponseLang ON CharacteristicResponseLang.characteristicId = ParticipantCharacteristic.characteristicId"
				+ "        AND ParticipantCharacteristic.value = CharacteristicResponseLang.valueLabel"
				+ "        AND CharacteristicResponseLang.languageId = 1;";
		
		/*
		String qry2 = " SELECT distinct "
				+ " ParticipantCharacteristic.surveyParticipantId,  "
				+ " ParticipantCharacteristic.characteristicId, "
				+ " CharacteristicResponseLang.ordering "
				+ " FROM ParticipantCharacteristic "
				+ " join (select distinct surveyParticipantId from Answer4Rpt2016 where surveyInstanceId = ? ) as x "
				+ "	on x.surveyParticipantId = ParticipantCharacteristic.surveyParticipantId "
				+ " join CharacteristicResponseLang on CharacteristicResponseLang.characteristicId = ParticipantCharacteristic.characteristicId"
				+ " and CharacteristicResponseLang.valueLabel = ParticipantCharacteristic.value "
				
				+ " where ParticipantCharacteristic.characteristicId in ( 2,3,21,22,23,24,25,35,41,42,43,49,50,52,53,54,55,56,60 )"
				+ " and CharacteristicResponseLang.languageId =1 ;";
				
				*/
		
		//fetch all excluding..
		//+ " where ParticipantCharacteristic.characteristicId not in ( 1,2,   8,9,11,12   ,14,16 )"
		
		try {
			PreparedStatement stmt = conn.prepareStatement(qry2);
			stmt.setInt(1, Integer.parseInt(surveyInstanceId) );
			ResultSet rs = stmt.executeQuery();
			while (rs.next()){
				int i = 1;
				
				String participantId = rs.getString(i++);
				String characteristicId = rs.getString(i++);
				String ordering= rs.getString(i++);
				String value= rs.getString(i++);
				
				
				//DataStore_student.getInstance().putCharacteristic( rs.getString(i++) ,  rs.getString(i++) , rs.getString(i++));
				
				
				if( ordering == null){
					
					System.out.println("NO ORDERING FOUND FOR { pid "+ participantId+ "  characteristicId:" + characteristicId + "  value:" +  value + "}");
					
				}
				else{
					
					dataStoreStudent.addStudentCharacteristic( participantId, characteristicId, ordering);
				}
				
				
			}
			
			//Utils.printResultSet(rs);
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			
			if(debugPrint)
				System.out.println("got all participant characteristics1");
		}
	}
	
	
	
	//part2, where ordering is already in participant characteristic
	public void getAllParticipantInfo2( String surveyInstanceId ){
		
		
				
		//#match participant characteristic value to characteristicResponseLang ordering, with exceptions
		String qry2 = "Select distinct "
				+ " Answer4Rpt2016.surveyParticipantId ,"
				+ "	ParticipantCharacteristic.characteristicId ,"
				+ "	ParticipantCharacteristic.value "
				
				+ "	From ParticipantCharacteristic "
				+ "	join Answer4Rpt2016 on ParticipantCharacteristic.surveyParticipantId = Answer4Rpt2016.surveyParticipantId "
				+ "	Where Answer4Rpt2016.surveyInstanceId= ? "
				+ " and ParticipantCharacteristic.characteristicId in (1)"
				+ " group by Answer4Rpt2016.surveyParticipantId , ParticipantCharacteristic.characteristicId;";
		try {
			
			PreparedStatement stmt = conn.prepareStatement(qry2);
			stmt.setInt(1, Integer.parseInt(surveyInstanceId) );
			ResultSet rs = stmt.executeQuery();
			
			
			while (rs.next()){
				int i = 1;
				
				//DataStore_student.getInstance().putCharacteristic( rs.getString(i++) ,  rs.getString(i++) , rs.getString(i++));
				String participantId  =  rs.getString(i++);
				String characteristicId = rs.getString(i++);
				String value = rs.getString(i++);
				
				
				if( characteristicId.equals("1") ){

					int cv = Integer.parseInt(value);
					cv+=2;
					value = Integer.toString(cv);
					
				}
				
				dataStoreStudent.addStudentCharacteristic( participantId, characteristicId , value );
				
			}			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if( debugPrint)
				System.out.println("got all participant characteristics2");
		}
	}
	
	

	
	
	
	
	
	
	
	
	
	
	/*
	 * unused, just for reference
	 */
	public void getAllParticipantInfoXXX( String surveyInstanceId ){
		
				
		String qry2 = ""
				+ "Select distinct "
				+ " Answer4Rpt2016.surveyParticipantId ,"
				+ "	ParticipantCharacteristic.characteristicId ,"
				+ "	ParticipantCharacteristic.value "
				+ "	From ParticipantCharacteristic "
				+ "	join Answer4Rpt2016 on ParticipantCharacteristic.surveyParticipantId = Answer4Rpt2016.surveyParticipantId "
				+ "	Where Answer4Rpt2016.surveyInstanceId= ? "
				+ " group by Answer4Rpt2016.surveyParticipantId , ParticipantCharacteristic.characteristicId;";
		
		try {
			PreparedStatement stmt = conn.prepareStatement(qry2);
			stmt.setInt(1, Integer.parseInt(surveyInstanceId) );
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()){
				//cutoff = rs.getFloat(1);
				//int i = 1;
				//DataStore_student.getInstance().putCharacteristic( rs.getString(i++) ,  rs.getString(i++) , rs.getString(i++));	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			System.out.println("got all participant characteristics");
		}
	}
	

}
