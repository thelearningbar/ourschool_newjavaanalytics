package utilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class LoggingInfo 
{
	
	private static Logger logger = LogManager.getLogger("GLOBAL LOG");
	private static String methodName="";
	private static String exceptionName="";
	//enum Level {Error, Warn, Fatal, Info, Debug}
	
	private static String[] levelArray = {"Error","Warn","Fatal","Info","Debug"};

    private LoggingInfo() 
    {
    	
    }

    public static void logError(String className, String msg, String surveyInstanceId) 
    {
    	
        log(levelArray[0], className, msg, null, surveyInstanceId);
        
    }

    public static void logWarn(String className, String msg, String surveyInstanceId) 
    {
    	
    	log(levelArray[1], className, msg, null,surveyInstanceId);
    	
    }

    public static void logFatal(String className, String msg, String surveyInstanceId) 
    {
    	
        log(levelArray[2], className, msg, null,surveyInstanceId);
        
    }

    public static void logInfo(String className, String msg, String surveyInstanceId) 
    {
    	
        log(levelArray[3], className, msg, null,surveyInstanceId);
        
    }

    public static void logDebug(String className, String msg, String surveyInstanceId) 
    {
    	
        log(levelArray[4], className, msg, null,surveyInstanceId);
        
    }


    public static void logError(String className, String msg, Throwable throwable, String surveyInstanceId) 
    {
    	
        log(levelArray[0], className, msg, throwable,surveyInstanceId);
        
    }


    public static void logWarn(String className, String msg, Throwable throwable, String surveyInstanceId) 
    {
    	
        log(levelArray[1], className, msg, throwable,surveyInstanceId);
        
    }

    public static void logFatal(String className, String msg, Throwable throwable, String surveyInstanceId) 
    {
    	
        log(levelArray[2], className, msg, throwable,surveyInstanceId);
        
    }

    public static void logInfo(String className, String msg, Throwable throwable, String surveyInstanceId) 
    {
    	
        log(levelArray[3], className, msg, throwable,surveyInstanceId);
        
    }

    public static void logDebug(String className, String msg, Throwable throwable, String surveyInstanceId)
    {
    	
        log(levelArray[4], className, msg, throwable,surveyInstanceId);
        
    }

    private static void log(String level, String className, String msg, Throwable throwable, String surveyInstanceId) 
    {
    	
    	if(throwable!=null) 
    	{	
    		
	    	methodName="."+setErrorDetails(throwable);
	    	exceptionName = getExceptionName(throwable);
	    	msg=msg+" LineNo.";
	    	
    	}
    	className=className.replace("class","SurveyInstance:"+surveyInstanceId);
    	
    	String message = className+methodName+exceptionName+" "+msg;
        
        switch (level) 
        {
        
            case "Info":
                logger.info(message, throwable);
                break;
                
            case "Warn":
                logger.warn(message, throwable);
                break;
                
            case "Error":
                logger.error(message, throwable);
                break;
                
            case "Fatal":
                logger.fatal(message, throwable);
                break;
           
            case "Debug":
                logger.debug(message, throwable);
                
        }
        
        methodName ="";
        exceptionName="";
        
    }
    
    private static String setErrorDetails(final Throwable cause)
    {
    	
         Throwable rootCause = cause;
         while(rootCause.getCause() != null &&  rootCause.getCause() != rootCause)
              rootCause = rootCause.getCause();

        return rootCause.getStackTrace()[0].getMethodName(); 
        
    }
    
    public static String getExceptionName(Throwable e)
    {
    	
    	return " "+e.getClass().getCanonicalName();
    	
    }

}