package utilities;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class TempTableCreator {
	
	Connection conn;

	public TempTableCreator( Connection conn ) {
		this.conn = conn;
	}
	
	
	public void createTempAnswerTable( String surveyInstanceId ){
		String qry = "create temporary table Answer4Rpt2016_temp as "
				+ "select * from Answer4Rpt2016 where surveyInstanceId = " 
				+ surveyInstanceId;
		
		Statement stmt;
		
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(qry);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			System.out.println("created temporary Table Answer4Rpt2016_temp");
		}
	}
	

	
	public void dropTempAnswerTable(  ){
		String qry = "drop table Answer4Rpt2016_temp  " ;
		
		Statement stmt;
		
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(qry);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			System.out.println("deleted temporary Table Answer4Rpt2016_temp");
		}
	}
	
	
	
/*
	public void saveMeasureInfo(String measureId){

		String qry = "select cutoff, minimumAnswersRequired from Measure where id = " + measureId;
		Statement stmt;
		
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(qry);
			
			while (rs.next()){
				int i=1;
				String cut = rs.getString(i++);
				String minAns = rs.getString(i++);
				measureCutoff.put(measureId, cut);
				measureMinimumAnswer.put(measureId, minAns);
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			
		}
	}
	*/

}
