package utilities;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 * Gets(finds) information about a survey instance
 * 
 * 
 * @author Evan K
 *
 */
public class SurveyInfoFinder {
	
	Connection conn;
	

	public SurveyInfoFinder( Connection conn ) {
		this.conn=conn;
	}
	
	
	public List<String> getSurveyMeasures( String surveyInstanceId){
		
		String qry = "SELECT measureId FROM SurveyInstance as si "
				+ "join SurveyDefinitionMeasure as sdm "
				+ "on si.surveyDefinitionId = sdm.surveyDefinitionId where si.id = " + surveyInstanceId;
		
		Statement stmt;
		List<String> result = new ArrayList<String>();
		
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(qry);
			while (rs.next()){
				result.add( rs.getString(1) );				
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			
		}
		
		return result;
		
	}
	
	
	/**
	 * Get organizational unit Id of a survey instance
	 * @param surveyInstanceId
	 * @return
	 */
	public String getOuId( String surveyInstanceId){

		String qry = "SELECT orgId FROM evan_mamun_test.SurveyInstance where id = " + surveyInstanceId;
		Statement stmt;
		
		String result = "0";
		
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(qry);
			
			while (rs.next()){
				int i=1;
				result = rs.getString(i++);				
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			
		}
		
		return result;
		
	}

	
	/**
	 * Get wether this is the survey of a primary or seconday school
	 * 1 primary  4 secondary
	 * @param surveyInstanceId
	 * @return integer value as string, 1=primary 4=secondary
	 */
	public String getSchoolType ( String surveyInstanceId ){
		
		
		String qry = 
				"SELECT targetTypeCd "
						+ "FROM SurveyInstance as si "
						+ "join SurveyDefinition as sd on si.surveyDefinitionId = sd.id "
						+ "where si.id = " + surveyInstanceId;
		Statement stmt;
		String result = "0";
		
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(qry);
			
			while (rs.next()){
				int i=1;
				result = rs.getString(i++);				
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			
		}
		
		return result;
		
	}

	

}
