package utilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Saves final results to database
 */
public class DataSaver {

	DataStore_student dataStoreStudent;
	DataStore_school dataStoreSchool;
	Connection conn;
	
	boolean saveStudentToDB = false;
	
	boolean saveSchoolToDB = false;
	
	private boolean debugPrint = false;
	
	
	public DataSaver(Connection conn , DataStore_student dataStoreStudent , 	DataStore_school dataStoreSchool ) {
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
	}
	
	
	
	public void saveData(  String surveyInstanceId , String ouId,  boolean savestudent , boolean saveschool ){
		saveStudentToDB = savestudent;
		saveSchoolToDB = saveschool;
		
		saveSchoolLevel(surveyInstanceId , ouId );
		saveStudentLevel(surveyInstanceId);
	}
	
	
	
	
	//can't update on unique key conflict because only 16 keys are allowed
	//just delete the data
	public void deleteDataForSurveyMeasure( String surveyInstanceId , String measureId ){
		String qry = "delete from evan_mamun_test.CharacteristicData "
				+ "where surveyInstanceId = ? "
				+ "and measureId=?";
		PreparedStatement stmt = null;
		
		try {
			if( conn == null){
				System.out.println("no connection");
			}
			stmt = conn.prepareStatement(qry);
			
			int x=1;
			stmt.setInt(x++, Integer.parseInt(surveyInstanceId) );
			stmt.setInt(x++, Integer.parseInt(measureId) );
			
			stmt.executeUpdate(); 
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			
		}
	}
	
	
	
	/*
	 * Avoids writing null, 0 instead
	 */
	private void saveSchoolLevel( String surveyInstanceId , String orgId ){
				
		HashMap<  HashMap<String,String>, HashMap<String, List<String>>   > characteristicResults = 
				dataStoreSchool.getCharacteristicResults();
		
		
		PreparedStatement stmt = null;
		
		//map  characteristic id to a position
		int[] characteristicName = {	
				1 , 2, 3,21,22,
				23,24,25,35,41,
				42,43,49,50,52,
				53,54,55,56,60
				};

		String qry = "Insert into evan_mamun_test.CharacteristicData ( "+
				  "ouId,"
				  + " surveyInstanceId,"
				  + "measureId,"
				  + "  populationActive,"
				  + "  populationPassed,"
				  + "  populationSkipped,"
				  + "  populationNotEnoughAnswered,"
				  + "  scoreActivePopulation,"
				  + "  scorePassedPopulation,"
				  
				  + "  c_Gender,"
				  + "  c_Grade,"
				  + "  c_Age,"
				  + "  c_LangAtHome_I,"
				  + "  c_Immigrant_I,"
				  
				  + "  c_Aboriginal_I,"
				  + "  c_GradeRepetition,"
				  + "  c_Ethnic,"
				  + "  c_ABIaboriginal,"
				  + "  c_LangAtHome_II,"
				  
				  + "  c_Immigrant_II,"
				  + "  c_Aboriginal_II,"
				  + "  c_SEScat3,"
				  + "  c_FNMI,"
				  + "  c_ChangeSchool,"
				  
				  + "  c_FI,"
				  + "  c_SpecialEducation,"
				  + "  c_FrenchImmersion,"
				  + "  c_Disability,"
				  + "  c_LangAtHome_III"
								
				+ ") values ("
				+ "?,?,?, ?,?,?, ?,?,?,"
				+ "?,?,?,?,?,"
				+ "?,?,?,?,?,"
				+ "?,?,?,?,?,"
				+ "?,?,?,?,?"
				+ ");";
		
		HashSet<String> measuresCovered = new HashSet<String>();
		
		try {
			
			if( conn == null){
				System.out.println("no connection");
			}
			
			
			stmt = conn.prepareStatement(qry);
			
			conn.setAutoCommit(false);
			
			if( debugPrint)
				System.out.println("\n Final Insert Statements");
			
			
			Set<String> allCharacteristics = dataStoreStudent.getAllCharacteristicIds();

			//For each combination of characteristics
			for (HashMap<String,String>  characteristicCombo  : characteristicResults.keySet()) {
				
				HashMap<String, List<String>> dataValues = characteristicResults.get(characteristicCombo);

				//for each data entry within combo
				for( String measureId : dataValues.keySet() ){
					
					List<String> values =  dataValues.get(measureId);	
					
					int ouId =    Integer.parseInt(orgId);
					int survey =  Integer.parseInt(surveyInstanceId);
					int measure = Integer.parseInt(measureId);
										
					int activeStudents = 			Integer.parseInt(values.get(0));
					int passedStudents = 			Integer.parseInt(values.get(1));
					int studentsNoQuestionsAnswered = Integer.parseInt(values.get(2));
					int studentsNotEnoughQuestionsAnswered = Integer.parseInt(values.get(3));
					
					float activeStudentsScore= Float.parseFloat(values.get(4));
					float passedStudentScore = Float.parseFloat(values.get(5));	
					
					
					int x=1;	
					stmt.setInt(x++, ouId );
					stmt.setInt(x++, survey );
					stmt.setInt(x++, measure );
					
					stmt.setInt(x++, activeStudents );
					stmt.setInt(x++, passedStudents );
					
					stmt.setInt(x++, studentsNoQuestionsAnswered );
					stmt.setInt(x++, studentsNotEnoughQuestionsAnswered );
					
					stmt.setFloat(x++, activeStudentsScore );
					stmt.setFloat(x++, passedStudentScore );
					
					
					for( int i=0; i< characteristicName.length; i++){
						
						String charId = Integer.toString( characteristicName[i] );
						
						if( characteristicCombo.containsKey( charId ) )	{
							//contains characteristic
							
							
							stmt.setInt( x++, Integer.parseInt( characteristicCombo.get( Integer.toString( characteristicName[i] )) ) );
							
						}
						else{
							
							if( allCharacteristics.contains( charId ) ){
								//for this particular combo characteristic was null
								
								stmt.setInt( x++, 1 );
								
							}
							else{
								//within the entires survey, there is no data for this characteristic
								
								stmt.setInt( x++, 0 );
							}
														
						}
						
						
						
					}
					
					
					//stmt.setFloat(x++, CharacteristicDatacol );
					
					
					if( saveSchoolToDB ){
						
						if( !measuresCovered.contains(measureId) ){
							measuresCovered.add(measureId);
							
							deleteDataForSurveyMeasure( surveyInstanceId , measureId );
						}
						
						//System.out.println(stmt.toString());
						
						stmt.executeUpdate(); 
						
					}
					else{
						if(debugPrint)
							System.out.println(stmt.toString());
					}
				}
				
				
			}
			
			conn.commit();
			conn.setAutoCommit(true);
			
			if( saveSchoolToDB ){
				System.out.println("saved school level");
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
			
			
		}
		finally{
			
		}
	}
	
	
	
	
	
	
	

	private void saveStudentLevel(  String surveyInstanceId ){
		PreparedStatement stmt = null;
		String qry="insert into StudentLevelScores ("
				+ "surveyParticipantId,"
				+ "surveyInstanceId,"
				+ "measureId,"
				+ "score,"
				+ "questionsAnswered,"
				+ "minimumAnswersSupressionFlag "
				+ ") values ( ?,?,?  ,  ?,?,? ) "
				+ "on duplicate key update "
				+ "score=values(score),"
				+ "questionsAnswered=values(questionsAnswered),"
				+ "minimumAnswersSupressionFlag=values(minimumAnswersSupressionFlag)"
				+ ";";
		
		try {
			
			
			if( conn == null){
				System.out.println("no connection");
			}
			
			conn.setAutoCommit(false);
			
			stmt = conn.prepareStatement(qry);
			//generate insert statements
			
			//for each student
			for (String surveyParticipantId : dataStoreStudent.getAllStudents() ) {
				
				//for each measure of student
				
				HashMap<String,List<String>> studentResults= dataStoreStudent.getStudentResults(surveyParticipantId);
				
				for(String measureId : studentResults.keySet() ){
					
					List<String> studentData = studentResults.get(measureId);
					
					float score = Float.parseFloat( studentData.get(0) );
					int questionsAnswered = Integer.parseInt( studentData.get(1) );
					int minimumAnswersSupressionFlag = Integer.parseInt( studentData.get(2) );
					
					int i=1;
					stmt.setInt(i++, Integer.parseInt(surveyParticipantId));
					stmt.setInt(i++, Integer.parseInt(surveyInstanceId));
					stmt.setInt(i++, Integer.parseInt(measureId));
					stmt.setFloat(i++, score);
					stmt.setInt(i++, questionsAnswered);
					stmt.setInt(i++, minimumAnswersSupressionFlag);
					
					//System.out.println(stmt.toString());
					if( saveStudentToDB ){
						stmt.executeUpdate(); 
						
						//System.out.println("student data saved");
					}
				}
			}
			
			conn.commit();
			conn.setAutoCommit(true);
			
			if( saveSchoolToDB ){
				System.out.println("saved student level");
			}

			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			
		}
		
		//System.out.println("\nSaved all student level measure info");
		
	}
	
	
	
	
	
	
	
	
	
	
	
}
