package utilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class DataStore_student {
	
	private  boolean debugPrint = false;
	
	
	//measures which have already been computed for students
	private  ArrayList<String> computedMeasures;
	
	//data on each individual survey participant
	private   HashMap<String,  Student > studentDetails ; 
	

	
	public DataStore_student() {
		studentDetails = new  HashMap<String,  Student >();
		
		computedMeasures = new ArrayList<String>();
				
		if( debugPrint)
			System.out.println("DataStore_student created");
		
	}
	
	
	public void clearAllData(){
		computedMeasures.clear();
		studentDetails.clear();
		
	}
	
	
	
	public int getNumStudents(){
		return studentDetails.keySet().size();
	}
	
	public HashMap<String,String> getStudentCharacteristics( String participantId ){
		return studentDetails.get( participantId ).getAllCharacteristics();
	}
	
	public  HashMap<String,List<String>> getStudentResults( String participantId ){
		if( !studentDetails.containsKey(participantId) ){
			System.out.println("user not found");
		}
		return studentDetails.get(participantId).getAllResults();
	}
		
	
	/**
	 * Shallow copy of studentDetails keyset  but as a list
	 * @return
	 */
	public  List<String> getAllStudents(){
		List<String> students = new ArrayList<String>();
		for( String x  : studentDetails.keySet()){
			students.add(x);
		}
		return students;
	}
	
	
	
	
	public void addNewStudent( String participantId , HashMap<String,String> characteristics , HashMap<String,List<String>> result){
		Student student = new Student(  participantId , characteristics , result );
		studentDetails.put( participantId , student);		
	}
		
	
	public void addStudentCharacteristic( String participantId , String characteristicId, String characteristicValue ){

		if( studentDetails.containsKey(participantId)){
			studentDetails.get(participantId).addCharacteristic(characteristicId, characteristicValue);
		}	
		else{
			HashMap<String,String> characteristics =  new HashMap<String,String>();
			characteristics.put(characteristicId, characteristicValue);
			addNewStudent( participantId , characteristics , null );
		}
	}
	
	
	public void addStudentResult( String participantId , String measureId, List<String> result ){
		//System.out.println( "addStudentResult");
		if( studentDetails.containsKey(participantId)){
			studentDetails.get(participantId).addResult(measureId, result);
		}	
		else{
			HashMap<String,List<String>> resultt = new HashMap<String,List<String>> ();
			resultt.put(measureId, result);
			addNewStudent( participantId , null , resultt );
		}
	}
	
	
	public void addNewStudent(  Student student ){
		System.out.println( "addStudent");
		studentDetails.put( student.getParticipantId() , student);
	}
			

	
	
	
	
	/*
	 * sorting of students using fewer passes
	 */
	public HashMap< HashMap<String,String> , List<String >>  sortUsersByCharacteristics( 
			List<HashMap<String,String>> combinations ){
		
		HashMap< HashMap<String,String> , List<String >> students = new HashMap< HashMap<String,String> , List<String >>();
		HashMap< HashMap<String,String> , List<String >> finalMap = new HashMap< HashMap<String,String> , List<String >>();
		
		
		for( String participantId : studentDetails.keySet() ){
			Student s =  studentDetails.get(participantId);
			HashMap<String,String> studentCharacteristics = s.getAllCharacteristics();
			if( students.containsKey(studentCharacteristics)){
				students.get(studentCharacteristics).add(participantId);
			}
			else{
				List<String> newList = new ArrayList<String>();
				newList.add(participantId);
				students.put(studentCharacteristics, newList);
			}
		}
		
		
		//for each combination of characteristics
		for( HashMap<String,String> combo : combinations ){
			
			//for each set of student characteristics
			for ( Iterator< HashMap<String,String>> i = students.keySet().iterator(); i.hasNext();) {
					
				HashMap<String,String> element = i.next();
				int numCharacteristic =0;
								
				for( String searchKey: combo.keySet()){
					
					String searchVal = combo.get(searchKey);
					
					if( element.containsKey(searchKey) ){
						if( element.get(searchKey).equals(searchVal) ){
							numCharacteristic++;
						}
					}
					else{
						//handle any kind of null conditions here
						if( searchVal.equals("0")){
							numCharacteristic++;
						}
					}
				}
				
				if( numCharacteristic >= combo.keySet().size() ){
					if( finalMap.containsKey(combo)){
						List<String> lis = finalMap.get(combo);
						lis.addAll( students.get(element));
					}
					else{
						finalMap.put(combo, students.get(element));
					}
				}
				
			}
			
		}
		
		System.out.println(finalMap.toString());
				
		return finalMap;
	}
	
	
	

	/*
	 * Find any users with characteristic from scratch
	 * 
	 * helper for    saveAnyUsersWithCharacteristic
	 */
	public  List<String> getUsersByCharacteristics( HashMap<String,String> characteristicCombination ){
		List<String> users = filterUsers( characteristicCombination );
		return users;
	}
	
	
	private  List<String> filterUsers( HashMap<String,String> searchCombo  ){
		
		List<String> finalList =  getAllStudents();//shallow copy
		
		
		for( String participantId : studentDetails.keySet() ){

			Student student = studentDetails.get(participantId);
			
			HashMap<String, String> combo = student.getAllCharacteristics();
			
			int numCharacteristic =0;	
			
			//for each characteristic
			for( String  combinationKey: searchCombo.keySet()){
				
				String searchVal = searchCombo.get( combinationKey );
				
				//check if user has all characteristics
				if( combo.containsKey(combinationKey) ){
					if( combo.get(combinationKey).equals(searchVal) ){
						numCharacteristic++;
					}
				}
				else{
					//handle any kind of null conditions here
					if( searchVal.equals("1")){
						numCharacteristic++;
					}
				}
			}
			
			if( numCharacteristic < searchCombo.keySet().size() ){
				finalList.remove(participantId);
			}
			else{
				
			}
		}
		return finalList;
	}
	
	
	
	/*
	 * for test only,   use to print out participants aggregated by some combination
	 */
	public  void aggregateUsers( HashMap<String,String> searchCombo  ){
		
		//System.out.println(studentsByCharacteristic.toString());
		
		//printStudents();
		
		List<String> finalList =  getAllStudents();
		
		//List<String> users = new ArrayList<>(  );
		
		for( String participantId : studentDetails.keySet() ){

			Student student = studentDetails.get(participantId);
			
			HashMap<String, String> combo = student.getAllCharacteristics();
			
			int numCharacteristic =0;	
			
			//for each characteristic
			for( String  combinationKey: searchCombo.keySet()){
				
				String searchVal = searchCombo.get( combinationKey );
				
				//check if user has all characteristics
				if( combo.containsKey(combinationKey) ){
					if( combo.get(combinationKey).equals(searchVal) ){
						numCharacteristic++;
					}
				}
				else{
					//handle any kind of null conditions here
					if( searchVal.equals("1")){
						numCharacteristic++;
					}
				}
			}
			
			if( numCharacteristic < searchCombo.keySet().size() ){
				finalList.remove(participantId);
			}
			else{
			}
		}
		
		System.out.println( "++++++++");
		for( String s : finalList){
			System.out.println(s);
		}
		System.out.println( "++++++++");
	}


	
	public Set<String> getAllCharacteristicIds(){
		
		HashSet <String> set =  new HashSet <String>();
		
		
		//for all students
		for( String participantId : studentDetails.keySet() ){

			Student student = studentDetails.get(participantId);
			HashMap<String,String> characteristics  = student.getAllCharacteristics();
			
			for( String characteristicId : characteristics.keySet() ){
				set.add(characteristicId);
			}
			
		}
				
		return set;
	}

	
	
	public  Map<Integer, int[]> getPossibleCharacteristics(){
		
		//linked hashmap to preserve order
		Map<Integer, HashSet<Integer> > map0 =new LinkedHashMap<Integer, HashSet<Integer> > ();
		
		Map<Integer, int[]> map = new LinkedHashMap<Integer, int[]>(); //was just hashmap
		
		
		//for all students
		for( String participantId : studentDetails.keySet() ){

			Student student = studentDetails.get(participantId);
			
			HashMap<String,String> characteristics  = student.getAllCharacteristics();
			
			for( String characteristicId : characteristics.keySet() ){
				String val = characteristics.get(characteristicId);
				int numVal = Integer.parseInt(val);
				
				
				if( map0.containsKey( Integer.parseInt(characteristicId) )   ){
					HashSet<Integer> aaa = map0.get( Integer.parseInt(characteristicId));
					if( aaa != null)
						aaa.add( numVal ) ;
				}
				else{
					HashSet<Integer> set = new HashSet<Integer>();
					
					//ensure each set has a 1 (ie skipped) condition
					set.add(numVal);
					set.add(1); 
					
					map0.put(  Integer.parseInt(characteristicId) , set);
					//System.out.println(set.toString());	 
				}
			}
		}
		
		for( Integer characteristicId : map0.keySet() ){
			HashSet<Integer> vals = map0.get(characteristicId);
			int[] arr = new int[vals.size()];
			int i=0;
			for( int x: vals){
				arr[i] = x;
				i++;
			}	
			map.put(characteristicId, arr);
		}		
				
		if( debugPrint ){
			System.out.println( "\npossible values for student characteristics:" );
			System.out.println( map0.toString());
		}
		
		return map;
	}

	
	
	
	//------------------------------------------------------------------
	//------------------------------------------------------
	//Prints	
	
	public  void printStudents(){
		System.out.println("Students :  {");
		
		for( String s : studentDetails.keySet() ){
			System.out.println( studentDetails.get(s).toString() );
		}
		
		System.out.println("} students  end ");
	}

	public  void printStudents2(){
		System.out.println("Students :  {");
		
		for( String s : studentDetails.keySet() ){
			System.out.println( studentDetails.get(s).results.toString() );
		}
		
		System.out.println("} students  end ");
	}
	//end Prints
	//------------------------------------------------------
	//------------------------------------------------------------------------
	
	
	
	

	public  void setStudentLevelMeasureComputed(String measureId){
		computedMeasures.add(measureId);
	}
	
	public  boolean isComputed(String measureId){
		return computedMeasures.contains(measureId);
	}
	
	
	public boolean studentHasResultsForMeasure(  String participantId , String measureId  ){
		boolean ret = false;
		
		if( studentDetails.containsKey(participantId) ){
			ret =  studentDetails.get(participantId).hasMeasure(measureId);
		}
		
		return ret;
	}
	
	public String getStudentResultsMeasureValue(  String participantId , String measureId , int index ){
		return studentDetails.get(participantId).getResult(measureId).get(index);
	}

	public Set<String> getStudentMeasuresComputed( String participantId ){
		return studentDetails.get(participantId).getMeasuresComputed();
	}
	
	public boolean containsStudent( String participantId ){
		return  studentDetails.containsKey(participantId);		
	}

	
	
	
	public class Student{
		
		private String participantId;
		private HashMap<String,String> characteristics;
		private HashMap<String,List<String>> results;
		
		
		/**
		 * 
		 * @param participantId
		 * @param characteristics
		 * @param results
		 */
		public Student( String participantId , HashMap<String,String> characteristics , HashMap<String,List<String>> results  ){
			
			this.participantId = participantId;
			
			if( characteristics!=null ){
				this.characteristics = characteristics;
			}
			else{
				this.characteristics =  new HashMap<String,String>();
			}
			
			if( results!=null ){
				this.results=results;
			}
			else{
				this.results = new HashMap<String,List<String>>();
			}
			
		}
		
		public String toString(){
			return "" + participantId + " " 
					+ characteristics.toString() + " "
					+ results.toString() ;
		}
		
		
		public void addResult(String measureId , String v1 , String v2 , String v3 ){
			List<String> vals = new ArrayList<String>();
			vals.add(v1);
			vals.add(v2);
			vals.add(v3);
			
			results.put(measureId, vals);
		}
		

		public void addResult(String measureId , List<String> result ){			
			results.put(measureId, result);
		}
		
		public HashMap<String,List<String>> getAllResults(){
			return results;
		}
		
		public List<String> getResult( String measureId ){
			if( results != null){
				
				return results.get(measureId);
			}
			else{
				return null;
			}
		}
		
		public boolean hasMeasure (String measureId){
			if( results != null){
				return results.containsKey(measureId);
			}
			else{
				return false;
			}
			
		}
		
		public Set<String> getMeasuresComputed(){
			return results.keySet();
		}
		
		public void addCharacteristic( String characteristicId , String characteristicValue ){
			characteristics.put(characteristicId, characteristicValue);
		}
		
		
		public String getCharacteristic(  String characteristicId ){
			return characteristics.get(characteristicId);
		}
		
		public HashMap<String,String> getAllCharacteristics(){
			return characteristics;
		}
		
		public String getParticipantId(){
			return participantId;
		}
		
	}
	
	
	
	
	
	
	
	
	
	
}






