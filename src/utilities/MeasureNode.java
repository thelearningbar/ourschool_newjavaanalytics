package utilities;

public class MeasureNode {

	public String measureName;
	public int computation, reporting;
	public int ID, parentID;

	public MeasureNode child, sibling;

	public MeasureNode() {
	}

	public MeasureNode(int computation, int reporting, int ID, int parentID) {
		this.computation = computation;
		this.reporting = reporting;

		this.ID = ID;
		this.parentID = parentID;

	}

	public MeasureNode(String measureName, int computation, int reporting, int ID, int parentID) {
		this.measureName = measureName;
		this.computation = computation;
		this.reporting = reporting;

		this.ID = ID;
		this.parentID = parentID;

	}

}
