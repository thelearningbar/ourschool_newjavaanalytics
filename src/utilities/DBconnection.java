package utilities;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBconnection {

	
	//BRANDING SERVER
	public String USER = "evan";
	public String PASS = "kp0pTwac!";
		
	//Choose DATABASE SCHEMA
	//public String DB_URL = "jdbc:mysql://10.1.1.5/tlb_branding";
	public String DB_URL = "jdbc:mysql://10.1.1.5/evan_mamun_test";
	
	
	/*
	//SLAVE PRODUCTION DATABASE
	public String USER = "bench";
	public String PASS = "!Q@W3e4r";
	//public String DB_URL = "jdbc:mysql://127.0.0.1:54321/tlb";
	public String DB_URL = "jdbc:mysql://209.15.241.53/tlb";
	//
	*/
	
	
	public String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	
	Connection conn = null;
	
	
	
	public DBconnection(){
		connect();
	}
	
	public Connection getConnection(){
		return conn;
	}
	
	public void connect(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL,USER,PASS);
			Utils.pr("connected");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void close(){
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			conn = null;
		}
	}

	
	
}
