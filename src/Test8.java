import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import algorithms.Population.ASumPopulationFromQuestionFilterPie;
import algorithms.Population.ASumPopulationFromSubmeasuresAndCutoff;
import utilities.DBconnection;
import utilities.MeasureArchitecture;
import utilities.MeasureTree;

public class Test8 {

	
	/**
	 * test individual algorithms
	 * 
	 */
	public Test8() {
		DBconnection db= new DBconnection();
		
		Connection conn = db.getConnection();
		
		
		
		
		int surveyInstanceId = 76318;
		
		int measureId = 1412;
		
		int filterQuestionId = 3552;
		
		/*
		ASumPopulationFromQuestionFilterPie pie = new ASumPopulationFromQuestionFilterPie(conn);
		pie.algo_ASumPopulationFromQuestionFilterPie_measuresA(measureId, surveyInstanceId, filterQuestionId);
		*/
		/*
		ASumPopulationFromSubmeasuresAndCutoff pop = new ASumPopulationFromSubmeasuresAndCutoff(conn);
		pop.algo_ASumPopulationFromSubMeasuresAndCutoff_measuresA(measureId, surveyInstanceId, 3.1);
		*/
		
		MeasureArchitecture arch = MeasureArchitecture.getInstance();
		
		/*
		arch.showTree(1694);
		
		arch.showTree(1771);
		*/
		
		
		//List<Integer> measures = Arrays.asList(1752,1753);
		
		List<Integer> measures = new ArrayList();;
		
		List<Integer> xx = arch.getComputingMeasure(measureId, measures);
		System.out.println(xx.toString());
		
		
		ASumPopulationFromQuestionFilterPie xxx = new ASumPopulationFromQuestionFilterPie(conn);
		xxx.compute(Integer.toString(measureId), Integer.toString(surveyInstanceId));
		
		//arch.getComputingMeasure(measureId, computingMeasure)
		
	}

}
