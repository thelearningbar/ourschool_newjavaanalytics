package algorithms.Alg1_student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;


public class Alg1Custom implements IAlg1{
	
	Connection conn = null;
	
	public String query;
	public ProcessRow ans;

	public Alg1Custom(Connection conn) {
		this.conn=conn;
		
		query = " Select" +
				" a.surveyParticipantId as sid,   " +
				" SUM((?*(lad.value-1))/(mq.optionNum-1))/count(a.questionId)  as score, " +
				" if (COUNT( a.questionId ) >= m.minimumAnswersRequired, 1,0) as flag , " +
				" count(a.questionId) as countQuestion" +
				" From Answer4Rpt2016 As a" +
				" Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId" +
				" Inner Join MeasureQuestion As mq On mq.questionId = a.questionId" +
				" And mq.measureId = ?" +
				" Inner Join Measure As m On m.id = mq.measureId" +
				" Where a.surveyInstanceId= ?" +
				" Group By a.surveyParticipantId";
	}
	
	
	public void computeResults(int measureId, int surveyInstance, int scaleFactor ,  String reserve1 ) {
		ans = fetchRowsAndProcess(scaleFactor,measureId,surveyInstance, query , new DefaultRowProcessor() );
	}
	
		
	/**
	 * Run to use a custom behavior on rows
	 * or
	 * Follow same steps for similar behavior
	 * 
	 * @param scaleFactor
	 * @param measureId
	 * @param surveyInstanceId
	 * @param query
	 * @param rowProcess
	 * @return
	 */
	public ProcessRow fetchRowsAndProcess( int scaleFactor, int measureId, int surveyInstanceId , String query , ProcessRow rowProcess ){
		pr( "AlgorithmId_1   MeasureId: " + measureId + "  SurveyInstanceId: "	+ surveyInstanceId );		
		PreparedStatement ps = prepareQueryStatement( scaleFactor, measureId , surveyInstanceId , query );
		forEachRow( ps , rowProcess);
		return rowProcess;
	}
	
	
	public PreparedStatement prepareQueryStatement( int scaleFactor, int measureId, int surveyInstanceId , String query ){
		PreparedStatement stmt = null;
		try {
			int i=1;
			stmt = conn.prepareStatement(query);
			stmt.setInt(i++, scaleFactor);
			stmt.setInt(i++, measureId);
			stmt.setInt(i++, surveyInstanceId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return stmt;
	}
	
	public void forEachRow( PreparedStatement ps , ProcessRow rowProcess){
		try {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int i=1;
				int surveyParticipantId = rs.getInt(i++);
				double score = rs.getDouble(i++);
				int flag = rs.getInt(i++);
				int countQuestion = rs.getInt(i++);
				rowProcess.process(surveyParticipantId, score, flag, countQuestion);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	//--------------------------------------------------
	
	/*
	 * Return type is stored inside the ProcessRow object
	 * 
	 * Implement this to modify row processing
	 * 
	 * Like Strategy pattern, but strategies are stored within the class
	 */
	interface ProcessRow{
		public void process( int surveyParticipantId,double score ,int flag,int countQuestion );
	}
	
	/*
	 * Modify ProcessRow objects to add new return types (need to get the data out of these
	 * objects after by yourself though)
	 * 
	 * See Double Dispatching
	 * (Function returns void, but accepts an object to put response into)`
	 */
	public class DefaultRowProcessor implements ProcessRow{
		
		//Change this to change return type
		Map<Integer, Double> surveyPaticipantScoreSubM;
		
		public DefaultRowProcessor(){
			surveyPaticipantScoreSubM = new TreeMap<Integer, Double>();
		}
		
		//use to get results out
		public Map<Integer, Double> getMap(){
			return surveyPaticipantScoreSubM;
		}
		
		public void process(int surveyParticipantId, double score, int flag, int countQuestion) {
			if(flag==1)
				surveyPaticipantScoreSubM.put(surveyParticipantId, score); // Computing this measure only
		}
	}
	
	
	//-----------------------------------------------
	

	public ProcessRow getAns(){
		return ans;
	}
	
	public static void pr( String s){
		System.out.println(s);
	}



}
