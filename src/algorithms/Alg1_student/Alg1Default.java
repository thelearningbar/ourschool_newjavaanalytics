package algorithms.Alg1_student;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import utilities.DataStore_student;


/**
 * 
 * Studnt level query
 * 
 * @author Evan K
 *
 */
public class Alg1Default implements IAlg1 {
	
	
	DataStore_student dataStoreStudent;
	Connection conn = null;
	
	
	public boolean useTemporaryTable = true;
	public boolean debugPrint = false;
	
	private String query;	
	
	
	public Alg1Default(Connection conn , DataStore_student dataStoreStudent) {
		this.conn=conn;
		this.dataStoreStudent=dataStoreStudent;
		
		
		//the only difference between the queries is in the Answer4Rpt table used
		// temp table must be created by temptable creator beforehand
		
		if( useTemporaryTable ){
			
			//reversed flag meaning from orgiinal, now flag will be 0 if have enough answers reqquired
			//using temporary table
			//temp table means 1 less where clause
			query = " Select" +
					" a.surveyParticipantId as sid,   " +
					" SUM(( ? * (lad.value-1))/(mq.optionNum-1))/count(a.questionId)  as score, " +
					" if (COUNT( a.questionId ) >= m.minimumAnswersRequired, 0,1) as flag , " +
					" count(a.questionId) as countQuestion" +
					" From Answer4Rpt2016_temp As a" +
					" Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId" +
					" Inner Join MeasureQuestion As mq On mq.questionId = a.questionId" +
					" And mq.measureId = ?" +
					" Inner Join Measure As m On m.id = mq.measureId" +
					" Group By a.surveyParticipantId";
		
		}
		else{
			
			//1 more where clause needed for survey InstanceId
			
			query = " Select" +
					" a.surveyParticipantId as sid,   " +
					" SUM(( ? * (lad.value-1))/(mq.optionNum-1))/count(a.questionId)  as score, " +
					" if (COUNT( a.questionId ) >= m.minimumAnswersRequired, 0,1) as flag , " +
					" count(a.questionId) as countQuestion" +
					" From Answer4Rpt2016 As a" +
					" Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId" +
					" Inner Join MeasureQuestion As mq On mq.questionId = a.questionId" +
					" And mq.measureId = ? " +
					" Inner Join Measure As m On m.id = mq.measureId" +
					" Where a.surveyInstanceId= ?" +
					" Group By a.surveyParticipantId";
		}
	}
	

	/**
	 * Call this to run the Algorithm
	 */
	@Override
	public void computeResults(int measureId, int surveyInstance, int scaleFactor, String reserve1) {
		
		createDefaultStudentScores( Integer.toString(measureId));
		PreparedStatement ps = prepareQueryStatement( scaleFactor, measureId , surveyInstance , query );
		forEachRow( ps , new DefaultRowProcessor( Integer.toString(measureId)));
		
	}

	
	
	/**
	 * Set prepared statement variables
	 * @param scaleFactor
	 * @param measureId
	 * @param surveyInstanceId
	 * @param query
	 * @return
	 */
	private PreparedStatement prepareQueryStatement( int scaleFactor, int measureId, int surveyInstanceId , String query ){
		PreparedStatement stmt = null;
		try {
			int i=1;
			stmt = conn.prepareStatement(query);
			stmt.setInt(i++, scaleFactor);
			stmt.setInt(i++, measureId);
			
			//need extra where clause if not using temp table
			if( !useTemporaryTable)
				stmt.setInt(i++, surveyInstanceId);
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(debugPrint)
			System.out.println(stmt.toString());
		
		return stmt;
	}
	
	
	
	private void forEachRow( PreparedStatement ps , ProcessRow rowProcess){
		try {
			
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int i=1;
				
				int surveyParticipantId = rs.getInt( i++ );
				double score 			= rs.getDouble( i++ );
				int flag 				= rs.getInt( i++ );
				int countQuestion 		= rs.getInt( i++ );
				
				rowProcess.process(surveyParticipantId, score, flag, countQuestion);
			}
			
			
			//Utils.printResultSet(rs);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	

	/**
	 * Ensures each student at least has a score of zero and no questions answered for the measure
	 * ie prevent the occurrence of students with no PMS score for the measure
	 * @param measureId
	 */
	private void createDefaultStudentScores( String measureId ){
		List<String> students = dataStoreStudent.getAllStudents();
		for(String student: students){
			List<String> result = new ArrayList<String>();	
			result.add( "0" );
			result.add( "0" );
			result.add( "1" );
			dataStoreStudent.addStudentResult( student, measureId, result);
		}
	}
	
	
	//--------------------------------------------------
	
	/*
	 * Return type is stored inside the ProcessRow object
	 * 
	 * Implement this to modify row processing
	 * 
	 * Like Strategy pattern, but strategies are stored within the class
	 */
	interface ProcessRow{
		public void process( int surveyParticipantId,double score ,int flag,int countQuestion );
	}
	
	/*
	 * Modify ProcessRow objects to add new return types (need to get the data out of these
	 * objects after by yourself though)
	 * 
	 * See Double Dispatching
	 * (Function returns void, but accepts an object to put response into)`
	 */
	public class DefaultRowProcessor implements ProcessRow{
		
		//Change this to change return type
		Map<Integer, Double> surveyPaticipantScoreSubM;
		
		String measureId = "";		
		
		public DefaultRowProcessor(String measureId){
			surveyPaticipantScoreSubM = new TreeMap<Integer, Double>();
			
			this.measureId = measureId;
		}
		
		//use to get results out
		public Map<Integer, Double> getMap(){
			return surveyPaticipantScoreSubM;
		}
		
		public void process(int surveyParticipantId, double score, int flag, int countQuestion) {
			
			List<String> result = new ArrayList<String>();
			
			result.add( Double.toString(score) );
			result.add( Integer.toString(countQuestion) );
			result.add( Integer.toString(flag) );
			
			dataStoreStudent.addStudentResult(Integer.toString(surveyParticipantId), measureId, result);
			
		}
	}
	
	//-----------------------------------------------
	public static void pr( String s){
		System.out.println(s);
	}
}
