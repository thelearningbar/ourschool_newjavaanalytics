package algorithms.AScoreMean_school;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.Utils;


//AScoreMean = 1113,1114,1115,1107,1108,1109,1522,1523,1524



public class Alg_AScoreMean {
	
	
	DataStore_student dataStoreStudent;
	DataStore_school dataStoreSchool;
	//private Connection conn;
	
	
	private boolean debugPrint = false;
	


	public Alg_AScoreMean(Connection conn , 
			DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool) {
		
		
		//this.conn=conn;
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		
	}
	
	
	
	public void StoreResult( String measureId ,  List<String> result ,  HashMap<String,String> characteristicCombo ){
		HashMap<String, List<String>> res = new HashMap<String, List<String>>();
		res.put(measureId, result );
		dataStoreSchool.putCharacteristicResults(characteristicCombo, measureId, result);
	}
	
	
	//evan version
	public void compute( String measureId , String surveyInstanceId ){
			
		computeAlg( measureId , surveyInstanceId  );
				
		if(debugPrint) 
			System.out.println( this.getClass().getName()+ " done for " + measureId  );
	}
	
	
	//evan version
	/*
	 * Performs computation on a list of users
	 */
	public void computeAlg( String measureId , String surveyInstanceId  ){
		
			
		
		List<String> students = dataStoreStudent.getAllStudents();

		for( String student : students){


			//	821626377  {1217=[0, 0, 1], 1216=[0, 0, 1], 1215=[7.5, 2, 1], 1214=[0.0, 2, 1]}

			HashMap<String, List<String>> studentResults = dataStoreStudent.getStudentResults(student);

			//821626377  {1=3, 2=10, 42=5, 43=3}
			HashMap<String, String> studentCharacteristics  = dataStoreStudent.getStudentCharacteristics(student);

			HashMap<String, List<String>>  schoolResults = dataStoreSchool.getCharacteristicResults( studentCharacteristics ); 



			int populationActive =0;
			int populationPassed =0;
			int popSkipped=0 ;
			int popNotEnoughQuestionsAnswered=0 ;

			double sumScoreActive = 0;
			double sumScorePassed =0;	


			if( schoolResults != null){

				//[0, 0, 1, 0, 0.0, 0.0] 
				List<String> schoolMeasureResult  = schoolResults.get(measureId);

				//there is already data for this combo-measure in the datastore at school lvl, find it to add to it
				if( schoolMeasureResult != null){

					populationActive = Integer.parseInt( schoolMeasureResult.get( 0 ) );
					populationPassed = Integer.parseInt( schoolMeasureResult.get( 1 ) );
					popSkipped = 		Integer.parseInt( schoolMeasureResult.get( 2 ) );
					popNotEnoughQuestionsAnswered= Integer.parseInt( schoolMeasureResult.get( 3 ) );
					sumScoreActive=  Double.parseDouble( schoolMeasureResult.get( 4 ) );
					sumScorePassed=  Double.parseDouble( schoolMeasureResult.get( 5 ) );

				}				
			}

			
			
			//student has results for any measure---
			if( studentResults!=null && studentResults.containsKey(measureId)  ){				

				//list of results for measure which beloongs to a student
				// 1217=[9.0625, 8, 0]
				List<String> studentList = studentResults.get(measureId);

				//data for this student ----
				Double score = 		Double.parseDouble( studentList.get(0) );
				int questionsAnswered = Integer.parseInt( studentList.get(1) );
				String supressed = studentList.get(2); 
				//------------------

					
					if( supressed.equals("0")){
						// score was not supressed for this user

						populationActive++;
						populationPassed++;
						
						sumScoreActive+=Utils.round(score, 2);
						sumScorePassed+=Utils.round(score, 2);
					}
					else{						
						// user is supressed for not enough/any questions

						if( questionsAnswered >=1 ){
							// did not answer enough questions
							popNotEnoughQuestionsAnswered++;
						}
						else{
							// did not answer any questions
							popSkipped++;
						}
						
					}

					//------------------------------------------------
					//update the data for combination-measure

					List<String> result = new ArrayList<String>();

					result.add(Integer.toString(populationActive));
					result.add(Integer.toString(populationPassed));
					result.add(Integer.toString(popSkipped));
					result.add(Integer.toString(popNotEnoughQuestionsAnswered));

					result.add(Double.toString(sumScoreActive));
					result.add(Double.toString(sumScorePassed));

					//overwrite whatever was already in the datastore					
					dataStoreSchool.putCharacteristicResults(studentCharacteristics, measureId, result);

					//------------------------------------------

					
					
					if( debugPrint ){
						System.out.println(studentCharacteristics.toString() + "  " + measureId + "-" + result.toString());
					}
				}
				

				
			}
						
			
				
		}
		
		
		
	

	
	
	
	
	
	
/*
	
	//-------------------------------------------
	//-------------------------------
	//original  for reference only
	

	//tally score from datastructure
	String AScoreMean =  " Select "
			+ "SUM(score), "
			+ "count(surveyParticipantId) "
			+ "from ParticipantMeasureScores "
			+ "where measureId = ? "
			+ "and SurveyInstanceId = ? " ;
	
	
	public void algo_AScoreMean_measuresP(int measureId, int surveyInstanceId) {

		ResultSet resScoreComputation = null;
		PreparedStatement stmt = null;
		
		try {
			System.out.println( "School Level--> algo_AScoreMean_measuresP   MeasureId: " 
					+ measureId + " SurveyInstanceId: "+ surveyInstanceId);

			String scoreComputationQuery = AScoreMean;
			
			stmt = conn.prepareStatement(scoreComputationQuery);

			stmt.setInt(1, measureId);
			stmt.setInt(2, surveyInstanceId);

			resScoreComputation = stmt.executeQuery(); 


			double schoolScore= -1;
			int numberOfParticipant= 0;


			while (resScoreComputation.next()) {
				schoolScore = resScoreComputation.getDouble(1);
				numberOfParticipant = resScoreComputation.getInt(2);
			}


			if (schoolScore != -1    ){
				schoolScore = schoolScore/numberOfParticipant;
				
				System.out.println("schoolScore " +  schoolScore); 
								
			}
			else {
				System.out.println("No computation Score (Student): " + numberOfParticipant);
				System.out.println();
			}


		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
*/

}
