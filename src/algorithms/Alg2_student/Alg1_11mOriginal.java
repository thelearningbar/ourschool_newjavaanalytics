package algorithms.Alg2_student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MultiMap;
import org.apache.commons.collections4.map.MultiValueMap;


import utilities.Utils;


/*
 * Just for debug
 */
public class Alg1_11mOriginal {

	
	Connection conn = null;
	
	public Alg1_11mOriginal(Connection conn) {
		this.conn = conn;
	}

	public  String Algorithm111m =
			" Select   a.surveyParticipantId as sid,  "
					+ "mq.isReverse,  " +
					" (if( mq.isReverse>0 , ((?*(mq.optionNum-lad.value))/(mq.optionNum-1)), ((?*(lad.value-1))/(mq.optionNum-1)) )) as score " +
					" From Answer4Rpt As a " +
					" Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId " +
					" Inner Join MeasureQuestion As mq On mq.questionId = a.questionId " +
					" And mq.measureId = ? " +
					" Where a.surveyInstanceId= ? " +
					" Group By a.surveyParticipantId, a.questionId ";

	/*
	String compositeMeasures = 
			" Select group_concat(id), m2.minimumAnswersRequired  from Measure as m1  "
			+ " Inner join (select minimumAnswersRequired from Measure where id= ? ) as m2    "
			+ " where compositeMeasureId=? " ;
	 */

	@SuppressWarnings("unused")
	public void computeMeasureScoreAlgorithmId_1_11_m(
			int scaleFactor, 
			double normalizeConst, 
			
			int maximumMotivationScore, 
			int measureId, 
			int surveyInstanceId ) {

		PreparedStatement stmt = null;
		
		ResultSet resScoreComputation = null;


		try {
			
			//conn = database.getConnection("Computing score for a Measure");
			// SurveyInfo

			System.out.println("AlgorithmId 1 - 11_m " + "MeasureId: " + measureId + " " + "SurveyInstanceId: "	+ surveyInstanceId);

			String scoreComputationQuery = Algorithm111m;

			/*
			 * Input: 
			 * scaleFactor, measureID, and surveyInstanceID 
			 * 
			 * Output:
			 * surveyParticipantId and Score
			 */
			
			if( conn == null){
				System.out.println("no connection");
			}

			stmt = conn.prepareStatement(scoreComputationQuery);

			stmt.setInt(1, scaleFactor);
			stmt.setInt(2, scaleFactor);
			stmt.setInt(3, measureId);
			stmt.setInt(4, surveyInstanceId);

			resScoreComputation = stmt.executeQuery(); 


			Map<Integer, Double> surveyPaticipantFinalScore = new HashMap<Integer, Double>();  // To store Scores
			Map<Integer, Integer> surveyPaticipantQuestionsAnswered = new HashMap<Integer, Integer>();
			MultiMap<Integer, List<String>> surveyPaticipantScorebyQuestions = new MultiValueMap<Integer, List<String>>();
			
			while (resScoreComputation.next()) {

				List<String> answerMap = new ArrayList<String>();

				/*
				 * <QuestionId, Answer.value > concatenate qiD and corresponding
				 * mapped answer (for MultiMap)
				 */

				answerMap.add(new String(String.valueOf(resScoreComputation.getInt(2)))); // isReverse
				answerMap.add(new String(String.valueOf(resScoreComputation.getDouble(3)))); // Score
																														
				/* ParticipantID , <QuestionId, Answer.value (converted) > */
				surveyPaticipantScorebyQuestions.put(resScoreComputation.getInt(1), answerMap);
				
			}
			
			//System.out.println(surveyPaticipantScorebyQuestions.toString());

		
			/* Apply QMotive */
			for (Integer key : surveyPaticipantScorebyQuestions.keySet()) {

				
               	@SuppressWarnings("unchecked")
				Collection<List<String>> QuestionNScore = (Collection<List<String>>) surveyPaticipantScorebyQuestions.get(key);
               	
               	
               	if(key== 821244047){
	               	System.out.println("\n");
	               	System.out.println( QuestionNScore.toString() );
	            	System.out.println("\n");
               	}
				
               	/* Get Regular Mean */

				Double regularSum = 0.0;
				Double regularMean= 0.0;
				int numberQuestion = 0;

				for (List<String> i : QuestionNScore) {
					if(Utils.getInt(i.get(0)) == 0){
						regularSum = regularSum + Double.valueOf(i.get(1));
						numberQuestion++;
					}
				}	
					
				//Avoid Divide By zero
				if (numberQuestion> 0){
					regularMean = Math.abs(regularSum/numberQuestion);	
				}
				
				/* Normalize Reverse Values*/
				Double reverseScore, newReverseScore; 
				for (List<String> i : QuestionNScore) {
					
					if( Utils.getInt(i.get(0)) > 0){
						
						reverseScore =  Double.valueOf(i.get(1));
						
						if( Math.abs(reverseScore-regularMean) > normalizeConst ){
							newReverseScore = Math.abs(maximumMotivationScore - reverseScore);
							i.set(1, String.valueOf(newReverseScore));  //Update current value
						}
					}
				}
				
				
				/*Find Mean*/
				Double temp = 0.0;
				Double numberOfQuestions = 0.0;

				for (List<String> i : QuestionNScore) {
					temp = temp + Double.valueOf(Double.valueOf(i.get(1))); // cumulative Score
					numberOfQuestions++;  // how many questions answered
				}

				double averageScore = temp / numberOfQuestions;
	
				/* Update temporary store: Computed Score */

				//surveyPaticipantFinalScore.put(key, averageScore);
				//surveyPaticipantQuestionsAnswered.put(key,numberOfQuestions);
				

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{}
	}
}