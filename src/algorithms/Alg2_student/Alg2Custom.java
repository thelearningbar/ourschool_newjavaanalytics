package algorithms.Alg2_student;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MultiMap;
import org.apache.commons.collections4.map.MultiValueMap;

import utilities.Utils;



public class Alg2Custom implements IAlg2 {



	Connection conn = null;
	
	private String Algorithm111m =
			" Select  " +
					" a.surveyParticipantId as sid,  "
					+ "mq.isReverse,  " +
					" (if( mq.isReverse>0 , ((?*(mq.optionNum-lad.value))/(mq.optionNum-1)), ((?*(lad.value-1))/(mq.optionNum-1)) )) as score " +
					
					" From Answer4Rpt2016 As a " +
					" Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId " +
					" Inner Join MeasureQuestion As mq On mq.questionId = a.questionId " +
					" And mq.measureId = ? " +
					" Where a.surveyInstanceId= ? " +
					" Group By a.surveyParticipantId, a.questionId ";
	
	
	public Alg2Custom(Connection conn) {
		this.conn = conn;
	}

	public MultiMap<Integer, List<String>> getSurveyPaticipantScorebyQuestions( ResultSet resScoreComputation){
		MultiMap<Integer, List<String>> surveyPaticipantScorebyQuestions = new MultiValueMap<Integer, List<String>>();
		
		try {
			while (resScoreComputation.next()) {

				List<String> answerMap = new ArrayList<String>();

				/* <QuestionId, Answer.value > concatenate qiD and corresponding
				 * mapped answer (for MultiMap)
				 */
				answerMap.add( new String(String.valueOf(resScoreComputation.getInt(2)))); // isReverse
				answerMap.add( new String(String.valueOf(resScoreComputation.getDouble(3)))); // Score

				/* ParticipantID , <QuestionId, Answer.value (converted) > */
				surveyPaticipantScorebyQuestions.put(resScoreComputation.getInt(1), answerMap);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return surveyPaticipantScorebyQuestions;
	}
	
	
	/**
	 * 
	 * @param scaleFactor
	 * @param normalizeConst
	 * @param maximumMotivationScore
	 * @param measureId
	 * @param surveyInstanceId
	 * @param usedForCompositeMeasure
	 */
	public void computeMeasureScoreAlgorithmId_1_11_m(int scaleFactor, double normalizeConst,  int maximumMotivationScore, int measureId, int surveyInstanceId ){
		computeMeasureScoreAlgorithmId_1_11_m_2(scaleFactor,normalizeConst,maximumMotivationScore,measureId,surveyInstanceId,result);
	}

	public void computeMeasureScoreAlgorithmId_1_11_m_2(
			int scaleFactor, double normalizeConst,  int maximumMotivationScore, 
			int measureId, int surveyInstanceId ,StoreResult result) {

		PreparedStatement stmt = null;		
		ResultSet resScoreComputation = null;

		try {
			System.out.println("AlgorithmId 1 - 11_m " + "MeasureId: " + measureId + " " + "SurveyInstanceId: "	+ surveyInstanceId);

			/*
			Map<Integer, Double> surveyPaticipantFinalScore = new HashMap<Integer, Double>();  // To store Scores			
			Map<Integer, Integer> surveyPaticipantQuestionsAnswered = new HashMap<Integer, Integer>();
			*/
			
			MultiMap<Integer, List<String>> surveyPaticipantScorebyQuestions = getSurveyPaticipantScorebyQuestions(  doQuery1(scaleFactor,measureId,surveyInstanceId) );

			/* Apply QMotive */
			for (Integer key : surveyPaticipantScorebyQuestions.keySet()) {

				@SuppressWarnings("unchecked")
				Collection<List<String>> QuestionNScore = (Collection<List<String>>) surveyPaticipantScorebyQuestions.get(key);

				/* Get Regular Mean */
				Double regularSum = 0.0;
				Double regularMean= 0.0;
				int numberQuestion = 0;

				for (List<String> i : QuestionNScore) {
					if(Utils.getInt(i.get(0)) == 0){
						regularSum = regularSum + Double.valueOf(i.get(1));
						numberQuestion++;
					}
				}

				//Avoid Divide By zero
				if (numberQuestion> 0)
					regularMean = Math.abs(regularSum/numberQuestion);	

				/* Normalize Reverse Values*/
				Double reverseScore, newReverseScore; 
				for (List<String> i : QuestionNScore) {
					if(Utils.getInt(i.get(0)) > 0){
						reverseScore =  Double.valueOf(i.get(1));
						if(Math.abs(reverseScore-regularMean)> normalizeConst ){
							newReverseScore = Math.abs(maximumMotivationScore - reverseScore);
							i.set(1, String.valueOf(newReverseScore));  //Update current value
						}
					}
				}

				/*Find Mean*/
				Double temp = 0.0;
				int numberOfQuestions = 0;
				for (List<String> i : QuestionNScore) {
					temp = temp + Double.valueOf(Double.valueOf(i.get(1))); // cumulative Score
					numberOfQuestions++;  // how many questions answered
				}
				double averageScore = temp / numberOfQuestions;

				//Debug
				if(key== 823377433) 
					System.out.println( "Debug QMotive: " + " measure " + measureId +   "   "  + key  + " sum "  +  regularSum + " Ques " + numberOfQuestions + "  " + averageScore);	

				/* Update temporary store: Computed Score */
				/*
				surveyPaticipantFinalScore.put(key, averageScore);
				surveyPaticipantQuestionsAnswered.put(key,numberOfQuestions);
				 */
				result.Score(key, averageScore);
				result.Questions(key, numberOfQuestions);
				
				
				//System.out.println( key + " " +surveyPaticipantFinalScore.get(key));
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
	

	/**
	 * Input: 
	 * scaleFactor, measureID, and surveyInstanceID 
	 * 
	 * Output:
	 * surveyParticipantId and Score
	 */
	public ResultSet doQuery1(int scaleFactor, int measureId, int surveyInstanceId){
		
		try {
			PreparedStatement stmt = conn.prepareStatement(Algorithm111m);
			stmt.setInt(1, scaleFactor);
			stmt.setInt(2, scaleFactor);
			stmt.setInt(3, measureId);
			stmt.setInt(4, surveyInstanceId);
			return stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null; 
		
	}
	
	public StoreResult result = new StoreResult();
	
	public class StoreResult{
		
		Map<Integer, Double> surveyPaticipantFinalScore = new HashMap<Integer, Double>();  // To store Scores			
		Map<Integer, Integer> surveyPaticipantQuestionsAnswered = new HashMap<Integer, Integer>();
				
		public void Score( int key , Double value){
			surveyPaticipantFinalScore.put(key, value);
		}	
		
		public void Questions (int key,int value){
			surveyPaticipantQuestionsAnswered.put(key,value);
		}
	}



	@Override
	public void computeMeasureScoreAlgorithmId_1_11_m(int scaleFactor, double normalizeConst,
			int maximumMotivationScore, int measureId, int surveyInstanceId, String reserve1) {
		
		
	}

}
