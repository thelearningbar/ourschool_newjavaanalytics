package algorithms.Alg2_student;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.collections4.MultiMap;
import org.apache.commons.collections4.map.MultiValueMap;

import utilities.DataStore_student;
import utilities.MeasureInfo;
import utilities.Utils;

public class Alg2Default implements IAlg2{

	Connection conn = null;	
	DataStore_student dataStoreStudent;
	
	
	public String Algorithm111m = "";
			
	
	public boolean useTemporaryTable = true;
	
	
	public boolean debugPrint = false;
	
	/*
	String compositeMeasures = 
			" Select group_concat(id), m2.minimumAnswersRequired  from Measure as m1  "
			+ " Inner join (select minimumAnswersRequired from Measure where id= ? ) as m2    "
			+ " where compositeMeasureId=? " ;
	 */
	
	public Alg2Default(Connection conn ,DataStore_student dataStoreStudent ) {
		this.conn=conn;
		

		this.dataStoreStudent=dataStoreStudent;
		
		
		if( useTemporaryTable ){
			
			//TEMP TABLE MUST BE CREATED BEFOREHAND with temptable creator
			//1 less where clause
			
			Algorithm111m = " Select  " +
					" a.surveyParticipantId as sid,  "
					+ "mq.isReverse,  " +
					" (if( mq.isReverse>0 , ((?*(mq.optionNum-lad.value))/(mq.optionNum-1)), ((?*(lad.value-1))/(mq.optionNum-1)) )) as score " +
					" From evan_mamun_test.Answer4Rpt2016_temp As a " +
					" Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId " +
					" Inner Join MeasureQuestion As mq On mq.questionId = a.questionId " +
					" And mq.measureId = ? " +
					
					" Group By a.surveyParticipantId, a.questionId ";
			
		}
		else{
			
			//NO TEMP TABLE
			// 1 more where clause
			
			Algorithm111m = " Select  " +
					" a.surveyParticipantId as sid,  "
					+ "mq.isReverse,  " +
					" (if( mq.isReverse>0 , ((?*(mq.optionNum-lad.value))/(mq.optionNum-1)), ((?*(lad.value-1))/(mq.optionNum-1)) )) as score " +
					" From Answer4Rpt2016 As a " +
					" Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId " +
					" Inner Join MeasureQuestion As mq On mq.questionId = a.questionId " +
					" And mq.measureId = ? " +
					" and surveyInstanceId = ? " +
					" Group By a.surveyParticipantId, a.questionId ";
		}
	}

	
	public void computeMeasureScoreAlgorithmId_1_11_m(
			int scaleFactor, double normalizeConst,  int maximumMotivationScore, int measureId, int surveyInstanceId , String reserve1 ){
		
		computeMeasureScoreAlgorithmId_1_11_m_2(scaleFactor,normalizeConst,maximumMotivationScore,measureId,surveyInstanceId);
		
	}
	
	
	/**
	 * Ensures each student at least has a score of zero and no questions answered for the measure
	 * ie prevent the occurrence of students with no PMS score for the measure
	 * @param measureId
	 */
	public void createDefaultStudentScores( String measureId ){
		List<String> students = dataStoreStudent.getAllStudents();
		for(String student: students){
			List<String> result = new ArrayList<String>();	
			result.add( "0" );
			result.add( "0" );
			result.add( "1" );
			dataStoreStudent.addStudentResult( student, measureId, result);
		}
	}
	
	
	
	public void computeMeasureScoreAlgorithmId_1_11_m_2(
			int scaleFactor, double normalizeConst,  
			int maximumMotivationScore, int measureId, int surveyInstanceId ) {

		PreparedStatement stmt = null;		
		ResultSet resScoreComputation = null;
		
		MeasureInfo measureInfo = MeasureInfo.getInstance();
		measureInfo.saveMeasureInfo( Integer.toString( measureId) );
		
		
		String minimumAns = measureInfo.getMinimumAnswer( Integer.toString( measureId) );
		int    minimumAnswersRequired = Integer.parseInt(minimumAns) ;
		
		
		

		try {
			
			if( debugPrint)
				System.out.println(this.getClass().getName() + "  : " + measureId + " " + "SurveyInstanceId: "	+ surveyInstanceId);
			
			
			MultiMap<Integer, List<String>> surveyPaticipantScorebyQuestions = 
					getSurveyPaticipantScorebyQuestions(  doQuery1(scaleFactor,measureId,surveyInstanceId) );

			//first ensure each participant has an initial score, etc of  0 and suppressed,  then overrwrite it later
			createDefaultStudentScores(Integer.toString(measureId));
			
			
			
			/* Apply QMotive */
			for (Integer key : surveyPaticipantScorebyQuestions.keySet()) {

				@SuppressWarnings("unchecked")
				Collection<List<String>> QuestionNScore = (Collection<List<String>>) surveyPaticipantScorebyQuestions.get(key);

				/* Get Regular Mean */
				Double regularSum = 0.0;
				Double regularMean= 0.0;
				int numberQuestion = 0;

				for (List<String> i : QuestionNScore) {
					if(Utils.getInt(i.get(0)) == 0){
						regularSum = regularSum + Double.valueOf(i.get(1));
						numberQuestion++;
					}
				}

				//Avoid Divide By zero
				if (numberQuestion> 0){
					regularMean = Math.abs(regularSum/numberQuestion);	
				}
				
				/* Normalize Reverse Values*/
				Double reverseScore, newReverseScore; 
				for (List<String> i : QuestionNScore) {
					
					if(Utils.getInt(i.get(0)) > 0){
						reverseScore =  Double.valueOf(i.get(1));
						
						if(Math.abs(reverseScore-regularMean)> normalizeConst ){
							newReverseScore = Math.abs(maximumMotivationScore - reverseScore);
							i.set(1, String.valueOf(newReverseScore));  //Update current value
						}
					}
					
				}

				/*Find Mean*/
				Double temp = 0.0;
				int numberOfQuestions = 0;
				
				for (List<String> i : QuestionNScore) {
					temp = temp + Double.valueOf(Double.valueOf(i.get(1))); // cumulative Score
					numberOfQuestions++;  // how many questions answered
				}
				
				double averageScore = temp / numberOfQuestions;
				
				String surveyParticipantId = Integer.toString(key);
				
				

				if( numberOfQuestions >= minimumAnswersRequired){	
					
					List<String> result = new ArrayList<String>();
					result.add( Double.toString(averageScore) );
					result.add( Integer.toString(numberOfQuestions) );
					result.add( Integer.toString(0) );
					
					dataStoreStudent.addStudentResult( surveyParticipantId , Integer.toString(measureId), result);
					
				}
				else{
					//supress student
					
					List<String> result = new ArrayList<String>();	
					result.add( Double.toString(averageScore) );
					result.add( Integer.toString(numberOfQuestions) );
					result.add( Integer.toString(1) );
					
					dataStoreStudent.addStudentResult( surveyParticipantId, Integer.toString(measureId), result);
					
					if( debugPrint){
						System.out.println( "not enough questions " + surveyParticipantId + 
								" "+ Integer.toString(measureId) + " " + result.toString() ); 
					}
				}
				
				//System.out.println( key + " " +surveyPaticipantFinalScore.get(key));
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
	
	

	public MultiMap<Integer, List<String>> getSurveyPaticipantScorebyQuestions( ResultSet resScoreComputation){
		MultiMap<Integer, List<String>> surveyPaticipantScorebyQuestions = new MultiValueMap<Integer, List<String>>();
		
		try {
			while (resScoreComputation.next()) {

				List<String> answerMap = new ArrayList<String>();

				/* <QuestionId, Answer.value > concatenate qiD and corresponding
				 * mapped answer (for MultiMap)
				 */
				answerMap.add( new String(String.valueOf(resScoreComputation.getInt(2)))); // isReverse
				answerMap.add( new String(String.valueOf(resScoreComputation.getDouble(3)))); // Score

				/* ParticipantID , <QuestionId, Answer.value (converted) > */
				surveyPaticipantScorebyQuestions.put(resScoreComputation.getInt(1), answerMap);

			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return surveyPaticipantScorebyQuestions;
	}
	
	
	

	/**
	 * Input: 
	 * scaleFactor, measureID, and surveyInstanceID 
	 * 
	 * Output:
	 * surveyParticipantId and Score
	 */
	public ResultSet doQuery1(int scaleFactor, int measureId, int surveyInstanceId){
		
		try {
			PreparedStatement stmt = conn.prepareStatement(Algorithm111m);
			stmt.setInt(1, scaleFactor);
			stmt.setInt(2, scaleFactor);
			stmt.setInt(3, measureId);
			
			
			if( !useTemporaryTable){
				//if not using a temp table, need another where clause
				
				stmt.setInt(4, surveyInstanceId);
			}
						
			if(debugPrint){
				System.out.println("result fetch");
				System.out.println(stmt.toString());
			}
			ResultSet rs = stmt.executeQuery();
			
			if( debugPrint)
				System.out.println("got results");
			
			
			
			
			return rs;
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null; 
		
	}
	
	
	/*
	public class StoreResult{
		Map<Integer, Double> surveyPaticipantFinalScore = new HashMap<Integer, Double>();  // To store Scores			
		Map<Integer, Integer> surveyPaticipantQuestionsAnswered = new HashMap<Integer, Integer>();
				
		public void Score( int key , Double value){
			surveyPaticipantFinalScore.put(key, value);
		}	
		
		public void Questions (int key,int value){
			surveyPaticipantQuestionsAnswered.put(key,value);
		}
	}
	*/
}
