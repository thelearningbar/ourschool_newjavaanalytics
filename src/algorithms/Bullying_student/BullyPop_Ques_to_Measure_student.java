package algorithms.Bullying_student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import utilities.DataStore_student;
import utilities.MeasureInfo;
import utilities.Utils;

/**
 * Just takes the questions from some measure, extracts and stores under new measure Id
 * 
 * ex  
 * Questions 3551,3552 belong to measure 1412
 * 
 * q3551 and q3552 will be stored as  score-1, questions anwsered-1  under measures 1708,1709
 * 
 * so: measureId->questionId->[another Measure id to save results under]
 * 
 *                  
 * Measure1412 ------  q3551[yes] -- 1708 [ population who answered Yes ]
 *                  \_ q3552[yes] -- 1709 [ population who answered Yes ]
 * 
 * other options not selected are left as skipped
 * 
 * @author Evan K
 *
 */
public class BullyPop_Ques_to_Measure_student {
	

	private DataStore_student dataStoreStudent ;
	private Connection conn;
	
	//public boolean useTemporaryTable = true;
	
	private String query2 = ""
			+ "Select "
			+ "surveyParticipantId,"
			+ "a.questionId "
			+ "From Answer4Rpt2016_temp As a "
			+ "Join MeasureQuestion AS mq On mq.questionId = a.questionId  "
			+ "where mq.measureId = ? ;";
	
	
	
	//alternate query works for 1416? 1413? 1420?
	/*
	private String query3 = 
	"select surveyParticipantId, "
	+ "a.questionId, "
	+ "lad.value-1 as score "
	+ "from Answer4Rpt2016_temp as a "
	+ "join MeasureQuestion as mq on mq.questionId = a.questionId "
	+ "join LikertAnswerDetail as lad on lad.id= a.answerDetailId  "
	+ " where mq.measureId =?";
	*/

	public BullyPop_Ques_to_Measure_student(Connection conn , DataStore_student dataStoreStudent ) {
		this.dataStoreStudent=dataStoreStudent;
		this.conn = conn;
	}
	
	
	public void compute(String measureId , String surveyInstanceId ){
		System.out.println(this.getClass().getName());
		
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {		
			stmt = conn.prepareStatement(query2);
			stmt.setInt(1, Integer.parseInt(measureId));
			rs = stmt.executeQuery();
									
			MeasureInfo minfo = MeasureInfo.getInstance();
			
			List<Integer> questionList = minfo.measureQuestion.get(Integer.parseInt(measureId));
						
			for( int n:questionList ){				
				String x = minfo.getNewMeasureForQuestion( Integer.toString(n));
				createDefaultStudentScores( x );
			}
			
			while (rs.next()) {
				int i=1;
				int participant = rs.getInt(i++);
				int question 	= rs.getInt(i++);
				
				//q3
				//int score 	= rs.getInt(i++);
				
				List<String> resultScore = new ArrayList<String>(); //data for this student
				resultScore.add( "1" );	//score, ie thier response  y/n
				resultScore.add( "1" );	//questions answered
				
				//for query3
				//resultScore.add( Integer.toString(score) );			//score, ie thier response  y/n
				//resultScore.add( Integer.toString(1) );				//questions answered	
				
				
				resultScore.add(  "0" );							//not supressed
								
				dataStoreStudent.addStudentResult( 
						Integer.toString(participant), 
						minfo.getNewMeasureForQuestion(Integer.toString(question)) , 
						resultScore  );
								
			}
			
			Utils.printResultSet(rs);
			
		
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			closeThings(rs,stmt);			
		}
	}
	

	
	
	
	public void closeThings(ResultSet rs , PreparedStatement stmt){
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
				stmt = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	

	/**
	 * Ensures each student at least has a score of zero and no questions answered for the measure
	 * ie prevent the occurrence of students with no PMS score for the measure
	 * @param measureId
	 */
	private void createDefaultStudentScores( String measureId ){
		
		List<String> students = dataStoreStudent.getAllStudents();
		
		for(String student: students){
			List<String> result = new ArrayList<String>();	
			result.add( "0" );
			result.add( "0" );
			result.add( "1" );
			dataStoreStudent.addStudentResult( student, measureId, result);
		}
		
	}
	
	

}
