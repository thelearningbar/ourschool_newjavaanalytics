package algorithms.Bullying_student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import utilities.DataStore_student;
import utilities.MeasureArchitecture;
import utilities.MeasureInfo;
import utilities.SurveyInfoFinder;

/**
 * 
 * For a given measure,  gets participant's questionId and their response (answerdetail id)
 * 
 * 
 * @author Evan K
 *
 */
public class BullyPopulation_student {
	

	private DataStore_student dataStoreStudent ;
	public boolean useTemporaryTable = true;
	private Connection conn;
	
	public boolean debugPrint = true;
	
	
	//question ids from join
	//uses temp table
	private String Alg4b_Revised = 
	"Select "
	+ "a.surveyParticipantId as sid, "
	+ "(lad.value-1) as score "
	+ "From Answer4Rpt2016_temp As a "
	+ "Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId "
	+ "Inner Join MeasureQuestion As mq On mq.questionId = a.questionId "
	+ "and mq.measureId = ? ";
	
	public BullyPopulation_student(Connection conn , DataStore_student dataStoreStudent ) {
		this.dataStoreStudent=dataStoreStudent;
		this.conn = conn;
	}
	


	/**
	 * 
	 * 
	 * @param measureId
	 * @param surveyInstanceId
	 * @param primarySecondary 1-primary 2-secondary
	 */
	public void compute(String measureId , String surveyInstanceId){

		if( debugPrint ) System.out.println(this.getClass().getName() + " measureId:" + measureId);

		MeasureArchitecture measureTree = MeasureArchitecture.getInstance();

		//1695,1698,1701,1704

		List<String> subMeasures = measureTree.getChildMeasures( Integer.parseInt(measureId) );
		List<List<String>> subSubMeasures = new ArrayList<List<String>> (); 



		// Initiate Student Table & create breadown tree

		for( String sumMea : subMeasures ){

			List<String> breakdownMeasures = measureTree.getChildMeasures( Integer.parseInt(sumMea) );;
			for( String bdMea : breakdownMeasures ){
				createDefaultStudentScores(bdMea);
			}

			subSubMeasures.add(breakdownMeasures);
		}


		//find which measureId to actually query ie which of (12,1120,1116,1119,1087)
		//int measure = getMeasureToQuery(surveyInstanceId);
		//if( debugPrint )  System.out.println("  measure to query :" + Integer.toString(measureId));

		MeasureInfo minfo = MeasureInfo.getInstance();

		String query = Alg4b_Revised;

		PreparedStatement stmt = null;
		ResultSet rs = null;

		//ensure have some result for all students (they must each have at least a 'skipped' entry)

		for( String sumMea : subMeasures ){

			List<String> breakdownMeasures = measureTree.getChildMeasures( Integer.parseInt(sumMea) );;

			for( String bdMea : breakdownMeasures ){
				createDefaultStudentScores(bdMea);
			}
			
			try {
				stmt = conn.prepareStatement(query);
				stmt.setInt(1, Integer.parseInt(sumMea));
				rs = stmt.executeQuery();

				while (rs.next()) {

					int i=1;
					int participant = rs.getInt(i++);
					//int question 	= rs.getInt(i++);
					int score 		= rs.getInt(i++);


					//String supportMeasureId = questionMap.get(Integer.toString(question));

					//get the breakdown measures of the supportmeasure,  order is important
					//List<String> breakdownMeasures = minfo.getSubMeasures(supportMeasureId);


					/*
					 * Submeasures  321 , 543 ,765
					 * 
					 * For some participant:
					 * LAD value = 2
					 * 
					 * So:
					 * index        0  ,  1  ,  2
					 * submeasure  321 , 543 , 765
					 * score        0  ,  0  ,  1
					 * q's answerd  1  ,  1  ,  1

					 * 
					 */
					//System.out.println(breakdownMeasures );

					for( String bdMea : breakdownMeasures ){

						//if(index==score){
						if( breakdownMeasures.get(score) == bdMea){

							List<String> resultTrue = new ArrayList<String>(); //data for this student
							resultTrue.add(  "1" );							//score, ie thier response  y/n
							resultTrue.add(  "1" );							//questions answered
							resultTrue.add(  "0" );		
                            
							//System.out.println( " >> " + participant + "  "+ breakdownMeasures.get(score) + "  "  + score  + " "  + bdMea);
							dataStoreStudent.addStudentResult( Integer.toString(participant), bdMea  , resultTrue );
						}

						else{
							//System.out.println( " << " + participant + "  "+ breakdownMeasures.get(score) + "  "  + score  + " " + bdMea );
							
							List<String> resultTrue = new ArrayList<String>(); //data for this student
							resultTrue.add(  "0" );							//score, ie thier response  y/n
							resultTrue.add(  "1" );							//questions answered
							resultTrue.add(  "0" );		
							dataStoreStudent.addStudentResult( Integer.toString(participant), bdMea  , resultTrue );
						}

					}

				}



			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				if (rs != null) {
					try {
						rs.close();
						rs = null;
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (stmt != null) {
					try {
						stmt.close();
						stmt = null;
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}


		}

	}
	
	/**
	 * TODO should be some nicer way of doing this
	 * 
	 * @param surveyInstanceId
	 * @return
	 */
	private int getMeasureToQuery( String surveyInstanceId ){
		int measure=0;

		SurveyInfoFinder finder = new SurveyInfoFinder(conn);
		List<String> measures = finder.getSurveyMeasures(surveyInstanceId);
		
		
		System.out.println(measures.toString());
		
		if( measures.contains("12")){
			measure=12;
		}
		else if( measures.contains("1120") ){
			measure=1120;
		}
		else if( measures.contains("1116") ){
			measure=1116;
		}
		else if( measures.contains("1119") ){
			measure=1119;
		}
		else if( measures.contains("1087") ){
			measure=1087;
		}
		return measure;
	}
	


	/**
	 * Ensures each student at least has a score of zero and no questions answered for the measure
	 * ie prevent the occurrence of students with no PMS score for the measure
	 * @param measureId
	 */
	private void createDefaultStudentScores( String measureId ){
		List<String> students = dataStoreStudent.getAllStudents();
		for(String student: students){
			List<String> result = new ArrayList<String>();	
			result.add( "0" );
			result.add( "0" );
			result.add( "1" );
			dataStoreStudent.addStudentResult( student, measureId, result);
		}
	}
	
	/*
	 * 

		System.out.println(this.getClass().getName());
		
		//int surveyInstance = Integer.parseInt(surveyInstanceId);
		//int measure = Integer.parseInt(measureId);
		String query = Alg4b_Revised;
		
		
		int measure = 1087;

		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;
				
		try {
			
			stmt = conn.prepareStatement(query);

			int j=1;
			stmt.setInt(j++, measure);
			resScoreComputation = stmt.executeQuery();
			
			createDefaultStudentScores("1696");
			createDefaultStudentScores("1697");
			createDefaultStudentScores("1699");
			createDefaultStudentScores("1700");
			createDefaultStudentScores("1702");
			createDefaultStudentScores("1703");
			createDefaultStudentScores("1705");
			createDefaultStudentScores("1706");
			
			while (resScoreComputation.next()) {
				
				int i=1;
				int participant = resScoreComputation.getInt(i++);
				int question 	= resScoreComputation.getInt(i++);
				int score 		= resScoreComputation.getInt(i++);
				
				List<String> resultTrue = new ArrayList<String>(); //data for this student
				resultTrue.add(  "1" );							//score, ie thier response  y/n
				resultTrue.add(  "1" );							//questions answered
				resultTrue.add(  "0" );							//not supressed
				
				List<String> resultFalse = new ArrayList<String>(); //data for this student
				resultFalse.add(  "0" );							//score, ie thier response  y/n
				resultFalse.add(  "1" );							//questions answered
				resultFalse.add(  "0" );							//not supressed
				
				String measureIdYes="";
				String measureIdNo ="";
				
				if( question == 443 || question == 1290){
					//phys
					measureIdYes="1696";
					measureIdNo ="1697";
				}
				else if( question == 444 || question == 1291){
					//verb
					measureIdYes="1699";
					measureIdNo ="1700";
				}
				else if ( question == 445 || question == 1292){
					//soc
					measureIdYes="1702";
					measureIdNo ="1703";
				}
				else if ( question == 446 || question == 1293){
					//cyb
					measureIdYes="1705";
					measureIdNo ="1706";
				}
				
				
				if( score > 0){
					//Answered NO
					dataStoreStudent.addStudentResult( Integer.toString(participant), measureIdNo  , resultTrue );
					
					
					
					dataStoreStudent.addStudentResult( Integer.toString(participant), measureIdYes , resultFalse  );
					
				}
				else{
					//Answered YES
					dataStoreStudent.addStudentResult( Integer.toString(participant), measureIdYes , resultTrue );
					
					
					
					dataStoreStudent.addStudentResult( Integer.toString(participant), measureIdNo  , resultFalse  );
				}
				
			}
			
		
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
		}


	 * 
	 * 
	 */


}
