package algorithms.Bullying_student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureInfo;
import utilities.Utils;


/**
 * for 1413
 * 
 * See also, ctrl click
 * Visitable_BullyPop_QMeasure_Validate
 * VisitorPick_BullyPop_QMeasure_Validate
 * 
 * 
 * TODO default scores
 * 
 * @author Evan K
 *
 */
public class BullyPop_QMeasure_Validate {

	DataStore_school dataStoreSchool;
	DataStore_student dataStoreStudent ;
	//public boolean useTemporaryTable = true;
	Connection conn;
	
	private String query2 ="Select mq.questionId, "
			+ "a.surveyParticipantId,"
			+ "lad.value "
			+ "From Answer4Rpt2016_temp As a "
			+ "Join MeasureQuestion AS mq On mq.questionId = a.questionId "
			+ "Join LikertAnswerDetail As lad On lad.id = a.answerDetailId "
			+ "where mq.measureId = ?  	";			;
	
	
	public BullyPop_QMeasure_Validate(Connection conn , DataStore_student dataStoreStudent , DataStore_school dataStoreSchool) {
		this.dataStoreSchool=dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn = conn;
	}

	
	public class StudentTemp{
		
		public List<Integer> questions;
		public List<Float> scores;
		public HashMap<Integer,Float> questionScore;
		
		public StudentTemp(){
			questions = new ArrayList<Integer>();
			scores = 	new ArrayList<Float>();
			questionScore = new HashMap<Integer,Float>();			
		}
		
		public void addQuestionScore(int question, float score){
			questions.add(question);
			scores.add(score);
			
			questionScore.put(question, score);
		}
		
		public float getScore( int question ){
			for( int x =0; x<questions.size(); x++ ){
				if( x==question){
					return scores.get(x);
				}
			}
			return -1.0f;
		}
		
		public String toString(){
			return questions.toString() + " " + scores.toString();
		}
		
	}

	
	
	/**
	 * 
	 * 
	 * 
	 * @param measureId
	 * @param surveyInstanceId
	 * @param filterMeasure
	 * @param filterCutoff
	 */
	public void compute(String measureId , String surveyInstanceId , String filterMeasure , float filterCutoff ){

		System.out.println(this.getClass().getName());
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		
		MeasureInfo minfo = MeasureInfo.getInstance();
		List<Integer> questions = minfo.getQuestions(Integer.parseInt(measureId));
		
		for(int x:questions){
			createDefaultStudentScores( minfo.getNewMeasureForQuestion( Integer.toString( x )  ) );
		}
				
		try {
			stmt = conn.prepareStatement(query2);
			stmt.setInt(1, Integer.parseInt(measureId));
			rs = stmt.executeQuery();
			
			HashMap<String,StudentTemp> studentsTemp = new HashMap<String,StudentTemp>();
			
			while (rs.next()) {
				int i=1;
				int question 	= rs.getInt(i++);
				int participant = rs.getInt(i++);
				int score 	= rs.getInt(i++);
				
				String participantId = Integer.toString(participant);
				StudentTemp student ;
				if( studentsTemp.containsKey(participantId) ){
					student = studentsTemp.get(participantId);
					student.addQuestionScore(question, score);
				}
				else{
					student = new StudentTemp();
					student.addQuestionScore(question, score);
					studentsTemp.put(participantId, student);
				}
			}
			System.out.println(  );
						
			
			float parentCutoff = filterCutoff; 
			
			
			//iterate over all students with
			List<String> students = dataStoreStudent.getAllStudents();
			for(String participantId: students){
				
				if( dataStoreStudent.containsStudent(participantId)){
					//System.out.println(""+participantId+" exists");
					
					if( dataStoreStudent.studentHasResultsForMeasure(participantId, filterMeasure)){
						
						String scoreStringParent = dataStoreStudent.getStudentResultsMeasureValue(participantId , filterMeasure , 0);
						String suppressedParent =  dataStoreStudent.getStudentResultsMeasureValue(participantId , filterMeasure , 2);
						float scoreParent = Float.parseFloat(scoreStringParent);
						
						//HashMap<String,String> studentCharacteristics = dataStoreStudent.getStudentCharacteristics(participantId);
						//HashMap<String,List<String>> schoolLvlResultsForCharacteristic = 	dataStoreSchool.getCharacteristicResults(studentCharacteristics);
						
						if( suppressedParent.equals("0") ){
							if( scoreParent>=parentCutoff){
								
								for( int x: questions ){
									
									String newMeasure = minfo.getNewMeasureForQuestion(Integer.toString(x));
									
									float score = 0;
									int questionsAnswered=0;
									int suppressedFlag = 1;
									
									
									if( studentsTemp.containsKey(participantId) ){
										//answered some multichoice at least
										StudentTemp student  = studentsTemp.get(participantId);
										HashMap<Integer, Float> map = student.questionScore;
										
										if( map.containsKey(x)){
											//answered current question											
											score = map.get(x);
											questionsAnswered=1;
											suppressedFlag = 0;				
										}
										else{
											//did not answer current question/option
											score = 0;
											questionsAnswered=1;
											suppressedFlag = 0;
										}
									}
									else{
										//did not answer any multichoice, yet passed parent cutoff
										score = 0;
										questionsAnswered=0;
										suppressedFlag = 0;
									}
									
									saveStudentResult(participantId, newMeasure, score, questionsAnswered, suppressedFlag);
									
								}
								
								
							}
						}
						
						
						
					}
					else{
						System.out.println("student No results for measure 1690");
					}
					
				}
				
			}
			//end for
			
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
					rs = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
		}
	}
	
	

	
	/*
	System.out.println( participantId );
	if( studentsTemp.containsKey(participantId) ){
		StudentTemp student  = studentsTemp.get(participantId);
		
		HashMap<Integer, Float> map = student.questionScore;
		
		for( int x: questions ){
			
			String newMeasure = minfo.getNewMeasureForQuestion(Integer.toString(x));
			
			int popSkipped=0 ;
			int popNotEnoughQuestionsAnswered=0 ;
			int populationActive =0;
			int populationPassed =0;
			double sumScoreActive = 0;
			double sumScorePassed =0;
			List<String> schoolMeasureResult = schoolLvlResultsForCharacteristic.get(newMeasure);
			if( schoolMeasureResult != null){
				populationActive =	 			Integer.parseInt( schoolMeasureResult.get( 0 ) );
				populationPassed = 				Integer.parseInt( schoolMeasureResult.get( 1 ) );
				popSkipped = 					Integer.parseInt( schoolMeasureResult.get( 2 ) );
				popNotEnoughQuestionsAnswered = Integer.parseInt( schoolMeasureResult.get( 3 ) );
				sumScoreActive =  				Double.parseDouble( schoolMeasureResult.get( 4 ) );
				sumScorePassed =  				Double.parseDouble( schoolMeasureResult.get( 5 ) );
			}
			
			if( map.containsKey(x)){
				//String surveyParticipantId , String measure , float score , int questionsAnswered , int suppressedFlag
				saveStudentResult(participantId, newMeasure, map.get(x), 1, 0);
				populationPassed++;
			}
			populationActive++;
			
			saveSchoolResult( populationActive, populationPassed, popSkipped,  popNotEnoughQuestionsAnswered,
					sumScoreActive,  sumScorePassed, newMeasure ,  studentCharacteristics);
		}
		
		
	}
	else{
		System.out.println(participantId + " did not answer any multi choice");
		
		//TODO get questionIDs dynamically
		for( int x: questions ){
			
			String newMeasure = minfo.getNewMeasureForQuestion(Integer.toString(x));
			
			int popSkipped=0 ;
			int popNotEnoughQuestionsAnswered=0 ;
			int populationActive =0;
			int populationPassed =0;
			double sumScoreActive = 0;
			double sumScorePassed =0;
			List<String> schoolMeasureResult = schoolLvlResultsForCharacteristic.get(newMeasure);
			if( schoolMeasureResult != null){
				populationActive =	 			Integer.parseInt( schoolMeasureResult.get( 0 ) );
				populationPassed = 				Integer.parseInt( schoolMeasureResult.get( 1 ) );
				popSkipped = 					Integer.parseInt( schoolMeasureResult.get( 2 ) );
				popNotEnoughQuestionsAnswered = Integer.parseInt( schoolMeasureResult.get( 3 ) );
				sumScoreActive =  				Double.parseDouble( schoolMeasureResult.get( 4 ) );
				sumScorePassed =  				Double.parseDouble( schoolMeasureResult.get( 5 ) );
			}
			
			//String surveyParticipantId , String measure , float score , int questionsAnswered , int suppressedFlag
			saveStudentResult(participantId, newMeasure, 0, 0, 0);
			populationActive++;
			saveSchoolResult( populationActive, populationPassed, popSkipped,  popNotEnoughQuestionsAnswered,
					sumScoreActive,  sumScorePassed, newMeasure ,  studentCharacteristics);
		}
		
	}
	*/
	
	
	
	public void saveSchoolResult(int populationActive, int populationPassed, int popSkipped, int popNotEnoughQuestionsAnswered,
			double sumScoreActive, double sumScorePassed, String measureId , HashMap<String,String> characteristics){

		List<String> resultTotal = new ArrayList<String>();
		resultTotal.add(Integer.toString(populationActive));
		resultTotal.add(Integer.toString(populationPassed));
		resultTotal.add(Integer.toString(popSkipped));
		resultTotal.add(Integer.toString(popNotEnoughQuestionsAnswered));
		resultTotal.add(Double.toString(sumScoreActive));
		resultTotal.add(Double.toString(sumScorePassed));

		//overwrite whatever was already in the datastore					
		dataStoreSchool.putCharacteristicResults(characteristics, measureId , resultTotal);
		
	}
	
	/**
	 * 
	 * @param surveyParticipantId
	 * @param measure
	 * @param score
	 * @param questionsAnswered
	 * @param suppressedFlag
	 */
	public void saveStudentResult( String surveyParticipantId , String measure , float score , int questionsAnswered , int suppressedFlag ){
		List<String> result = new ArrayList<String>();
		result.add( Float.toString(score) );
		result.add( Integer.toString(questionsAnswered) );
		result.add( Integer.toString(suppressedFlag) );
		dataStoreStudent.addStudentResult( surveyParticipantId , measure, result);
	}

	public void saveStudentResult( int surveyParticipantId , String measure , Double score , int questionsAnswered , int suppressedFlag ){
		List<String> result = new ArrayList<String>();
		result.add( Double.toString(score) );
		result.add( Integer.toString(questionsAnswered) );
		result.add( Integer.toString(suppressedFlag) );
		dataStoreStudent.addStudentResult( Integer.toString(surveyParticipantId) , measure, result);
	}
	
	

	/**
	 * Ensures each student at least has a score of zero and no questions answered for the measure
	 * ie prevent the occurrence of students with no PMS score for the measure
	 * @param measureId
	 */
	private void createDefaultStudentScores( String measureId ){
		
		List<String> students = dataStoreStudent.getAllStudents();
		
		for(String student: students){
			List<String> result = new ArrayList<String>();	
			result.add( "0" );
			result.add( "0" );
			result.add( "1" );
			dataStoreStudent.addStudentResult( student, measureId, result);
		}
		
	}
	
	
}
