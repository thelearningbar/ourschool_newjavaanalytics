package algorithms.Bullying_student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.Utils;


/**
 * 
 * TODO
 * 
 * 
 * 
 * 1254 => algorithmId_30_measures
 * 
 * @author Evan K
 *
 */
public class Alg30 {
	

	//private DataStore_student dataStoreStudent;
	Connection conn ;
	
	public boolean useTemporaryTable = false;
	public boolean debugPrint = true;
	//private String query="";	
	
	
	
	public String Algorithm3 = "Select sid, score from( " +
			"Select " +
			"a.surveyParticipantId as sid,     " +
			"(lad.value-1) as score, "
			+ "if (COUNT( a.questionId ) >= m.minimumAnswersRequired, 1, 0) as flag " +
			"From Answer4Rpt As a " +
			"Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId " +
			"Inner Join MeasureQuestion As mq On mq.questionId = a.questionId " +
			"Inner Join Measure As m On m.id = mq.measureId " +
			"Where a.surveyInstanceId= ? " +
			"And mq.measureId = ? " +
			"Group By a.surveyParticipantId ) as mamun where flag =1 ";

	
	
	
	public Alg30(Connection conn ,  DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool) {
		
		//this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
		
		if( useTemporaryTable ){
			
		}
		else{

		}
	}
	
	
	public void compute(){
		
	}
	
	
	public void computeMeasureScoreAlgorithmId_30(int measureId, int surveyInstanceId) {
		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;

		try {
			System.out.println(	"Algo3:  MeasureId: " + measureId + " SurveyInstanceId: " + surveyInstanceId );

			String scoreComputationQuery = Algorithm3;

			/*
			 * Input: scaleFactor, measureID, and surveyInstanceID Output:
			 * surveyParticipantId and Score
			 */

			stmt = conn.prepareStatement(scoreComputationQuery);

			stmt.setInt(1, measureId);
			stmt.setInt(2, surveyInstanceId);
			resScoreComputation = stmt.executeQuery(); // participantId,
			
			
			Utils.printResultSet(resScoreComputation);
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			
		}
	}

}
