package algorithms.Bullying_student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Alg7_Original {
	
	Connection conn;
	
	
	public static String Algorithm4b = 
			" Select " +
			" a.surveyParticipantId as sid, "
			+ "a.questionId as qid, " +
			" (lad.value-1) as score " +
			" From Answer4Rpt2016 As a " +
			" Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId " +
			" Inner Join MeasureQuestion As mq On mq.questionId = a.questionId " +
			//" And mq.measureId = ? " +
			" Inner Join Measure As m On m.id = mq.measureId and a.questionId in (443,444,445,446) " +
			" Where a.surveyInstanceId= ? " +
			" Group By a.surveyParticipantId, a.questionId ";
	
	
	public static String Algorithm3 = "Select sid, score from( " +
			"Select " +
			"a.surveyParticipantId as sid,     " +
			"(lad.value-1) as score, "
			+ "if (COUNT( a.questionId ) >= m.minimumAnswersRequired, 1, 0) as flag " +
			"From Answer4Rpt2016 As a " +
			"Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId " +
			"Inner Join MeasureQuestion As mq On mq.questionId = a.questionId " +
			"And mq.measureId = ? " +
			"Inner Join Measure As m On m.id = mq.measureId " +
			"Where a.surveyInstanceId= ? " +
			"Group By a.surveyParticipantId ) as mamun where flag =1 ";
	
	
	public static String Algorithm21 = "Select sid, score from(" +
			" Select" +
			" a.surveyParticipantId as sid,    " +
			" SUM(If(lad.value-1=?, 1 , 0 )) as score, "
			+ "if (COUNT( a.questionId ) >= m.minimumAnswersRequired, 1, 0) as flag" +
			" From Answer4Rpt2016 As a" +
			" Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId" +
			" Inner Join MeasureQuestion As mq On mq.questionId = a.questionId" +
			" And mq.measureId = ?" +
			" Inner Join Measure As m On m.id = mq.measureId" +
			" Where a.surveyInstanceId= ?" +
			" Group By a.surveyParticipantId ) as mamun where flag =1";
	
	
	
	public Map<Integer,Integer> measureIdToQuery = new HashMap<Integer,Integer>();
	
	
	public Alg7_Original( Connection conn ) {
		this.conn = conn;
		
		measureIdToQuery.put(12,12);
		measureIdToQuery.put(1688,12);
		measureIdToQuery.put(1689,12);
		
	}
	
	
	
	public void computeMeasureScoreAlgorithmId_7_1(int measureId, int surveyInstanceId, Map<Integer, String> mapBullying) {
		
		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;

		try {

			System.out.println(
					"Algo 7_1: " +  "  MeasureId Bully: " + measureId + " " + "SurveyInstanceId: " + surveyInstanceId );

		
			// temporary store: Retrieved Score 
			
			Map <Integer, Double> participantFinalScore = new TreeMap<Integer, Double>();
			Map <Integer, Integer> participantScoreHigh = new TreeMap<Integer, Integer>();
			Map <Integer, Integer> participantScoreMedium = new TreeMap<Integer, Integer>();
			
			String scoreComputationQuery = null;
			
			// Algorithm 6 query 
			 //Input: measureId, SurveyInstanceId 
			 //Output: surveyParticipantId, QuestionId, Score 

		
				scoreComputationQuery = Algorithm4b;
				stmt = conn.prepareStatement(scoreComputationQuery);

				
				stmt.setInt(1, surveyInstanceId);
				

				resScoreComputation = stmt.executeQuery();

			while (resScoreComputation.next()) {
				
				String bullyType = mapBullying.get(resScoreComputation.getInt(2)); 
				
				int bullyScore = resScoreComputation.getInt(3);
				
				if((bullyType.equals("physical") && bullyScore >= 2) 
						|| (bullyType.equals("other")  &&  bullyScore >= 3 )  )
					participantScoreHigh.put(resScoreComputation.getInt(1), 1);
					
				if ((bullyType.equals("physical") && bullyScore >= 1) 
						|| (bullyType.equals("other")  &&  bullyScore >= 2 ) )
					participantScoreMedium.put(resScoreComputation.getInt(1), 1);
				
				participantFinalScore.put(resScoreComputation.getInt(1), 0.0) ;
				
			}
			
			
			System.out.println(participantFinalScore.size()  + "  " + participantFinalScore.toString());
			System.out.println(participantScoreHigh.size()   + "  " + participantScoreHigh.toString() );
			System.out.println(participantScoreMedium.size() + "  " + participantScoreMedium.toString());
			
			for(Integer key : participantFinalScore.keySet()  ){
				
				double score=0.0;
				
				if(participantScoreHigh.get(key)!= null)
					score= score + 2;
					
				if(participantScoreMedium.get(key)!= null)
					score= score + 1;
				
				if(score>0.0)
					participantFinalScore.replace(key, score )  ;
				
			}
			
			
			/*
			for(Integer key : participantFinalScore.keySet()  ){
				System.out.println(key + " "+ participantFinalScore.get(key).toString() );
			}
			*/
			
			//System.out.println(participantFinalScore.toString());
			//comparisonWithPMS( measureId,  surveyInstanceId, participantFinalScore);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				/*
				database.close("Close Connection for Input Query", conn);
				conn = null;
				*/
			}
		}

		
		
	}
	
	

}
