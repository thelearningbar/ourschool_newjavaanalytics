package algorithms.Bullying_student;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureInfo;


/**
 * 
 * 
 * Dont use
 *
 */
public class Alg20 {

	public Alg20() {
		
	}
	
	public static String Algorithm20 = "Select sid, score from(" +
			" Select" +
			" a.surveyParticipantId as sid,    " +
			" If( lad.likertResponseGroupId =7, "
			+ "SUM(If(lad.value-1 = 0 , 1 , 0 )), "
			+ "SUM(If(lad.value-1>= ? , 1 , 0 ) ))    "
			+ "as score, if (COUNT( a.questionId ) >= m.minimumAnswersRequired, 1, 0) as flag" +
			" From Answer4Rpt2016 As a" +
			" Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId" +
			" Inner Join MeasureQuestion As mq On mq.questionId = a.questionId" +
			" And mq.measureId = ?" +
			" Inner Join Measure As m On m.id = mq.measureId" +
			" Where a.surveyInstanceId= ? and a.questionId in (Select questionId from MeasureQuestion where measureId = ?)" +
			" Group By a.surveyParticipantId ) as mamun where flag =1";
	
/*
	public void computeMeasureScoreAlgorithmId_20(double questionCutOff, int mappedMeasure , int measureId, int surveyInstanceId) {
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet resScoreComputation = null;

		try {

			// SurveyInfo

			System.out.println("Algorithm20:  " + " Mapped measure- "+ mappedMeasure + " main Measure-- "  + measureId);

			// System.out.println("QuestionCutoff: " + questionCutOff+ " " +
			// "MeasureId: " + measureId + " " + "SurveyInstanceId: " +
			// surveyInstanceId);

			String scoreComputationQuery = Algorithm20;

			
			// Input: scaleFactor, measureID, and surveyInstanceID Output:
			// surveyParticipantId and Score
			
			 

			stmt = conn.prepareStatement(scoreComputationQuery);

			stmt.setDouble(1, questionCutOff);
			stmt.setInt(2, measureId);
			stmt.setInt(3, surveyInstanceId);
			stmt.setInt(4, mappedMeasure);

			resScoreComputation = stmt.executeQuery(); // participantId,

			Map<Integer, Double> surveyPaticipantScoreQuestions = new TreeMap<Integer, Double>(); 

			System.out.println(questionCutOff + "  cutoff>>m  "+ measureId );


			while (resScoreComputation.next()) {


				//surveyPaticipantScoreQuestions.put(resScoreComputation.getInt(1), resScoreComputation.getDouble(2)); // temporary



			}

			comparisonWithPMS( measureId, surveyInstanceId, surveyPaticipantScoreQuestions) ;



		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				database.close("Close Connection for Input Query", conn);
				conn = null;
			}
		}


	}
			*/
			
	

}
