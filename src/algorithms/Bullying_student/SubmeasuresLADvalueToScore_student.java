package algorithms.Bullying_student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureInfo;


/**
 * 
 * For measure 1415
 * 
 * For each student find the submeasures of the measure, for the 1 question of each submeasure (assumes only 1), 
 * save their lad value as score for that submeasure
 * 
 * @author Evan K
 *
 */
public class SubmeasuresLADvalueToScore_student {
	

	private DataStore_student dataStoreStudent;
	private Connection conn;
	
	public boolean useTemporaryTable = true;
	
	
	//TODO, remove the tlb_branding part from the inner select
	private String query=
			"Select surveyParticipantId, mq.measureId , lad.value from Answer4Rpt2016 as a "
			+ "join MeasureQuestion as mq on mq.questionId = a.questionId "
			+ "join LikertAnswerDetail as lad on lad.id= a.answerDetailId  "
			+ "where a.surveyInstanceId = ?  "
			+ "and mq.measureId in "
			+ "( Select id From tlb_branding.Measure where compositeMeasureId= ? ); ";
	

	public SubmeasuresLADvalueToScore_student(Connection conn ,  DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool ) {
		//this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent = dataStoreStudent;
		this.conn = conn;
	}
	


	public void compute( String measureId , String surveyInstanceId  ){
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {

			
			//ensure all students have the 'skipped' entry for all the submeasures
			MeasureInfo minfo = MeasureInfo.getInstance();
			List<String> submeasures = minfo.getSubMeasures(measureId);
			for( String m : submeasures ){
				createDefaultStudentScores(m);
			}


			int survey = Integer.parseInt(surveyInstanceId);
			int measure = Integer.parseInt(measureId);
			
			//do query to db
			stmt = conn.prepareStatement(query);
			stmt.setInt(1, survey);
			stmt.setInt(2, measure);
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				
				int i=1;
				String participant = rs.getString(i++);
				String submeasure  = rs.getString(i++);
				String score  = rs.getString(i++);
				
				
				//data for student object
				List<String> studentResult = new ArrayList<String>(); 
				studentResult.add( score );	//score, ie thier response/likertAnswerDetail.value
				studentResult.add(  "1" );	//questions answered
				studentResult.add(  "0" );	//not supressed
				
				//save data
				dataStoreStudent.addStudentResult( participant, submeasure , studentResult );
			}
			
			
			//mark measure and its submeasures as computed in student level
			dataStoreStudent.setStudentLevelMeasureComputed(measureId);
			for( String m : submeasures ){
				dataStoreStudent.setStudentLevelMeasureComputed(m);
			}
			

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
					rs = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
	
	

	/**
	 * Ensures each student at least has a score of zero and no questions answered for the measure 
	 * ie measure is supressed
	 * ie prevent the occurrence of students with no PMS score for the measure
	 * 
	 * must be called before assigning scores, it will overwrite
	 * 
	 * @param measureId
	 */
	private void createDefaultStudentScores( String measureId ){
		List<String> students = dataStoreStudent.getAllStudents();
		for(String student: students){
			List<String> resultSuppressed = new ArrayList<String>();	
			resultSuppressed.add( "0" );
			resultSuppressed.add( "0" );
			resultSuppressed.add( "1" );
			dataStoreStudent.addStudentResult( student, measureId, resultSuppressed);
		}
	}
	
	
}
