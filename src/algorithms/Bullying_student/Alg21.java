package algorithms.Bullying_student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureArchitecture;
import utilities.MeasureInfo;
import utilities.SurveyInfoFinder;



/**
 * 
 * Algorithm for computing at student level  
 * 
 * Bully-Category Basic
 * measures 1087,1120 
 * the ones that are just Yes/No
 * 
 * 
 * see 
 * alg7
 * 
 * 
 * saves results under 1690,1688,1689
 * 
 * 
 * @author Evan K
 *
 */
public class Alg21 {



	private DataStore_student dataStoreStudent;
	private Connection conn ;
	
	public boolean useTemporaryTable = false;
	public boolean debugPrint = true;
	private String query="";	
	
	
	//flag changed from original, 1=should suppress
	
	//query using temporary table created using temptable creator
	private String alg21_tempTable = 
			"Select "
			+ "a.surveyParticipantId as sid,  "
			+ "SUM(If(lad.value-1=?, 1 , 0 )) as score, "
			+ "if (COUNT( a.questionId ) >= m.minimumAnswersRequired, 0, 1) as flag "
			+ ",COUNT( a.questionId ) as countQuestion "
			+ "From Answer4Rpt2016_temp As a "
			+ "Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId "
			+ "Inner Join MeasureQuestion As mq On mq.questionId = a.questionId And mq.measureId = ?  "
			+ "Inner Join Measure As m On m.id = mq.measureId "
			+ "Group By a.surveyParticipantId "; 
	
	
	//using plain answer table
	private String alg21 = 
			"Select "
			+ "a.surveyParticipantId as sid,  "
			+ "SUM(If(lad.value-1=?, 1 , 0 )) as score, "
			+ "if (COUNT( a.questionId ) >= m.minimumAnswersRequired, 0, 1) as flag "
			+ ", COUNT( a.questionId ) as countQuestion "
			+ "From Answer4Rpt2016 As a "
			+ "Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId "
			+ "Inner Join MeasureQuestion As mq On mq.questionId = a.questionId "
			+ "And mq.measureId = ?  "
			+ "Inner Join Measure As m On m.id = mq.measureId "
			+ "Where a.surveyInstanceId=  ? "
			+ "Group By a.surveyParticipantId "; 
	
	


	public Alg21( Connection conn ,  DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool) {
		
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
		
		if( useTemporaryTable ){
			query = alg21_tempTable;
		}
		else{
			query = alg21;
		}
	}
	
	
	
	/**
	 * Call this to run algorithm
	 * 
	 * @param surveyInstanceId
	 * @param measureId
	 */
	public void compute( String surveyInstanceId , String measureId , double questionCutoff){
		
		createDefaultStudentScores( measureId );

		computeMeasureScoreAlgorithmId_21( questionCutoff,  Integer.parseInt(measureId) , Integer.parseInt(surveyInstanceId) );
		
	}
	


	private void computeMeasureScoreAlgorithmId_21(double questionCutOff, int measureId, int surveyInstanceId) {
		
		System.out.println("CLASS" + this.getClass().getName() );
		
		MeasureArchitecture measureTree = MeasureArchitecture.getInstance();

		List<String> submeasures = measureTree.getChildMeasures( measureId );
		
		//create skipped entries for submeasures
		for(String submeasure:submeasures){
			createDefaultStudentScores( submeasure );
		}
		
		PreparedStatement ps = prepareQueryStatement( questionCutOff ,  measureId  , surveyInstanceId , query);
		
		try {
			ResultSet rs = ps.executeQuery();
						
			while (rs.next()) {
				
				int i=1;
				
				int surveyParticipantId = rs.getInt( i++ );
				double score 			= rs.getDouble( i++ );
				int flag 				= rs.getInt( i++ );
				int countQuestion 		= rs.getInt( i++ );
				
				/**
				 * All 3 measures are given same result
				 */
				saveStudentResult(surveyParticipantId, submeasures.get(0), score, countQuestion, flag);
				saveStudentResult(surveyParticipantId, submeasures.get(1), score, countQuestion, flag);
				saveStudentResult(surveyParticipantId, submeasures.get(2), score, countQuestion, flag);
				
			}
			
			//Utils.printResultSet(rs);
			
			
			//marks the work as done
			dataStoreStudent.setStudentLevelMeasureComputed( Integer.toString(measureId) );
			dataStoreStudent.setStudentLevelMeasureComputed( submeasures.get(0) );
			dataStoreStudent.setStudentLevelMeasureComputed( submeasures.get(1)  );
			dataStoreStudent.setStudentLevelMeasureComputed( submeasures.get(2) );
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void saveStudentResult( int surveyParticipantId , String measure , Double score , int questionsAnswered , int suppressedFlag ){
		List<String> result = new ArrayList<String>();
		result.add( Double.toString(score) );
		result.add( Integer.toString(questionsAnswered) );
		result.add( Integer.toString(suppressedFlag) );
		dataStoreStudent.addStudentResult( Integer.toString(surveyParticipantId) , measure, result);
		
	}
	
	
	
	/**
	 * Set prepared statement variables
	 * @param questionCutOff 0 usually, not same as measure cutoff!
	 * @param measureToQueryFor measure id to search for
	 * @param surveyInstanceId id of survey instance
	 * @param query query string with ? to be filled in
	 * @return
	 */
	private PreparedStatement prepareQueryStatement( double questionCutOff, int measureToQueryFor, int surveyInstanceId , String query ){
		
		PreparedStatement stmt = null;
		
		try {
			int i=1;
			stmt = conn.prepareStatement(query);
			stmt.setDouble(i++, questionCutOff);
			stmt.setInt(i++, measureToQueryFor);
			
			//need extra where clause if not using temp table
			if( !useTemporaryTable)
				stmt.setInt(i++, surveyInstanceId);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		/*
		if(debugPrint)
			System.out.println(stmt.toString());
		*/
		
		return stmt;
	}
	
	
	/**
	 * Ensures each student at least has a score of zero and no questions answered for the measure
	 * ie prevent the occurrence of students with no PMS score for the measure
	 * @param measureId
	 */
	private void createDefaultStudentScores( String measureId ){
		List<String> students = dataStoreStudent.getAllStudents();
		for(String student: students){
			List<String> result = new ArrayList<String>();	
			result.add( "0" );
			result.add( "0" );
			result.add( "1" );
			dataStoreStudent.addStudentResult( student, measureId, result);
		}
	}

	
	
	/**
	 //* Original for reference only
	 * 
	 public static String Algorithm21 = 
			"Select sid, score from(" +
					" Select" +
					" a.surveyParticipantId as sid,    " +
					" SUM(If(lad.value-1=?, 1 , 0 )) as score, "
					+ "if (COUNT( a.questionId ) >= m.minimumAnswersRequired, 1, 0) as flag" +
					" From Answer4Rpt2016 As a" +
					" Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId" +
					" Inner Join MeasureQuestion As mq On mq.questionId = a.questionId" +
					" And mq.measureId = ?" +
					" Inner Join Measure As m On m.id = mq.measureId" +
					" Where a.surveyInstanceId= ?" +
					" Group By a.surveyParticipantId ) as mamun where flag =1";
	 
	 
	public void computeMeasureScoreAlgorithmId_21(double questionCutOff, int measureId, int surveyInstanceId) {

		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;

		try {
			System.out.println("Measure :  "+ measureId + "  SurveyInstanceId : " +  surveyInstanceId);
			String scoreComputationQuery = Algorithm21;
			stmt = conn.prepareStatement(scoreComputationQuery);

			stmt.setDouble(	1, questionCutOff	);
			stmt.setInt(	2, measureId		);
			stmt.setInt(	3, surveyInstanceId );
			resScoreComputation = stmt.executeQuery(); 

			Map<Integer, Double> surveyPaticipantScoreQuestions = new TreeMap<Integer, Double>(); 

			while (resScoreComputation.next()) {
				surveyPaticipantScoreQuestions.put(resScoreComputation.getInt(1), resScoreComputation.getDouble(2)); // temporary storage
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {}

			}
		}
	}
	*/
}
