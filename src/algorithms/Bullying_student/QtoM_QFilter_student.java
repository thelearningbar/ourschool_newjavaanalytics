package algorithms.Bullying_student;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureInfo;


/**
 * 
 * Question to Measure, with Question Filter
 * ie QtoM + QFilter
 * 
 * 
 * 
 * 
 *                           Passed Filter Question (You been bullied? =Yes)
 *            ________________________________|______________________________________________
 *           |                                                                               |  
 *             /- question0[yes/no] -- newMeasure0 [ score=1 or score = 0 , or suppressed ]
 * Measure----- - question1[yes/no] -- newMeasure1 [ score=1 or score = 0 , or suppressed ]
 *             \_ question2[yes/no] -- newMeasure2 [ score=1 or score = 0 , or suppressed ]
 * 
 * 
 * See also
 * V_QtoM_QFilter
 * 
 * measure 1420
 * 
 * @author Evan K
 *
 */
public class QtoM_QFilter_student {


	private DataStore_school dataStoreSchool;
	private DataStore_student dataStoreStudent ;
	private Connection conn;
	public boolean useTemporaryTable = true;

	String query = 
			"Select  "
				+ "mq.questionId, "
				+ "a.surveyParticipantId,"
				+ "if (lad.value=1 , 1 , 0 ) as score "
				+ "From Answer4Rpt2016_temp As a  "
				+ "Join MeasureQuestion AS mq On mq.questionId = a.questionId  "
				+ "join LikertAnswerDetail as lad on lad.id=a.answerDetailId  "
				+ "And mq.measureId = ? "
				+ "order by a.surveyParticipantId;";


	public QtoM_QFilter_student( Connection conn , DataStore_student dataStoreStudent , DataStore_school dataStoreSchool) {
		this.dataStoreSchool=dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn = conn;
	}



	/**
	 * Temporary storage for data for a student
	 * 
	 * @author Evan K
	 *
	 */
	public class StudentTemp{
		public HashMap<Integer,Integer> questionScore;
		public boolean passedFilter = false;

		public StudentTemp(){
			questionScore = new HashMap<Integer,Integer>();			
		}

		public void addQuestionScore(int question, int score){
			questionScore.put(question, score);
		}

		public String toString(){
			return questionScore.toString() ;
		}

		public void setDidPass(){
			passedFilter=true;
		}

	}


	public void compute(String measureId , String surveyInstanceId , int filterQuestionId){

		System.out.println(this.getClass().getName());

		PreparedStatement stmt = null;
		ResultSet rs = null;

		MeasureInfo minfo = MeasureInfo.getInstance();
		
		//list of questions belonging to measure ex 1412,1411,1420
		List<Integer> questionsParentMeasure = minfo.getQuestions(Integer.parseInt(measureId));
		
		
		createDefaultStudentScores( measureId );

		//create default student scores
		for( int currentQuestion:questionsParentMeasure){
			
			if( currentQuestion != filterQuestionId ){
				String newMeasure = minfo.getNewMeasureForQuestion(Integer.toString(currentQuestion));
				createDefaultStudentScores( newMeasure );
			}
		}


		try {
			stmt = conn.prepareStatement(query);
			stmt.setInt(1, Integer.parseInt(measureId));
			rs = stmt.executeQuery();


			HashMap<String,StudentTemp> studentsTemp = new HashMap<String,StudentTemp>();

			int passed = 0;
			
			//build a complete list student objects from all rows of query, therefore will have any existing results for the filter question
			while (rs.next()) {
				int i=1;
				int question 	= rs.getInt(i++);
				int participant = rs.getInt(i++);
				int score 	= rs.getInt(i++);

				String participantId = Integer.toString(participant);
				StudentTemp student ;

				if( studentsTemp.containsKey(participantId) ){
					student = studentsTemp.get(participantId);
					student.addQuestionScore(question, score);
				}
				else{
					student = new StudentTemp();
					student.addQuestionScore(question, score);
					studentsTemp.put(participantId, student);
				}

				//student has passed filter
				if( question == filterQuestionId ){
					student.setDidPass();
					
					passed++;
				}
			}
			
			System.out.println(passed + " pass");
			

			//iterate over all the students found (that passed filter), update their scores overwriting default scores for measure
			for(String studentId : studentsTemp.keySet() ){

				StudentTemp student  = studentsTemp.get(studentId);

				
				if( student.passedFilter=true ){
					//answered filter question
					
					//saveStudentResult(studentId, measureForQuestionOption, score , questionsAnswered, suppressed);

					for( int currentQuestion:questionsParentMeasure){
						
						//this question cooresponds to a measureId, under which we save population data
						String measureForQuestionOption = minfo.getNewMeasureForQuestion(Integer.toString(currentQuestion));
						
						if( currentQuestion == filterQuestionId){
							measureForQuestionOption = measureId;
							//System.out.println(filterQuestionId + " filterQuestionId "  +  measureForQuestionOption + "__");
						}
						
						if( measureForQuestionOption==null){
							System.out.println("question has no new measure "+ currentQuestion );
						}
			
							
						

						// question and score of current student
						HashMap<Integer, Integer> map = student.questionScore;

						float score =0;
						int suppressed = 1;
						int questionsAnswered=1;
						
						//answered yes to filter question
						if( map.get(filterQuestionId) == 1 ){
							
							//student has answered question
							if( map.containsKey(currentQuestion)){
								
								//score saved (whatever it is 0/1)  and not suppressed
								score = map.get(currentQuestion);
								questionsAnswered	=1;
								suppressed			=0;
							}
							else{
								//said yes to filter and no response to the multiChoice option
								//score suppressed
								score 			 =0;
								questionsAnswered=0;
								suppressed		 =1;
							}	
							
						}
						else{//answered no to filter question
														
							// suppress regardless of score
							
							//student has answered question
							if( map.containsKey(currentQuestion)){
								score = map.get(currentQuestion);
								questionsAnswered	=1;
								suppressed			=1;
							}
							else{
								//said no to filter and not have a response to the multiChoice option
								score				=0;
								questionsAnswered	=0;
								suppressed			=1;
							}					
							
						}
						
						//System.out.println(studentId+ " "+ measureForQuestionOption + " "+ score + " "+ questionsAnswered + " "+ suppressed);
						saveStudentResult(studentId, measureForQuestionOption, score , questionsAnswered, suppressed);

					}
				}else{
					//participant has no answer for filter question
					
				}

			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			closeThings( rs , stmt );
		}
	}



	
	
	public void closeThings(ResultSet rs , PreparedStatement stmt){
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
				stmt = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}



	public void saveSchoolResult(int populationActive, int populationPassed, int popSkipped, int popNotEnoughQuestionsAnswered,
			double sumScoreActive, double sumScorePassed, String measureId , HashMap<String,String> characteristics){

		List<String> resultTotal = new ArrayList<String>();
		resultTotal.add(Integer.toString(populationActive));
		resultTotal.add(Integer.toString(populationPassed));
		resultTotal.add(Integer.toString(popSkipped));
		resultTotal.add(Integer.toString(popNotEnoughQuestionsAnswered));
		resultTotal.add(Double.toString(sumScoreActive));
		resultTotal.add(Double.toString(sumScorePassed));

		//overwrite whatever was already in the datastore					
		dataStoreSchool.putCharacteristicResults(characteristics, measureId , resultTotal);

	}

	/**
	 * 
	 * @param surveyParticipantId string
	 * @param measure string
	 * @param score float
	 * @param questionsAnswered int
	 * @param suppressedFlag int
	 */
	public void saveStudentResult( String surveyParticipantId , String measure , float score , int questionsAnswered , int suppressedFlag ){
		List<String> result = new ArrayList<String>();
		result.add( Float.toString(score) );
		result.add( Integer.toString(questionsAnswered) );
		result.add( Integer.toString(suppressedFlag) );
		dataStoreStudent.addStudentResult( surveyParticipantId , measure, result);
	}

	public void saveStudentResult( int surveyParticipantId , String measure , Double score , int questionsAnswered , int suppressedFlag ){
		List<String> result = new ArrayList<String>();
		result.add( Double.toString(score) );
		result.add( Integer.toString(questionsAnswered) );
		result.add( Integer.toString(suppressedFlag) );
		dataStoreStudent.addStudentResult( Integer.toString(surveyParticipantId) , measure, result);
	}



	/**
	 * Ensures each student at least has a score of zero and no questions answered for the measure
	 * ie prevent the occurrence of students with no PMS score for the measure
	 * @param measureId
	 */
	private void createDefaultStudentScores( String measureId ){

		List<String> students = dataStoreStudent.getAllStudents();

		for(String student: students){
			List<String> result = new ArrayList<String>();	
			result.add( "0" );
			result.add( "0" );
			result.add( "1" );
			dataStoreStudent.addStudentResult( student, measureId, result);
		}

	}



}
