package algorithms.Bullying_student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import utilities.DataStore_student;
import utilities.MeasureArchitecture;
import utilities.MeasureInfo;
import utilities.SurveyInfoFinder;


/**
 * 
 * For measure 1692(new measure) 
 * 
 * 
 * saves results under 1690,1688,1689
 * 
 * must query for measures 12,1116,1119 , 
 * 
 * Bully-Category Extended
 * ie The Never/Once/twice/everyDay ones
 * 
 * 
 * see alg21
 * 
 * takes the questions which the student answered and turns it into a score
 * score is saved to 3 measures, different cutoffs are later applied to each of the 3 measures for students
 * 
 * 
 * @author Evan K
 *
 */
public class Alg7_Default {
		
	
	private DataStore_student dataStoreStudent ;
	private Connection conn;
	public Map<Integer,Integer> measureIdToQuery = new HashMap<Integer,Integer>();
	public boolean useTemporaryTable = true;
	
	private String query ="";
	
	
	public boolean debugPrint  = true;
	
	
	//TODO cleanup
	//original, specific question ids
	public String Algorithm4b = 
			" Select " 
			+" a.surveyParticipantId as sid, "
			+ "a.questionId as qid, " 
			+" (lad.value-1) as score " 
			+" From Answer4Rpt2016 As a " 
			+" Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId " 
			+" Inner Join MeasureQuestion As mq On mq.questionId = a.questionId " 
			+" Inner Join Measure As m On m.id = mq.measureId "
			+ "and a.questionId in (443,444,445,446) " 
			+" Where a.surveyInstanceId= ? " 
			+" Group By a.surveyParticipantId, a.questionId ";
	
	//question ids from join
	//uses temp table
	private String Alg4b_Revised = 
			"Select "
			+ "a.surveyParticipantId as sid, "
			+ "a.questionId as qid, "
			+ "(lad.value-1) as score "
			+ "From Answer4Rpt2016_temp As a "
			+ "Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId "
			+ "Inner Join MeasureQuestion As mq On mq.questionId = a.questionId "
			+ "Where a.surveyInstanceId= ? "
			+ "and mq.measureId = ? "
			+ "Group By a.surveyParticipantId, a.questionId ;";
	
	public Alg7_Default( Connection conn , DataStore_student dataStoreStudent ) {
		
		this.dataStoreStudent=dataStoreStudent;
		this.conn = conn;
		
		if( useTemporaryTable ){
			query = Alg4b_Revised;
		}
		else{
			query = Algorithm4b;
		}
	}
		

	public void compute(String measureId , String surveyInstanceId ){
		
		//TODO get this map from DB?
		Map<Integer, String> mapBullying = new HashMap<Integer, String> ();
		mapBullying.put(443,"physical");
		mapBullying.put(1290,"physical");
		//mapBullying.put(444,"other"); //verbal
		//mapBullying.put(445,"other"); //social
		//mapBullying.put(446,"other"); //other
		
		//MeasureInfo minfo = MeasureInfo.getInstance();
		MeasureArchitecture measureTree = MeasureArchitecture.getInstance();
		
		//ie  1690,1989,1688
		List<String> submeasures = measureTree.getChildMeasures( Integer.parseInt(measureId) );
		
		System.out.println(">>> " + measureId +" "+  submeasures.toString() + "  queryMeasure:" + measureId );
		
		
		//order of submeasures is important because of order of cutoffs!
		
		computeMeasureScoreAlgorithmId_7_1( 
				Integer.parseInt(measureId)  , Integer.parseInt(surveyInstanceId) , 
				submeasures.get(0),submeasures.get(1), submeasures.get(2));
		
	}

	/**
	 * 
	 * @param measureToQueryFor
	 * @param surveyInstanceId
	 * @param mapBullying
	 * @param submeasureLow
	 * @param submeasureHigh
	 * @param submeasureMean
	 */
	public void computeMeasureScoreAlgorithmId_7_1(int measureId, int surveyInstanceId, 
			String submeasureLow ,String submeasureHigh, String submeasureMean) {
		
		
		
		createDefaultStudentScores( submeasureMean , submeasureLow,submeasureHigh );
		
		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;
		
		System.out.println(submeasureMean+" " +submeasureHigh+" "+submeasureLow );
		System.out.println(	"Algo 7_1 Default: " 
						+ "  MeasureId Bully: " + measureId 
						+ "  SurveyInstanceId: " + surveyInstanceId );
		
		
		try {
			Map <Integer,  Double> participantFinalScore  = new TreeMap<Integer, Double>();
			Map <Integer, Integer> participantScoreHigh   = new TreeMap<Integer, Integer>();
			Map <Integer, Integer> participantScoreMedium = new TreeMap<Integer, Integer>();
			
			// Algorithm 6 query 
			// Input: measureId, SurveyInstanceId 
			// Output: surveyParticipantId, QuestionId, Score 
			
			stmt = conn.prepareStatement(query);
			stmt.setInt(1, surveyInstanceId);
			if( useTemporaryTable ){
				stmt.setInt(2, measureId);
			}
			
			resScoreComputation = stmt.executeQuery();
			
			while (resScoreComputation.next()) {
				
				//String bullyType = mapBullying.get(resScoreComputation.getInt(2)); 
				
				boolean bullyHigh = false, bullyLow = false ;
				int questionId = resScoreComputation.getInt(2);
				
				if (questionId == 443 || questionId == 1290)  
					 bullyHigh = true;
				else 
					bullyLow = true;
				
				//if(debugPrint) System.out.println( bullyType + "   index: " +  resScoreComputation.getInt(2));
				
				int bullyScore = resScoreComputation.getInt(3);
				
				if( ( bullyHigh  && bullyScore >= 2) || (bullyLow  &&  bullyScore >= 3) )  
					participantScoreHigh.put(resScoreComputation.getInt(1), 1);
					
				if( (bullyHigh && bullyScore >= 1) || (bullyLow &&  bullyScore >= 2 ) )
					participantScoreMedium.put(resScoreComputation.getInt(1), 1);
				
				participantFinalScore.put(resScoreComputation.getInt(1), 0.0) ;
				
			}
			
			
			//for each participant calc score
			for(Integer key : participantFinalScore.keySet()  ){
				
				double score=0.0;
				
				if(participantScoreHigh.get(key)!= null){
					score= score + 2;
				}
				
				if(participantScoreMedium.get(key)!= null){
					score= score + 1;
				}
				
				if(score>0.0){
					participantFinalScore.replace(key, score )  ;
				}
				
				
				saveStudentResult( key , submeasureMean , score, 1, 0);
				saveStudentResult( key , submeasureLow  , score, 1, 0);
				saveStudentResult( key , submeasureHigh , score, 1, 0);
								
			}
			
			//marks the work as done
			dataStoreStudent.setStudentLevelMeasureComputed( Integer.toString(measureId) );
			dataStoreStudent.setStudentLevelMeasureComputed( submeasureMean );
			dataStoreStudent.setStudentLevelMeasureComputed( submeasureLow  );
			dataStoreStudent.setStudentLevelMeasureComputed( submeasureHigh );
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
		}

		
		
	}
	
	

	public void saveStudentResult( int surveyParticipantId , String measure , Double score , int questionsAnswered , int suppressedFlag ){
		List<String> result = new ArrayList<String>();
		result.add( Double.toString(score) );
		result.add( Integer.toString(questionsAnswered) );
		result.add( Integer.toString(suppressedFlag) );
		dataStoreStudent.addStudentResult( Integer.toString(surveyParticipantId) , measure, result);
		
	}
	
	
	

	/**
	 * Ensures each student at least has a score of zero and no questions answered for the measure
	 * ie prevent the occurrence of students with no PMS score for the measure
	 * @param measureId
	 */
	private void createDefaultStudentScores( String measureId, String sub1 , String sub2  ){
		List<String> students = dataStoreStudent.getAllStudents();
		
		for(String student: students){
			List<String> result = new ArrayList<String>();	
			result.add( "0" );
			result.add( "0" );
			result.add( "1" );
			dataStoreStudent.addStudentResult( student, measureId, result);
		}
		
		for(String student: students){
			List<String> result = new ArrayList<String>();	
			result.add( "0" );
			result.add( "0" );
			result.add( "1" );
			dataStoreStudent.addStudentResult( student,sub1, result);
		}
		
		for(String student: students){
			List<String> result = new ArrayList<String>();	
			result.add( "0" );
			result.add( "0" );
			result.add( "1" );
			dataStoreStudent.addStudentResult( student, sub2, result);
		}
	}
	
	

}




/*
public static String Algorithm4b = 
	" Select " +
	" a.surveyParticipantId as sid, "
	+ "a.questionId as qid, " +
	" (lad.value-1) as score " +
	" From Answer4Rpt2016 As a " +
	" Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId " +
	" Inner Join MeasureQuestion As mq On mq.questionId = a.questionId " +
	" Inner Join Measure As m On m.id = mq.measureId and a.questionId in (443,444,445,446) " +
	" Where a.surveyInstanceId= ? " +
	" Group By a.surveyParticipantId, a.questionId ";
*/

/*
public static String Algorithm3 = "Select sid, score from( " +
		"Select " +
		"a.surveyParticipantId as sid,     " +
		"(lad.value-1) as score, "
		+ "if (COUNT( a.questionId ) >= m.minimumAnswersRequired, 1, 0) as flag " +
		"From Answer4Rpt2016 As a " +
		"Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId " +
		"Inner Join MeasureQuestion As mq On mq.questionId = a.questionId " +
		"And mq.measureId = ? " +
		"Inner Join Measure As m On m.id = mq.measureId " +
		"Where a.surveyInstanceId= ? " +
		"Group By a.surveyParticipantId ) as mamun where flag =1 ";
*/

/*
public static String Algorithm21 = "Select sid, score from(" +
		" Select" +
		" a.surveyParticipantId as sid,    " +
		" SUM(If(lad.value-1=?, 1 , 0 )) as score, "
		+ "if (COUNT( a.questionId ) >= m.minimumAnswersRequired, 1, 0) as flag" +
		" From Answer4Rpt2016 As a" +
		" Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId" +
		" Inner Join MeasureQuestion As mq On mq.questionId = a.questionId" +
		" And mq.measureId = ?" +
		" Inner Join Measure As m On m.id = mq.measureId" +
		" Where a.surveyInstanceId= ?" +
		" Group By a.surveyParticipantId ) as mamun where flag =1";
*/
