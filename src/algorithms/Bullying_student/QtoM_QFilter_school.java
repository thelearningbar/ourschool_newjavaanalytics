package algorithms.Bullying_student;


import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureRegister;



/**
 * 
 * 1420
 *  check if works, use for 1413 too?
  * 
 * Participant must have answered 'Yes' to UI filter-question before
 * their Yes/No answer to each multiple choice option (which is a question) is counted
 * toward another cooresponding measure (which has been created for each 'question')
 * 
 * 
 *             Passed Filter Measure/Question (parent measure ie 1420) (You been bullied? =Yes)
 *              ________________________________|_______________________________________
 *             |                                                                        |  
 *                  /- question0[yes/no] -- newMeasure0 [ population who answered Yes ]
 * Measure1420 ----- - question1[yes/no] -- newMeasure1 [ population who answered Yes ]
 *                  \_ question2[yes/no] -- newMeasure2 [ population who answered Yes ]
 * 
 * 
 * 
 * 
 * @author Evan K
 *
 */
public class QtoM_QFilter_school {


	private DataStore_school dataStoreSchool;
	private DataStore_student dataStoreStudent ;
	public boolean useTemporaryTable = true;


	public QtoM_QFilter_school( Connection conn , DataStore_student dataStoreStudent , DataStore_school dataStoreSchool) {
		this.dataStoreSchool=dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
	}

	public void compute(String measureId , String surveyInstanceId ){
		System.out.println(this.getClass().getName());


		List<String> students = dataStoreStudent.getAllStudents();
		
		List<String> submeasures = MeasureRegister.getInstance().getChildMeasuresSchoolLvl(measureId);

		for( String student : students){

			//	821626377  {1217=[0, 0, 1], 1216=[0, 0, 1], 1215=[7.5, 2, 1], 1214=[0.0, 2, 1]}
			HashMap<String, List<String>> studentResults = dataStoreStudent.getStudentResults(student);

			//821626377  {1=3, 2=10, 42=5, 43=3}
			HashMap<String, String> studentCharacteristics  = dataStoreStudent.getStudentCharacteristics(student);

			//results for this combination of characteristics
			//   measureId , [active pop,passed pop ,noQ pop ,notEnough pop, active scor,passed score]
			HashMap<String, List<String>>  schoolLvlResultsForCharacteristic = dataStoreSchool.getCharacteristicResults( studentCharacteristics ); 

			
			
			//for updating main measure
			int populationActive =0;
			int populationPassed =0;
			int popSkipped=0 ;
			int popNotEnoughQuestionsAnswered=0 ;
			double sumScoreActive = 0;
			double sumScorePassed =0;	
			
			if( schoolLvlResultsForCharacteristic != null){
				List<String> schoolMeasureResult  = schoolLvlResultsForCharacteristic.get(measureId);
				//there is already data for this combo-measure in the datastore at school lvl, find it to add to it
				if( schoolMeasureResult != null){
					populationActive = Integer.parseInt( schoolMeasureResult.get( 0 ) );
					populationPassed = Integer.parseInt( schoolMeasureResult.get( 1 ) );
					popSkipped = 		Integer.parseInt( schoolMeasureResult.get( 2 ) );
					popNotEnoughQuestionsAnswered= Integer.parseInt( schoolMeasureResult.get( 3 ) );
					sumScoreActive=  Double.parseDouble( schoolMeasureResult.get( 4 ) );
					sumScorePassed=  Double.parseDouble( schoolMeasureResult.get( 5 ) );
				}				
			}
			
			
			//if student passed filter question
			if( studentResults.containsKey(measureId)  ){

				List<String> studentResultFilter = studentResults.get(measureId);
				Double scorefilter = 		Double.parseDouble( studentResultFilter.get(0) );
				String supressedFilter = studentResultFilter.get(2); 
			
				

				//for each submeasure, update result based on if student answered the question
				for( String submeasureId : submeasures  ){
					String measureForOption = submeasureId;
					
					//for updating child measures
					int populationActive2 =0;
					int populationPassed2 =0;
					int popSkipped2=0 ;
					if( schoolLvlResultsForCharacteristic != null){
						List<String> schoolMeasureResult  = schoolLvlResultsForCharacteristic.get(measureForOption);
						//there is already data for this combo-measure in the datastore at school lvl, find it to add to it
						if( schoolMeasureResult != null){
							populationActive2 = Integer.parseInt( schoolMeasureResult.get( 0 ) );
							populationPassed2 = Integer.parseInt( schoolMeasureResult.get( 1 ) );
							popSkipped2 = 		Integer.parseInt( schoolMeasureResult.get( 2 ) );
						}	
					}

					if( supressedFilter.equals("0")){
						// passed filter question
						
						if(scorefilter>0){
														
							if( studentResults.containsKey(measureForOption)){
								//student has results for option
								List<String> studentList = studentResults.get(measureForOption);

								//data for this student ----
								Double score = 		Double.parseDouble( studentList.get(0) );
								String supressed = studentList.get(2); 
								//------------------

								//TODO get cutoff from elsewhere
								double cutoff = 1;

								//------------
								//ALG
								if( supressed.equals("0")){

									// student is above cutoff
									if( score >= cutoff )
										populationPassed2++;

									// score was not supressed for this user
									populationActive2++;	
								}
								else{
									//suppressed flag=1
									// user is supressed 

									popSkipped2++;
								}
							}


						}
					}
					
					//update submeasure
					saveSchoolResult( populationActive2, populationPassed2, popSkipped2,  popNotEnoughQuestionsAnswered,
							sumScoreActive,  sumScorePassed, measureForOption ,  studentCharacteristics);


				}

				//update parent measure
				saveSchoolResult( populationActive, populationPassed, popSkipped,  popNotEnoughQuestionsAnswered,
						sumScoreActive,  sumScorePassed, measureId ,  studentCharacteristics);



			}




		}
	}




	public void saveSchoolResult(int populationActive, int populationPassed, int popSkipped, int popNotEnoughQuestionsAnswered,
			double sumScoreActive, double sumScorePassed, String measureId , HashMap<String,String> characteristics){

		List<String> resultTotal = new ArrayList<String>();
		resultTotal.add(Integer.toString(populationActive));
		resultTotal.add(Integer.toString(populationPassed));
		resultTotal.add(Integer.toString(popSkipped));
		resultTotal.add(Integer.toString(popNotEnoughQuestionsAnswered));
		resultTotal.add(Double.toString(sumScoreActive));
		resultTotal.add(Double.toString(sumScorePassed));

		//overwrite whatever was already in the datastore					
		dataStoreSchool.putCharacteristicResults(characteristics, measureId , resultTotal);

	}


}
