package algorithms.Bullying_student;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureInfo;
/**
 * 
 * Similar to BullyPop_Ques_to_Measure_student but all likert values with score!=1 score is 0
 * 
 * 
 * 
 * Question to Measure, 
 * ie QtoM with different query
 * 
 * 
 *             /- question0[yes/no] -- newMeasure0 [ score=1 or score = 0 , or suppressed ]
 * Measure----- - question1[yes/no] -- newMeasure1 [ score=1 or score = 0 , or suppressed ]
 *             \_ question2[yes/no] -- newMeasure2 [ score=1 or score = 0 , or suppressed ]
 * 
 * 
 * See also
 * BullyPop_Ques_to_Measure_student which is similar but uses lad.value-1 for score
 * 
 * measure 1420
 * 
 * @author Evan K
 *
 */
public class QtoM_NoFilter_student {
	

	//private DataStore_school dataStoreSchool;
	private DataStore_student dataStoreStudent ;
	private Connection conn;
	public boolean useTemporaryTable = true;
	
	String query = 
			"Select  "
				+ "mq.questionId, "
				+ "a.surveyParticipantId,"
				+ "if (lad.value=1 , 1 , 0 ) as score "
				+ "From Answer4Rpt2016_temp As a  "
				+ "Join MeasureQuestion AS mq On mq.questionId = a.questionId  "
				+ "join LikertAnswerDetail as lad on lad.id=a.answerDetailId  "
				+ "And mq.measureId = ? "
				+ "order by a.surveyParticipantId;";

	public QtoM_NoFilter_student(Connection conn , DataStore_student dataStoreStudent , DataStore_school dataStoreSchool) {
		//this.dataStoreSchool=dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn = conn;
	}
	
	
	public void compute(String measureId , String surveyInstanceId ){

		System.out.println(this.getClass().getName());

		PreparedStatement stmt = null;
		ResultSet rs = null;

		MeasureInfo minfo = MeasureInfo.getInstance();
		List<Integer> questionsParentMeasure = minfo.getQuestions(Integer.parseInt(measureId));

		//create default student scores
		for( int currentQuestion:questionsParentMeasure){
			String newMeasure = minfo.getNewMeasureForQuestion(Integer.toString(currentQuestion));
			createDefaultStudentScores( newMeasure );
		}

		try {
			stmt = conn.prepareStatement(query);
			stmt.setInt(1, Integer.parseInt(measureId));
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				int i=1;
				int question 	= rs.getInt(i++);
				int participant = rs.getInt(i++);
				int score 	= rs.getInt(i++);
				int questionsAnswered  =1;
				String participantId = Integer.toString(participant);
				int suppressed =0;
				String measureForQuestionOption = minfo.getNewMeasureForQuestion(Integer.toString(question));
				
				saveStudentResult(participantId, measureForQuestionOption, score , questionsAnswered, suppressed);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			closeThings( rs , stmt );
		}
	}



	/**
	 * 
	 * @param surveyParticipantId string
	 * @param measure string
	 * @param score float
	 * @param questionsAnswered int
	 * @param suppressedFlag int
	 */
	public void saveStudentResult( String surveyParticipantId , String measure , float score , int questionsAnswered , int suppressedFlag ){
		List<String> result = new ArrayList<String>();
		result.add( Float.toString(score) );
		result.add( Integer.toString(questionsAnswered) );
		result.add( Integer.toString(suppressedFlag) );
		dataStoreStudent.addStudentResult( surveyParticipantId , measure, result);
	}

	/**
	 * Ensures each student at least has a score of zero and no questions answered for the measure
	 * ie prevent the occurrence of students with no PMS score for the measure
	 * @param measureId
	 */
	private void createDefaultStudentScores( String measureId ){

		List<String> students = dataStoreStudent.getAllStudents();

		for(String student: students){
			List<String> result = new ArrayList<String>();	
			result.add( "0" );
			result.add( "0" );
			result.add( "1" );
			dataStoreStudent.addStudentResult( student, measureId, result);
		}

	}

	
	public void closeThings(ResultSet rs , PreparedStatement stmt){
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
				stmt = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	

}
