package algorithms.Population;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.Utils;


public class ASumPopulationFromQuestionFilter {


	private DataStore_school dataStoreSchool;
	private DataStore_student dataStoreStudent;
	Connection conn;


	public static String ASumPopulationFromQuestionFilter =

			" Select  "
					+ "mq.questionId,  "
					+" sum(case when  lad.value=1 then 1 else 0 end)/ count(a.surveyParticipantId)*100 As qtotal  "

			+" From Answer4Rpt2016 As a  "
			+" Inner Join MeasureQuestion AS mq On mq.questionId = a.questionId  "
			+" Inner join LikertAnswerDetail as lad on lad.id=a.answerDetailId  "
			+" And mq.measureId = ? "
			+" And a.surveyInstanceId =? "
			+" INNER JOIN "
			+"("

			+" SELECT a.surveyParticipantId as pop  "
			+" FROM Answer4Rpt as a  "
			+" Inner join LikertAnswerDetail as lad on lad.id=a.answerDetailId "
			+" and questionId=?  "
			+"and surveyInstanceId =?  "
			+"and lad.value = 1  "

			+") "
			+"AS participantFilter "
			+"ON participantFilter.pop = a.surveyParticipantId  "
			+" Group By mq.questionId " ;


	//1253

	public ASumPopulationFromQuestionFilter(Connection conn ,  DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool ) {
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
	}


	public void compute( String measureId , String surveyInstanceId){
		// filterQuestionId is id of a question 
		
		algo_ASumPopulationFromQuestionFilter_measuresA( Integer.parseInt(measureId) , Integer.parseInt(surveyInstanceId), 1);
	}


	public void algo_ASumPopulationFromQuestionFilter_measuresA(int measureId, int surveyInstanceId, Integer filterQuestionId) {

		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;


		try {

			System.out.println( "School Level--> algo_ASumPopulationFromQuestionFilter_measuresA "
					+ "MeasureId: " + measureId 
					+ " SurveyInstanceId: "+ surveyInstanceId);

			String scoreComputationQuery = ASumPopulationFromQuestionFilter;

			stmt = conn.prepareStatement(scoreComputationQuery);
			stmt.setInt(1, measureId);
			stmt.setInt(2, surveyInstanceId);
			stmt.setInt(3, filterQuestionId);
			stmt.setInt(4, surveyInstanceId);
			resScoreComputation = stmt.executeQuery(); 


			Map <Integer, Double> questionPopulation = new HashMap<Integer, Double> ();
			int percentageTotal=0;

			while (resScoreComputation.next()) {
				questionPopulation.put(resScoreComputation.getInt(1), resScoreComputation.getDouble(2));
				resScoreComputation.getDouble(2); 
			}


			for (Integer key: questionPopulation.keySet()){
				percentageTotal += (int) Math.round(questionPopulation.get(key));
			}



			if(percentageTotal > 0){

				Map <Integer, Double> questionPopulationNormalized = new HashMap<Integer, Double> ();
				List<Double> toCheckwithChartData = new ArrayList<Double> ();

				if (percentageTotal != 100){					
					questionPopulationNormalized = Utils.normalizePopulationPercentage(questionPopulation);

					for (Integer key: questionPopulationNormalized.keySet()){
						toCheckwithChartData.add((double) Math.round(questionPopulation.get(key))) ;
						System.out.println(" Question :  " + key + "   -> " + questionPopulationNormalized.get(key)) ;
					}

				}
				else{
					for (Integer key: questionPopulation.keySet()){
						System.out.println(" Question :  " + key + "   -> " + (int) Math.round(questionPopulation.get(key))) ;
						toCheckwithChartData.add((double) Math.round(questionPopulation.get(key))) ;
					}
				}
			} 
			else 
				System.out.println("No computation Score (Student): " + surveyInstanceId);


			Utils.printResultSet(resScoreComputation);

			System.out.println();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}



	}

}
