package algorithms.Population;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.Utils;


/**
 * 
 * Tally of students who answered each option in a multiple choice
 * 
 *
 */
public class ASumPopulationFromQuestionFilterPie {

	private DataStore_school  dataStoreSchool;
	private DataStore_student dataStoreStudent;
	private Connection conn;


	public static String ASumPopulationFromQuestionFilterPie =

			" Select mq.questionId, count(a.surveyParticipantId) " + 
					" From Answer4Rpt2016 As a " +
					" INNER JOIN ("
					
					+ "SELECT a.surveyParticipantId as pop  "+
					" FROM Answer4Rpt2016 as a  "+
					" Inner join LikertAnswerDetail as lad on lad.id=a.answerDetailId "+
					" and questionId=?  "
					+ "and surveyInstanceId =?  "
					+ "and lad.value = 1  "
					
					+ ") "
					+ "AS participantFilter ON participantFilter.pop = a.surveyParticipantId  "+
					" Right Join MeasureQuestion AS mq On mq.questionId = a.questionId  "
					
					+ "And a.surveyInstanceId =? " +
					" where mq.measureId = ? " +
					
					" Group By mq.questionId " ;
	
	
	String q2 = "Select mq.questionId, count(a.surveyParticipantId) "
			+ "From Answer4Rpt2016 As a "
			+ "Right Join MeasureQuestion AS mq On mq.questionId = a.questionId  "
			+ "And a.surveyInstanceId = ? "
			+ "where mq.measureId = ? "
			+ "Group By mq.questionId ;";

	
	//1251,1252,1412 ASumPopulationFromQuestionFilterPie_measuresA


	public ASumPopulationFromQuestionFilterPie( Connection conn ,  DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool  )  {
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
	}
	
	
	public ASumPopulationFromQuestionFilterPie( Connection conn ){
		this.conn=conn;
	}
	
	
	
	
	public void compute( String measureId, String surveyInstanceId ){
		algo_ASumPopulationFromQuestionFilterPie_measuresA( Integer.parseInt(measureId), Integer.parseInt(surveyInstanceId) , 1 );
	}
	
	
	


	public void algo_ASumPopulationFromQuestionFilterPie_measuresA(int measureId, int surveyInstanceId, int filterQuestionId) {

		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;

		try {
			System.out.println( "School Level--> algo_ASumPopulationFromQuestionFilterPie_measuresA "
					+ "MeasureId: " + measureId + " " 
					+ "SurveyInstanceId: "+ surveyInstanceId);

			/*
			String scoreComputationQuery = ASumPopulationFromQuestionFilterPie;
			stmt = conn.prepareStatement(scoreComputationQuery);
			stmt.setInt(1, filterQuestionId);
			stmt.setInt(2, surveyInstanceId);
			stmt.setInt(3, surveyInstanceId);
			stmt.setInt(4, measureId);
			resScoreComputation = stmt.executeQuery(); 
			*/
			
			String scoreComputationQuery = q2;
			stmt = conn.prepareStatement(scoreComputationQuery);
			stmt.setInt(1, surveyInstanceId);
			stmt.setInt(2, measureId);
			resScoreComputation = stmt.executeQuery(); 
			
			

			Map <Integer, Double> questionPopulation = new HashMap<Integer, Double> ();

			int populationTotal=0;
			int percentageTotal=0;

			while (resScoreComputation.next()) {
				questionPopulation.put(resScoreComputation.getInt(1), resScoreComputation.getDouble(2));
				populationTotal += resScoreComputation.getDouble(2) ; 
				// System.out.println ( resScoreComputation.getInt(1) + "  " + resScoreComputation.getDouble(2) );
			}


			if(populationTotal > 0){
				for (Integer key: questionPopulation.keySet()){
					questionPopulation.replace(key, questionPopulation.get(key)*100/populationTotal ) ;
					percentageTotal += (int) Math.round(questionPopulation.get(key));
				}
				
				Map <Integer, Double> questionPopulationNormalized = new HashMap<Integer, Double> ();
				List<Double> schoolScore= new ArrayList<Double>() ;

				if (percentageTotal != 100){
					
					//See Utils
					questionPopulationNormalized = Utils.normalizePopulationPercentage(questionPopulation);

					for (Integer key: questionPopulationNormalized.keySet()){
						System.out.println("Question [N] :  " + key + "   -> " + questionPopulationNormalized.get(key)) ;
						schoolScore.add(questionPopulationNormalized.get(key)) ;
					}

				}
				else{ 
					for (Integer key: questionPopulation.keySet()){
						System.out.println("Question :  " + key + "   -> " + questionPopulation.get(key)) ;
						schoolScore.add( (double) Math.round(questionPopulation.get(key))) ;
					}
				}


			}else 
				System.out.println("No computation Score (Student): " + populationTotal);

			System.out.println();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
		}




	}

}
