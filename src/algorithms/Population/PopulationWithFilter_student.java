package algorithms.Population;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureInfo;
import utilities.Utils;

public class PopulationWithFilter_student {


	private DataStore_school  dataStoreSchool;
	private DataStore_student dataStoreStudent;
	private Connection conn;
	
	//EX
	/*
	
	SELECT 
    surveyParticipantId,
    a.questionId,
    measureId,
    lad.value,
    a.answerDetailId,
    mq.optionNum
	FROM
    Answer4Rpt2016 AS a
        INNER JOIN MeasureQuestion AS mq ON mq.questionId = a.questionId
        INNER JOIN LikertAnswerDetail AS lad ON lad.id = a.answerDetailId
        where 
        mq.measureId IN (1261 , 1262, 1263, 1264, 1265, 1266, 1267, 1268)
		and surveyInstanceId = 73634
        order by surveyParticipantId, measureId;
	 
	 */
	
	private String query = 
			"SELECT "
			+ " surveyParticipantId,"
			+ " measureId,"
			+ " lad.value,"
			+ " FROM"
			+ " Answer4Rpt2016 AS a"
			+ " INNER JOIN MeasureQuestion AS mq ON mq.questionId = a.questionId"
			+ " INNER JOIN LikertAnswerDetail AS lad ON lad.id = a.answerDetailId"
			+ " where"
			+ " mq.measureId IN (1261 , 1262, 1263, 1264, 1265, 1266, 1267, 1268)"
			+ "	and surveyInstanceId = 73634 "
			+ "order by surveyParticipantId, measureId;";
	
	
	public PopulationWithFilter_student(Connection conn ,  DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool ) {
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
	}
		
	public void compute( String measureId , String surveyInstanceId ){
		findAnswers(0,0);
	}
	
	
	public void findAnswers(int measureId, int surveyInstanceId) {

		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;

		try {
						
			stmt = conn.prepareStatement(query);
			resScoreComputation = stmt.executeQuery(); 
			while (resScoreComputation.next()) {
				
			}
						
		//	Utils.printResultSet(resScoreComputation);
					
		
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		
	}
}
