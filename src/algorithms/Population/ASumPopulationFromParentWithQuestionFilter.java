package algorithms.Population;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utilities.DataStore_school;
import utilities.DataStore_student;

public class ASumPopulationFromParentWithQuestionFilter {

	private DataStore_school dataStoreSchool;
	private DataStore_student dataStoreStudent;
	Connection conn;


	public ASumPopulationFromParentWithQuestionFilter(Connection conn ,  DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool ) {
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
	}


	public static String ASumPopulationFromParentWithQuestionFilter =
			" Select  mq.questionId,  "+
			" count(a.surveyParticipantId)*100/participants.pop as qtotal  "+
			" From Answer4Rpt As a  "+
			
			" INNER JOIN ("
			
			+ "SELECT a.surveyParticipantId as pop  "+
			" FROM Answer4Rpt as a  "+
			" Inner join LikertAnswerDetail as lad on lad.id=a.answerDetailId "+
			" and questionId=?  "
			+ "and surveyInstanceId =?  "
			+ "and lad.value = 1  ) "
			
			+ "AS participantFilter ON participantFilter.pop = a.surveyParticipantId  "+
		   	
			" INNER JOIN ("
		   + "SELECT  count(a.surveyParticipantId) as pop  "+
		   " FROM Answer4Rpt as a  "+
		   " Inner join LikertAnswerDetail as lad on lad.id=a.answerDetailId "+
		   	" and questionId=?  "
		   	+ "and surveyInstanceId =?  "
		   	+ "and lad.value = 1  ) "
		   	+ "AS participants  "+
			
		   	" Right Join MeasureQuestion AS mq On "
			+ "mq.questionId = a.questionId  "
			+ "And a.surveyInstanceId =? "+
			" Where mq.measureId = ? "+
		   " Group By mq.questionId "  ;



	public void algo_ASumPopulationFromParentWithQuestionFilter_measuresA(int measureId, int surveyInstanceId,
			Integer filterQuestionId) {

		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;

		try {
			System.out.println( "School Level--> algo_ASumPopulationFromParentWithQuestionFilter_measuresA " + "MeasureId: " + measureId + " " + "SurveyInstanceId: "+ surveyInstanceId);

			String scoreComputationQuery = ASumPopulationFromParentWithQuestionFilter;

			stmt = conn.prepareStatement(scoreComputationQuery);

			stmt.setInt(1, filterQuestionId);
			stmt.setInt(2, surveyInstanceId);
			stmt.setInt(3, filterQuestionId);
			stmt.setInt(4, surveyInstanceId);
			stmt.setInt(5,  surveyInstanceId );
			stmt.setInt(6, measureId);
			resScoreComputation = stmt.executeQuery(); 
			
			Map <Integer, Double> questionPopulation = new HashMap<Integer, Double> ();

			int percentageTotal=0;
			while (resScoreComputation.next()) {
				questionPopulation.put(resScoreComputation.getInt(1), resScoreComputation.getDouble(2));
			}
			
			for (Integer key: questionPopulation.keySet()){
				//questionPopulation.replace(key, questionPopulation.get(key)*100/populationTotal ) ;
				percentageTotal += (int) Math.round(questionPopulation.get(key));
			}

			Map <Integer, Double> questionPopulationNormalized = new HashMap<Integer, Double> ();
			List<Double> toCheckwithChartData = new ArrayList<Double> ();

			if(percentageTotal > 0){
				if (percentageTotal != 100){
					//NEED
					//	questionPopulationNormalized = normalizePopulationPercentage(questionPopulation);

					for (Integer key: questionPopulationNormalized.keySet()){
						System.out.println(" Question :  " + key + "   -> " + questionPopulationNormalized.get(key)) ;
						toCheckwithChartData.add(questionPopulationNormalized.get(key)) ;
					}

				}
				else{
					for (Integer key: questionPopulation.keySet()){
						System.out.println(" Question :  " + key + "   -> " + (int) Math.round(questionPopulation.get(key))) ;
						toCheckwithChartData.add((double) Math.round(questionPopulation.get(key))) ;
					}
				}
			}
			else
				System.out.println("No computation Score (Student): " + surveyInstanceId);

			System.out.println();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
