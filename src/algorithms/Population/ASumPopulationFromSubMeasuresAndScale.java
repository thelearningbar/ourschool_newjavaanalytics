package algorithms.Population;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureInfo;
import utilities.Utils;




public class ASumPopulationFromSubMeasuresAndScale {

	private DataStore_school  dataStoreSchool;
	private DataStore_student dataStoreStudent;
	private Connection conn;
	
	
	public static String ASumPopulationFromSubMeasuresAndScaleHead =
			 " Select "
			 + "mq.questionId, "
			 + "avg(lad.value-1) * mq.optionNum / (mq.optionNum-1) " +
			 " From Answer4Rpt2016_temp As a  " +
			 " Inner Join MeasureQuestion AS mq On mq.questionId = a.questionId  " +
			 " Inner join LikertAnswerDetail as lad on lad.id= a.answerDetailId  " +
	         " And mq.measureId in ( "  ;
	
	
	//changed to group by measureId from questionId
	public static String ASumPopulationFromSubMeasuresAndScaleTail =
			 " ) Group By mq.measureId ;"  ; 
	
	
	
	/*
	public static String ASumPopulationFromSubMeasuresAndScaleHead =
			 " Select mq.questionId, avg(lad.value-1) * mq.optionNum / (mq.optionNum-1) " +
			 " From Answer4Rpt_temp As a  " +
			 " Inner Join MeasureQuestion AS mq On mq.questionId = a.questionId  " +
			 " Inner join LikertAnswerDetail as lad on lad.id= a.answerDetailId  " +
	         " And mq.measureId in ( "  ;
	public static String ASumPopulationFromSubMeasuresAndScaleTail =
			 " ) And a.surveyInstanceId = ?  " + 
			 " Group By mq.questionId "  ;
	*/
	
	
	//test instance: 73634
	
	//1255 ASumPopulationFromSubMeasuresAndScale_measuresA (1261-1268)
	
	public static String FindSubMeausuresList=
			" Select id From Measure where compositeMeasureId= ?";
	
	
	
	public ASumPopulationFromSubMeasuresAndScale( Connection conn ,  DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool ) {
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
	}
	
	public void compute( String measureId, String surveyInstanceId){
		algo_ASumPopulationFromSubMeasuresAndScale_measuresA(Integer.parseInt(measureId), Integer.parseInt(surveyInstanceId));
	}
	

	public void algo_ASumPopulationFromSubMeasuresAndScale_measuresA(int measureId, int surveyInstanceId) {

		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;

		try {
			System.out.println( "School Level--> algo_ASumPopulationFromSubMeasuresAndScale_measuresA " 
		+ " MeasureId: " + measureId 
		+ " SurveyInstanceId: "+ surveyInstanceId);
						
			//BUILD QUERY
			List<String> subMeasures = MeasureInfo.getInstance().getSubMeasures(Integer.toString(measureId));
			
			ArrayList<Integer> subMeausuresList = new ArrayList<Integer>();
			for( String measure:subMeasures){
				subMeausuresList.add(Integer.parseInt(measure));
			}
			
			StringBuilder stringBuilder = new StringBuilder();
			for (int i=0; i<subMeausuresList.size();i++){
				stringBuilder.append("?,");
			}			
			String scoreComputationQuery = ASumPopulationFromSubMeasuresAndScaleHead + 
					                stringBuilder.deleteCharAt(stringBuilder.length()-1).toString() + 
					                ASumPopulationFromSubMeasuresAndScaleTail; 
			
			stmt = conn.prepareStatement(scoreComputationQuery);
			int i;
			for ( i=0; i<subMeausuresList.size();i++){
				stmt.setInt(i+1, subMeausuresList.get(i));
			}
			
			//dont need with temporary table
			//stmt.setInt(i+1, surveyInstanceId);
			
			scoreComputationQuery = stringBuilder.toString();
			resScoreComputation = stmt.executeQuery(); 
			
			
			Map <Integer, Double> questionPopulation = new HashMap<Integer, Double> ();
			while (resScoreComputation.next()) {
				questionPopulation.put(resScoreComputation.getInt(1), resScoreComputation.getDouble(2));
			}
			List<Double> toCheckwithChartData = new ArrayList<Double> ();
						
			
			for (Integer key: questionPopulation.keySet()){
				questionPopulation.replace(key, Math.round(questionPopulation.get(key) *10.0)/10.0) ;	
				System.out.println(" Question :  " + key + "   -> " + questionPopulation.get(key)) ;
				toCheckwithChartData.add(questionPopulation.get(key)) ; 				
			}
			
			
			//Utils.printResultSet(resScoreComputation);
					
		
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		
	}
	
	/*
	 * ORIGINAL FOR REFERENCE
	 * 
	public void algo_ASumPopulationFromSubMeasuresAndScale_measuresA(int measureId, int surveyInstanceId) {

		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;

		try {
			System.out.println( "School Level--> algo_ASumPopulationFromSubMeasuresAndScale_measuresA " 
		+ " MeasureId: " + measureId 
		+ " SurveyInstanceId: "+ surveyInstanceId);

			
			String scoreComputationQuery = FindSubMeausuresList;
			stmt = conn.prepareStatement(scoreComputationQuery);			
			stmt.setInt(1, measureId);
			resScoreComputation = stmt.executeQuery(); 
			
			ArrayList<Integer> subMeausuresList = new ArrayList<Integer>();

			while (resScoreComputation.next()) {
				subMeausuresList.add( resScoreComputation.getInt(1));
			}

			scoreComputationQuery= null;
						
			StringBuilder stringBuilder = new StringBuilder();
			
			for (int i=0; i<subMeausuresList.size();i++){
				stringBuilder.append("?,");
			}
			
			scoreComputationQuery = ASumPopulationFromSubMeasuresAndScaleHead + 
					                stringBuilder.deleteCharAt(stringBuilder.length()-1).toString() + 
					                ASumPopulationFromSubMeasuresAndScaleTail; 

			stmt = conn.prepareStatement(scoreComputationQuery);

			int i;
			for ( i=0; i<subMeausuresList.size();i++){
				stmt.setInt(i+1, subMeausuresList.get(i));
			}
			
			stmt.setInt(i+1, surveyInstanceId);			
			scoreComputationQuery = stringBuilder.toString();
			resScoreComputation = stmt.executeQuery(); 
			
			Map <Integer, Double> questionPopulation = new HashMap<Integer, Double> ();
			
			while (resScoreComputation.next()) {
				questionPopulation.put(resScoreComputation.getInt(1), resScoreComputation.getDouble(2));
			
			}

			List<Double> toCheckwithChartData = new ArrayList<Double> ();
			
			
			for (Integer key: questionPopulation.keySet()){
				questionPopulation.replace(key, Math.round(questionPopulation.get(key) *10.0)/10.0) ;
				
				System.out.println(" Question :  " + key + "   -> " + questionPopulation.get(key)) ;
								
				toCheckwithChartData.add(questionPopulation.get(key)) ; 				
			}
					
		
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	*/

}
