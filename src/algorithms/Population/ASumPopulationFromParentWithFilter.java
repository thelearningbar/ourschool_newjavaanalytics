package algorithms.Population;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utilities.DataStore_school;
import utilities.DataStore_student;

public class ASumPopulationFromParentWithFilter {
	
	private DataStore_student dataStoreStudent;
	private DataStore_school dataStoreSchool;
	Connection conn;

	

	/*
	public static String ASumPopulationFromParentWithFilter =

			" Select mq.questionId, " + 
			" count(a.surveyParticipantId)*100/populationFilter.pop As qtotal " +
			" From Answer4Rpt As a " +
			" INNER JOIN ( " 
				
				+" SELECT a.surveyParticipantId  as pop " 
				+" FROM `ParticipantMeasureScores` As a " 
				+" Where a.measureId = ? "
				+ " and a.`surveyInstanceId` = ?  "
				+ " and a.score >=? "
			
			+ ") "

			+ " AS populationFilterManual "
			+ " on  populationFilterManual.pop = a.surveyParticipantId " +
			
			" INNER JOIN ( " +
				
				" SELECT count(a.surveyParticipantId)  as pop" +
				" FROM `ParticipantMeasureScores` As a " +
				" Where a.measureId = ? "
				+ " and s.`surveyInstanceId` = ?  "
				+ " and a.score >= ? "
			
			+ ") AS populationFilter " +
			
			" Right Join MeasureQuestion AS mq On mq.questionId = a.questionId  And a.surveyInstanceId =? " +
			" where mq.measureId = ?  " +
			" Group By mq.questionId " ;


	public ASumPopulationFromParentWithFilter( Connection conn ,  DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool ) {
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent =dataStoreStudent;
		this.conn=conn;
	}
	
	
	public void compute( String measureId, String surveyInstanceId){
		
		algo_ASumPopulationFromParentWithFilter_measuresA( Integer.parseInt(measureId) , Integer.parseInt(surveyInstanceId), 1 , 1.0 );
		
	}


	public void algo_ASumPopulationFromParentWithFilter_measuresA(
			int measureId, int surveyInstanceId, 
			int parentMeasureForPopulation, double parentMeasureCutOff) {

		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;

		try {
			System.out.println( "School Level--> algo_ASumPopulationFromParentWithFilter_measuresA "
					+ "MeasureId: " + measureId + " " 
					+ "SurveyInstanceId: "+ surveyInstanceId);

			String scoreComputationQuery = ASumPopulationFromParentWithFilter;

			stmt = conn.prepareStatement(scoreComputationQuery);
			stmt.setInt(1, parentMeasureForPopulation);
			stmt.setInt(2, surveyInstanceId);
			stmt.setDouble(3,parentMeasureCutOff);
			
			stmt.setInt(4, parentMeasureForPopulation);
			stmt.setInt(5, surveyInstanceId);
			stmt.setDouble(6,parentMeasureCutOff);
			
			stmt.setInt(7, surveyInstanceId);
			stmt.setInt(8, measureId);
			resScoreComputation = stmt.executeQuery(); 

			Map<Integer, Double>  mapQuestionScore= new HashMap<Integer, Double> () ;

			while (resScoreComputation.next()) {
				mapQuestionScore.put(resScoreComputation.getInt(1), resScoreComputation.getDouble(2));
			}

			List<Double> toCheckwithChartData = new ArrayList<Double> ();
			if (mapQuestionScore.size()>0){
				for (Integer key: mapQuestionScore.keySet()){
					System.out.println("Question : " + key + " Score: " +  mapQuestionScore.get(key));
					toCheckwithChartData.add( mapQuestionScore.get(key)) ;
				}
			}
			else{ 
				//System.out.println("No computation Score (Student): " +  " MeasureId : " + measureId  + " SurveyInstanceId :" + surveyInstanceId  + " " +  "Method   >> " +  new Object(){}.getClass().getEnclosingMethod().getName()  + " >> " + getCallingMethodName(3) );
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

*/
	
}
