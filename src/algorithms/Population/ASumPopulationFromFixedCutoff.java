package algorithms.Population;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ASumPopulationFromFixedCutoff {


	Connection conn;

	public ASumPopulationFromFixedCutoff(Connection conn) {
		this.conn=conn;
	}

	public static String ASumPopulationFromFixedCutoff =
			" Select  mq.questionId,  "+
					" sum(case when  lad.value=1 then 1 else 0 end)/ count(a.surveyParticipantId)*100 As qtotal  "+
					" From Answer4Rpt As a  "+
					" Inner Join MeasureQuestion AS mq On mq.questionId = a.questionId  "+
					" Inner join LikertAnswerDetail as lad on lad.id=a.answerDetailId  "+
					" And mq.measureId = ? And a.surveyInstanceId =? "+
					" Group By mq.questionId " ;

	public void algo_ASumPopulationFromFixedCutoff_measuresA(int measureId, int surveyInstanceId) {
		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;

		try {
			System.out.println( "School Level--> algo_ASumPopulationFromFixedCutoff_measuresA " + "MeasureId: " 
					+ measureId + " " + "SurveyInstanceId: "+ surveyInstanceId);
			String scoreComputationQuery = ASumPopulationFromFixedCutoff;

			stmt = conn.prepareStatement(scoreComputationQuery);
			stmt.setInt(1, measureId);
			stmt.setInt(2, surveyInstanceId);

			resScoreComputation = stmt.executeQuery(); 
			Map <Integer, Double> questionPopulation = new HashMap<Integer, Double> ();
			int percentageTotal=0;

			while (resScoreComputation.next()) {
				questionPopulation.put(resScoreComputation.getInt(1), resScoreComputation.getDouble(2));
				percentageTotal += (int) Math.round(resScoreComputation.getDouble(2));
			}

			if(percentageTotal > 0){
				Map <Integer, Double> questionPopulationNormalized = new HashMap<Integer, Double> ();
				List<Double> toCheckwithChartData = new ArrayList<Double> ();
				if (percentageTotal != 100){
					//questionPopulationNormalized = normalizePopulationPercentage(questionPopulation);
					for (Integer key: questionPopulationNormalized.keySet()){
						System.out.println(" Question :  " + key + "   -> " + questionPopulationNormalized.get(key)) ;
						toCheckwithChartData.add(questionPopulationNormalized.get(key)) ;
					}
				}
				else{
					/*
					for (Integer key: questionPopulation.keySet()){
					}
					*/
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{

		}
	}


}
