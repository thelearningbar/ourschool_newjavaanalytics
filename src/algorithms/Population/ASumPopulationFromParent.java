package algorithms.Population;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureInfo;
import utilities.Utils;

public class ASumPopulationFromParent {

	private DataStore_student dataStoreStudent;
	private DataStore_school   dataStoreSchool;
	Connection conn;

	//For:
	//1254 ASumPopulationFromParent_measuresA
	
	//outer part finds percentage of users who answered each __option?___ (submeasure) ???
	//inner select gets number of participants in PMS who have the measure
	public static String ASumPopulationFromParent =

			" Select "
			+ " mq.questionId, " 
			+" count(a.surveyParticipantId) * 100/populationFilter.pop As qtotal " 
				
			+" From Answer4Rpt As a " 
			+" Inner Join MeasureQuestion AS mq On mq.questionId = a.questionId" 
			+" And mq.measureId = ? "
			+ " And a.surveyInstanceId = ? " 
			
			+" INNER JOIN ( " 
			
			+" SELECT count(a.surveyParticipantId)  as pop" 
			+" FROM ParticipantMeasureScores As a " 
			+" Where a.measureId = ? "
			+ "and a.surveyInstanceId = ?  "
			+ "and a.score >= ? "
			
			+ ") AS populationFilter " 
			
			+" Group By mq.questionId " ;
	
	

	
	public ASumPopulationFromParent(Connection conn ,  DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool ) {
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent = dataStoreStudent;
		this.conn = conn;
	}
	
	
	public void compute(String measureId , String surveyInstanceId ){
		
		algo_ASumPopulationFromParent_measuresA( Integer.parseInt(measureId) , 
				Integer.parseInt(surveyInstanceId) ,0,10.0 );
	}
	
	
	
	//TODO version that works with our system
	public void comp(){
		
		//just count how many answered each question
		
		String measureId="1116";
		
		MeasureInfo minfo = MeasureInfo.getInstance();
		
		//find cutoff
		float cutoff = -1;
		String cutof = minfo.getCutoff(measureId);
		cutoff = Float.parseFloat(cutof);
		
		List<String> students = dataStoreStudent.getAllStudents();
		
		
		
		
		
		if(  true ){

			String newMeasureId =  measureId;
			List<String> submeasures = minfo.getSubMeasures( newMeasureId );
			
			//for each submeasure
			for( String submeasure : submeasures){
				
				
				//for each participant
				for( String student : students){
										
					//count no students not suppressed
					
					//active,passed ++  for each who answered question		
					
					//821626377  {1217=[0, 0, 1], 1216=[0, 0, 1], 1215=[7.5, 2, 1], 1214=[0.0, 2, 1]}
					HashMap<String, List<String>> studentResults = dataStoreStudent.getStudentResults(student);
		
					//821626377  {1=3, 2=10, 42=5, 43=3}
					HashMap<String, String> studentCharacteristics  = dataStoreStudent.getStudentCharacteristics(student);
		
					//  results for this combination of characteristics
					//  measureId , [active pop, passed pop , noQ pop , notEnough pop, active scor,passed score]
					HashMap<String, List<String>>  schoolResults = dataStoreSchool.getCharacteristicResults( studentCharacteristics ); 
					int popSkipped=0 ;
					int popNotEnoughQuestionsAnswered=0 ;
		
		
					double sumScoreActive = 0;
					double sumScorePassed =0;
					int populationActive =0;
					int populationPassed =0;
					

					if( schoolResults != null){

						//[0, 0, 1, 0, 0.0, 0.0] 
						List<String> schoolMeasureResult  = schoolResults.get(submeasure);

						//there is already data for this combo-measure in the datastore at school lvl, find it to add to it
						if( schoolMeasureResult != null){

							populationActive = Integer.parseInt( schoolMeasureResult.get( 0 ) );
							populationPassed = Integer.parseInt( schoolMeasureResult.get( 1 ) );
							popSkipped = 		Integer.parseInt( schoolMeasureResult.get( 2 ) );
							popNotEnoughQuestionsAnswered= Integer.parseInt( schoolMeasureResult.get( 3 ) );
							sumScoreActive=  Double.parseDouble( schoolMeasureResult.get( 4 ) );
							sumScorePassed=  Double.parseDouble( schoolMeasureResult.get( 5 ) );

						}				

					}
					
					
					
					if( studentResults!=null && studentResults.containsKey(submeasure)  ){				

						//list of results for measure which beloongs to a student
						// 1217=[9.0625, 8, 0]
						List<String> studentList = studentResults.get(submeasure);

						//data for this student ----
						Double score = 		Double.parseDouble( studentList.get(0) );
						int questionsAnswered = Integer.parseInt( studentList.get(1) );
						String supressed = studentList.get(2); 
						//------------------


						
						//------------
						//ALG
						if( supressed.equals("0")){
							// score was not supressed for this user

							if( Utils.round(score, 2) >= cutoff ){
								// student is above cutoff

								// ie  student is active and passed, (passed is subset of active	
								sumScoreActive+=Utils.round(score, 2);
								sumScorePassed+=Utils.round(score, 2);
								populationActive++;			
								populationPassed++;

							}
							else{
								// student is under cutoff, but still active because they were not suppressed

								//debug
								sumScoreActive+=Utils.round(score, 2);
								populationActive++;
							}

						}
						else{
							//suppressed flag=1
							// user is supressed for not enough/any questions

							if( questionsAnswered >=1 ){
								// did not answer enough questions
								popNotEnoughQuestionsAnswered++;
							}
							else{
								// did not answer any questions
								popSkipped++;
							}
						}


					}
					
					
					//------------------------------------------------
					//update the data for combination-measure

					List<String> result = new ArrayList<String>();

					result.add(Integer.toString(populationActive));
					result.add(Integer.toString(populationPassed));
					result.add(Integer.toString(popSkipped));
					result.add(Integer.toString(popNotEnoughQuestionsAnswered));

					result.add(Double.toString(sumScoreActive));
					result.add(Double.toString(sumScorePassed));

					//overwrite whatever was already in the datastore					
					dataStoreSchool.putCharacteristicResults(studentCharacteristics, submeasure, result);
					//------------------------------------------
					
					
				}
			}		
		}
		
		
		
				

		
		
		
		
		
	}
	
	
	
	
	


	/**
	 * 
	 * @param measureId
	 * @param surveyInstanceId
	 * @param parentMeasureForPopulation  parent Measure Id
	 * @param parentMeasureCutOff  cutoff of parent measure
	 */
	public void algo_ASumPopulationFromParent_measuresA(
			int measureId, int surveyInstanceId, 
			int parentMeasureForPopulation, double parentMeasureCutOff) {

		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;

		try {
			System.out.println( "School Level--> algo_ASumPopulationFromParent_measuresA "
					+ "MeasureId: " + measureId + " " 
					+ "SurveyInstanceId: "+ surveyInstanceId);

			String scoreComputationQuery =ASumPopulationFromParent;


			stmt = conn.prepareStatement(scoreComputationQuery);
			stmt.setInt(	1, measureId		);
			stmt.setInt(	2, surveyInstanceId	);
			stmt.setInt(	3, parentMeasureForPopulation);
			stmt.setInt(	4, surveyInstanceId	);
			stmt.setDouble(	5, parentMeasureCutOff	);
			resScoreComputation = stmt.executeQuery(); 

			Map<Integer, Double>  mapQuestionScore= new HashMap<Integer, Double> () ;

			while (resScoreComputation.next()) {
				mapQuestionScore.put(resScoreComputation.getInt(1), resScoreComputation.getDouble(2));
			}

			for (Integer key: mapQuestionScore.keySet()){
				System.out.println("Question : " + key + " Score: " +  mapQuestionScore.get(key));
			}


		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}


	}
	
	
	
	

}
