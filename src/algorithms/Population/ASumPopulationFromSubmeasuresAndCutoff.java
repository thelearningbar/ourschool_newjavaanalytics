package algorithms.Population;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import utilities.DataStore_school;
import utilities.DataStore_student;

public class ASumPopulationFromSubmeasuresAndCutoff {

	private DataStore_student dataStoreStudent;
	private DataStore_school  dataStoreSchool;
	private Connection conn;

	
	public ASumPopulationFromSubmeasuresAndCutoff(Connection conn ){
		this.conn = conn;
	}
	
	
	public static String FindSubMeausuresList=
			" Select id From tlb_branding.Measure where compositeMeasureId= ?";
	
	
	public static String ASumPopulationFromSubMeasuresAndCutoffHead =
		" Select mq.questionId, sum(if(lad.value>= ?,100,0))/ sum(if(lad.value>=0,1,0)) " +
		" From Answer4Rpt2016 As a  "  +
		" Inner Join MeasureQuestion AS mq On mq.questionId = a.questionId  " +
		" Inner join LikertAnswerDetail as lad on lad.id= a.answerDetailId  " +
		" And mq.measureId in ( "  ;
	
	public static String ASumPopulationFromSubMeasuresAndCutoffTail =
				" ) And a.surveyInstanceId = ?  " + 
					" Group By mq.questionId "  ;
	
	
	public String query = 
			"Select mq.questionId, sum(if(lad.value>= ?,100,0))/ sum(if(lad.value>=0,1,0)) as x "
			+ "From Answer4Rpt2016 As a  "
			+ "Inner Join MeasureQuestion AS mq On mq.questionId = a.questionId  "
			+ "Inner join LikertAnswerDetail as lad on lad.id= a.answerDetailId  "
			+ "And mq.measureId in ( Select id From tlb_branding.Measure where compositeMeasureId= 1415 ) "
			+ "And a.surveyInstanceId =?  "
			+ "Group By mq.questionId ;";
	
	public void algo_ASumPopulationFromSubMeasuresAndCutoff_measuresA(int measureId, int surveyInstanceId, Double measureCutoff) {

		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;

		try {

			/*
			//finding submeasures of measure
			String scoreComputationQuery = FindSubMeausuresList;
			stmt = conn.prepareStatement(scoreComputationQuery);
			stmt.setInt(1, measureId);
			resScoreComputation = stmt.executeQuery(); 
			ArrayList<Integer> subMeausuresList = new ArrayList<Integer>();
			while (resScoreComputation.next()) {
				subMeausuresList.add( resScoreComputation.getInt(1));
			}
*/
						
			//adding the submeasures found to the measureId in (... part of query
			
			/*
			stmt = null;
			resScoreComputation = null;
			scoreComputationQuery= null;
			StringBuilder stringBuilder = new StringBuilder();
			for (int i=0; i<subMeausuresList.size();i++){
				stringBuilder.append("?,");
			}
			scoreComputationQuery = ASumPopulationFromSubMeasuresAndCutoffHead + 
					stringBuilder.deleteCharAt(stringBuilder.length()-1).toString() + 
					ASumPopulationFromSubMeasuresAndCutoffTail; 
			stmt = conn.prepareStatement(scoreComputationQuery);
			stmt.setDouble(1, measureCutoff);
			int i;
			for ( i=0; i<subMeausuresList.size();i++){
				stmt.setInt(i+2, subMeausuresList.get(i));
			}
			stmt.setInt(i+2, surveyInstanceId);
			scoreComputationQuery = stringBuilder.toString();
			*/
			
			String scoreComputationQuery =query;
			stmt = conn.prepareStatement(scoreComputationQuery);
			
			stmt.setDouble(1, measureCutoff);
			stmt.setInt(2, surveyInstanceId);
			resScoreComputation = stmt.executeQuery(); 

			
			
			Map <Integer, Double> questionPopulation = new HashMap<Integer, Double> ();

			while (resScoreComputation.next()) {
				questionPopulation.put(resScoreComputation.getInt(1), resScoreComputation.getDouble(2));
			}
			
			for (Integer key: questionPopulation.keySet()){
				questionPopulation.replace(key, Math.round(questionPopulation.get(key) *10.0)/10.0) ;
				System.out.println(" Question :  " + key + "   -> " + questionPopulation.get(key)) ;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			
		}
	}
	
	
	
}
