package algorithms.ACutoff_school;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Alg_ACutoffDouble_Original {

	Connection conn;
	
	public static String AScoreCutOffPlus =	
			" Select SUM(if(p.score>= ?, 100, 0)), "
			+ "SUM(if(p.score>= ?, 100, 0)), count(surveyParticipantId) "
			+ "from ParticipantMeasureScores as p "
			+ "INNER JOIN Measure as m on m.id = p.measureId where measureId = ? and SurveyInstanceId = ? " ;
	
	public Alg_ACutoffDouble_Original(Connection conn) {
		this.conn=conn;
	}
	
	


	public void algo_ACutOffDouble_measuresP(int measureId, int surveyInstanceId, List<Double> list) {

		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;

		try {
			System.out.println( "School Level (Cutoff+)--> " + "MeasureId: " + measureId + " " + "SurveyInstanceId: "+ surveyInstanceId);

			String scoreComputationQuery = AScoreCutOffPlus;
			stmt = conn.prepareStatement(scoreComputationQuery);
			stmt.setDouble(1, list.get(0));
			stmt.setDouble(2, list.get(1));
			stmt.setInt(3, measureId);
			stmt.setInt(4, surveyInstanceId);
			resScoreComputation = stmt.executeQuery(); 


			double[] schoolScore = new double[]{-1.0,-1.0};
			int numberOfParticipant= 0;

			while (resScoreComputation.next()) {
				schoolScore[0] = resScoreComputation.getDouble(1);
				schoolScore[1] = resScoreComputation.getDouble(2);

				numberOfParticipant = resScoreComputation.getInt(3);

			}



			if (((schoolScore[0] != -1 && schoolScore[0] != 0) || (schoolScore[1] != -1 && schoolScore[1] != 0))   ){
				schoolScore[0] = schoolScore[0]/numberOfParticipant;
				schoolScore[1] = schoolScore[1]/numberOfParticipant;

				System.out.println("cutOff1 : " + schoolScore[0] +'\n' + "cutOff2 : " +   schoolScore[1] + '\n' 
						+   "cutOff3 : " +  Math.abs(schoolScore[0]-schoolScore[1])  +  '\n' +"participants: " + numberOfParticipant); 
				System.out.println();

				//comparisonWithChartData( conn, measureId, surveyInstanceId, schoolScore) ;
				System.out.println();
			}
			else if (((schoolScore[0] != -1 && schoolScore[0] == 0) || (schoolScore[1] != -1 && schoolScore[1] == 0))  ){


				System.out.println("cutOff1 : " + schoolScore[0] +'\n' + "cutOff2 : " +   schoolScore[1] + '\n' 
						+   "cutOff3 : " +  Math.abs(schoolScore[0]-schoolScore[1])  +  '\n' +"participants: " + numberOfParticipant); 

				System.out.println();

				//comparisonWithChartData( conn, measureId, surveyInstanceId, schoolScore) ;
				System.out.println();
			}
			else {
				System.out.println("No computation Score (Student): " + numberOfParticipant);
				System.out.println();
			}






		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				//database.close("Close Connection for Input Query", conn);
				//conn = null;
			}
		}

	}

}
