package algorithms.ACutoff_school;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureInfo;
import utilities.SurveyInfoFinder;
import utilities.Utils;

public class Alg_ACutoffDouble_Default {


	private DataStore_student dataStoreStudent;
	private DataStore_school dataStoreSchool;
	private Connection conn;
	public boolean debugPrint = true;




	public Alg_ACutoffDouble_Default( Connection conn ,  DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool) {
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
	}


	public void compute( String measureId , String surveyInstanceId , Double cutoff0 , Double cutoff1 ){

		MeasureInfo minfo = MeasureInfo.getInstance();
				
		List<String> submeasures = minfo.getSubMeasures( measureId );
		List<Double> cutoffs = new ArrayList<Double>();
		cutoffs.add( cutoff0  ) ;
		cutoffs.add( cutoff1  ) ;

		algo_ACutOffDouble_measuresP(  submeasures.get(2), surveyInstanceId, cutoffs , submeasures);

		//set measure as computed
		//dataStoreSchool.setMeasureComputed( newMeasureId );
		dataStoreSchool.setMeasureComputed( measureId );
		dataStoreSchool.setMeasureComputed( submeasures.get(0) );
		dataStoreSchool.setMeasureComputed( submeasures.get(1) );
		dataStoreSchool.setMeasureComputed( submeasures.get(2) );
		
	}



	public void algo_ACutOffDouble_measuresP(String measureIdMean, String surveyInstanceId, List<Double> list, List<String> submeasures) {

		double cutoffLow = list.get(0);
		double cutoffHigh= list.get(1);

		String measureIdLow = submeasures.get(0);
		String measureIdHigh = submeasures.get(1);
		//String measureIdMean = submeasures.get(2);


		/*
		 * diagram

			--non-Active-------|-----------------------Active----------------------------
			                   |
		    -------------------|--------pop passed, pop active---------------------------

		                  cutoffLow                    cutoffHigh
		    -------Low---------|--------Mid zone---------|--------High zone--------------
		                       |                         |
		                       |                         |  
		    -------------------|----pop passed mid-------|---- pop passed High-----------



		    When saving data, saves results under:

		    -------------------|---------------measureId----------------------------------
		    -------------------|--- measureId Low---------|   measureId High--------------

		 */



		if(debugPrint ){
			System.out.println( "School Level (Cutoff+)--> MeasureId: "+measureIdMean+" SurveyInstanceId: "+surveyInstanceId);
			System.out.println( "cutoff low: " + cutoffLow + "   cutoffHigh: " + cutoffHigh);
			
			System.out.println("measureIdMean " + measureIdMean);
			System.out.println("measureIdLow "  + measureIdLow);
			System.out.println("measureIdHigh " + measureIdHigh);
			
			System.out.println("");
			System.out.println("");
		}

		//list of al student ids
		List<String> students = dataStoreStudent.getAllStudents();


		for( String student : students){

			//821626377  {1217=[0, 0, 1], 1216=[0, 0, 1], 1215=[7.5, 2, 1], 1214=[0.0, 2, 1]}
			HashMap<String, List<String>> studentResults = dataStoreStudent.getStudentResults(student);

			//821626377  {1=3, 2=10, 42=5, 43=3}
			HashMap<String, String> studentCharacteristics  = dataStoreStudent.getStudentCharacteristics(student);

			//results for this combination of characteristics
			//   measureId , [active pop,passed pop ,noQ pop ,notEnough pop, active scor,passed score]
			HashMap<String, List<String>>  schoolResults = dataStoreSchool.getCharacteristicResults( studentCharacteristics ); 


			int popSkipped=0 ;
			int popNotEnoughQuestionsAnswered=0 ;

			//total population active
			int populationActive =0;

			int populationPassed =0;
			int populationPassedMid =0;
			int populationPassedHigh =0;

			double sumScoreActive = 0;
			double sumScoreActiveMid = 0;
			double sumScoreActiveHigh = 0;

			double sumScorePassed =0;
			double sumScorePassedMid =0;
			double sumScorePassedHigh =0;	


			if( schoolResults != null){
				
				//[active , passed , skipped , notenough answered , scoreActive , scorePassed
				//[0, 0, 1, 0, 0.0, 0.0] 
				List<String> schoolMeasureResult  = schoolResults.get(  measureIdMean  );
				List<String> schoolMeasureResultMid  = schoolResults.get(  measureIdLow );
				List<String> schoolMeasureResultHigh  = schoolResults.get( measureIdHigh  );


				//Total  Mid+High zones
				if( schoolMeasureResult != null){
					populationActive =	 			Integer.parseInt( schoolMeasureResult.get( 0 ) );
					populationPassed = 				Integer.parseInt( schoolMeasureResult.get( 1 ) );
					popSkipped = 					Integer.parseInt( schoolMeasureResult.get( 2 ) );
					popNotEnoughQuestionsAnswered=  Integer.parseInt( schoolMeasureResult.get( 3 ) );
					sumScoreActive=  				Double.parseDouble( schoolMeasureResult.get( 4 ) );
					sumScorePassed=  				Double.parseDouble( schoolMeasureResult.get( 5 ) );
				}
				
				//Mid zone
				if( schoolMeasureResultMid != null){
					
					populationPassedMid = 			Integer.parseInt( schoolMeasureResultMid.get( 1 ) );
					sumScoreActiveMid=  			Double.parseDouble( schoolMeasureResultMid.get( 4 ) );
					sumScorePassedMid=  			Double.parseDouble( schoolMeasureResultMid.get( 5 ) );
					
				}

				//High zone
				if( schoolMeasureResultHigh != null){
					
					populationPassedHigh = 			Integer.parseInt( schoolMeasureResultHigh.get( 1 ) );
					sumScoreActive=  				Double.parseDouble( schoolMeasureResultHigh.get( 4 ) );
					sumScorePassedHigh=  			Double.parseDouble( schoolMeasureResultHigh.get( 5 ) );
					
				}

			}


			if( studentResults!=null && studentResults.containsKey(measureIdMean)  ){				

				//list of results for measure which beloongs to a student
				// 1217=[9.0625, 8, 0]
				List<String> studentList = studentResults.get(measureIdMean);

				//data for this student ----
				Double score = 		Double.parseDouble( studentList.get(0) );
				int questionsAnswered = Integer.parseInt( studentList.get(1) );
				String supressed = studentList.get(2); 
				//------------------

				//System.out.println(""+score);
				

				//------------
				//ALG
				if( supressed.equals("0")){
					// score was not supressed for this user

					// student is under cutoff, but still active because they were not suppressed
					sumScoreActive+=Utils.round(score, 2);
					populationActive++;

					if( Utils.round(score, 2) >= cutoffLow ){

						populationPassed++;
						sumScorePassed+=Utils.round(score, 2);

						if( Utils.round(score, 2) >= cutoffHigh ){
							sumScoreActiveHigh+=Utils.round(score, 2);
							sumScorePassedHigh+=Utils.round(score, 2);
							populationPassedHigh++;
						}
						else{
							populationPassedMid++;

							sumScoreActiveMid+=Utils.round(score, 2);
							sumScorePassedMid+=Utils.round(score, 2);

						}
					}

				}
				else{
					//suppressed flag=1
					// user is supressed for not enough/any questions

					if( questionsAnswered >=1 ){
						// did not answer enough questions
						popNotEnoughQuestionsAnswered++;
					}
					else{
						// did not answer any questions
						popSkipped++;
					}
				}


			}
			else{
				//System.out.println( "studentResults==null or !studentResults.containsKey(measureId) ");

				if( debugPrint){

					if( studentResults == null){
						System.out.println( "studentResults == null" );
					}
					else
						if( !studentResults.containsKey(measureIdMean)){
							System.out.println( "!studentResults.containsKey(measureId) "  + measureIdMean);
						}
				}


			}



			//------------------------------------------------
			//update the data for combination-measure

			//Cutoff1

			List<String> resultTotal = new ArrayList<String>();

			resultTotal.add(Integer.toString(populationActive));
			resultTotal.add(Integer.toString(populationPassed));
			resultTotal.add(Integer.toString(popSkipped));
			resultTotal.add(Integer.toString(popNotEnoughQuestionsAnswered));

			resultTotal.add(Double.toString(sumScoreActive));
			resultTotal.add(Double.toString(sumScorePassed));

			//overwrite whatever was already in the datastore					
			dataStoreSchool.putCharacteristicResults(studentCharacteristics, measureIdMean , resultTotal);
			//------------------------------------------


			//------------------------------------------------
			//Cutoff2

			List<String> resultMid = new ArrayList<String>();

			resultMid.add(Integer.toString(populationActive));
			resultMid.add(Integer.toString(populationPassedMid));
			resultMid.add(Integer.toString(popSkipped));
			resultMid.add(Integer.toString(popNotEnoughQuestionsAnswered));

			resultMid.add(Double.toString(sumScoreActiveMid));
			resultMid.add(Double.toString(sumScorePassedMid));
				
			dataStoreSchool.putCharacteristicResults(studentCharacteristics, measureIdLow, resultMid);
			//------------------------------------------


			//------------------------------------------------
			//Cutoff3

			List<String> resultHigh= new ArrayList<String>();

			resultHigh.add(Integer.toString(populationActive));
			resultHigh.add(Integer.toString(populationPassedHigh));
			resultHigh.add(Integer.toString(popSkipped));
			resultHigh.add(Integer.toString(popNotEnoughQuestionsAnswered));

			resultHigh.add(Double.toString(sumScoreActiveHigh));
			resultHigh.add(Double.toString(sumScorePassedHigh));
				
			dataStoreSchool.putCharacteristicResults(studentCharacteristics, measureIdHigh , resultHigh);
			//------------------------------------------


			/*
			System.out.println("");
			System.out.println( resultTotal.toString() );
			System.out.println( resultMid.toString() );
			System.out.println( resultHigh.toString() );

			 */
		}


	}

}



