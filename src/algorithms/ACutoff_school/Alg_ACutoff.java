package algorithms.ACutoff_school;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureInfo;
import utilities.Utils;


//ACutOff = 1101,1102,1103, 1104, 1105,1106,1,1215,1216,1217,1214 	

public class Alg_ACutoff {

	private DataStore_school dataStoreSchool;
	private DataStore_student dataStoreStudent;
	private boolean debugPrint = true;

	public Alg_ACutoff(Connection conn , DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool) {
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent = dataStoreStudent;
	}


	
	public void compute( String measureId , String surveyInstanceId ){
		computeAlg( measureId , surveyInstanceId  );
	}

	

	public void computeAlg( String measureId , String surveyInstanceId  ){

		if(debugPrint){
			System.out.println(this.getClass().getName() + "  "  + measureId + "   SurveyInstanceId: "+ surveyInstanceId);
		}

		
		float cutoff = -1;
		String cutof = MeasureInfo.getInstance().getCutoff(measureId);
		cutoff = Float.parseFloat(cutof);


		//debug arrays
		ArrayList< Double > scoresAboveCutoff  = new ArrayList<Double>();
		ArrayList< Double > scoresBelowCutoff  = new ArrayList<Double>();
		
		
		if( cutoff>-1 ){
			//do not try to compute if dont have a cutoff value

			//list of al student ids
			List<String> students = dataStoreStudent.getAllStudents();


			for( String student : students){


				//	821626377  {1217=[0, 0, 1], 1216=[0, 0, 1], 1215=[7.5, 2, 1], 1214=[0.0, 2, 1]}
				HashMap<String, List<String>> studentResults = dataStoreStudent.getStudentResults(student);

				//821626377  {1=3, 2=10, 42=5, 43=3}
				HashMap<String, String> studentCharacteristics  = dataStoreStudent.getStudentCharacteristics(student);

				//results for this combination of characteristics
				//   measureId , [active pop,passed pop ,noQ pop ,notEnough pop, active scor,passed score]
				HashMap<String, List<String>>  schoolResults = dataStoreSchool.getCharacteristicResults( studentCharacteristics ); 

				int populationActive =0;
				int populationPassed =0;
				int popSkipped=0 ;
				int popNotEnoughQuestionsAnswered=0 ;

				double sumScoreActive = 0;
				double sumScorePassed =0;	


				if( schoolResults != null){

					//[0, 0, 1, 0, 0.0, 0.0] 
					List<String> schoolMeasureResult  = schoolResults.get(measureId);

					//there is already data for this combo-measure in the datastore at school lvl, find it to add to it
					if( schoolMeasureResult != null){

						populationActive = Integer.parseInt( schoolMeasureResult.get( 0 ) );
						populationPassed = Integer.parseInt( schoolMeasureResult.get( 1 ) );
						popSkipped = 		Integer.parseInt( schoolMeasureResult.get( 2 ) );
						popNotEnoughQuestionsAnswered= Integer.parseInt( schoolMeasureResult.get( 3 ) );
						sumScoreActive=  Double.parseDouble( schoolMeasureResult.get( 4 ) );
						sumScorePassed=  Double.parseDouble( schoolMeasureResult.get( 5 ) );

					}				

				}

				
				if( studentResults!=null && studentResults.containsKey(measureId)  ){				

					//list of results for measure which beloongs to a student
					// 1217=[9.0625, 8, 0]
					List<String> studentList = studentResults.get(measureId);

					//data for this student ----
					Double score = 		Double.parseDouble( studentList.get(0) );
					int questionsAnswered = Integer.parseInt( studentList.get(1) );
					String supressed = studentList.get(2); 
					//------------------


					
					//------------
					//ALG
					if( supressed.equals("0")){
						// score was not supressed for this user

						if( Utils.round(score, 2) >= cutoff ){
							// student is above cutoff

							// ie  student is active and passed, (passed is subset of active	
							scoresAboveCutoff.add(Utils.round(score, 2));	//debug
							sumScoreActive+=Utils.round(score, 2);
							sumScorePassed+=Utils.round(score, 2);
							populationActive++;			
							populationPassed++;

						}
						else{
							// student is under cutoff, but still active because they were not suppressed

							//debug
							scoresBelowCutoff.add(Utils.round(score, 2));
							sumScoreActive+=Utils.round(score, 2);
							populationActive++;
						}

					}
					else{
						//suppressed flag=1
						// user is supressed for not enough/any questions

						if( questionsAnswered >=1 ){
							// did not answer enough questions
							popNotEnoughQuestionsAnswered++;
						}
						else{
							// did not answer any questions
							popSkipped++;
						}
					}


				}
				
				
				//------------------------------------------------
				//update the data for combination-measure

				List<String> result = new ArrayList<String>();

				result.add(Integer.toString(populationActive));
				result.add(Integer.toString(populationPassed));
				result.add(Integer.toString(popSkipped));
				result.add(Integer.toString(popNotEnoughQuestionsAnswered));

				result.add(Double.toString(sumScoreActive));
				result.add(Double.toString(sumScorePassed));

				//overwrite whatever was already in the datastore					
				dataStoreSchool.putCharacteristicResults(studentCharacteristics, measureId, result);
				//------------------------------------------


				if( debugPrint ){
					//System.out.println(studentCharacteristics.toString() + "  " + measureId + "-" + result.toString());
				}


			}



		}





	}
	
	







	//--------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------
	//----------------------------------------------------------------
	// Original version, only for reference


	/*
	 * 1  numerator:    100 for each participant score which is over cutoff,  0 for those under cutoff
	 * 2  denomonator:  number of participants, under or over cutoff
	 * 3  take avg
	 * 
	 */
	
	/*
	String AScoreCutOff = " Select "
			+ "SUM(  if(p.score>= m.cutoff, 100, 0)  ), "
			+ "count(surveyParticipantId) , "
			+ "m.cutoff "
			+ "from ParticipantMeasureScores as p "
			+ "JOIN "
			+ "Measure as m on m.id = p.measureId "
			+ "where measureId = ? "
			+ "and SurveyInstanceId = ? " ;
			
			
	public void algo_ACutOff_measuresP(int measureId, int surveyInstanceId) {
		PreparedStatement stmt = null;
		ResultSet resScoreComputation = null;

		try {

			System.out.println( "School Level--> " + "\n algo_ACutOff_measuresP: " + measureId + " " + "\n SurveyInstanceId: "+ surveyInstanceId);

			String scoreComputationQuery = AScoreCutOff;

			stmt = conn.prepareStatement(scoreComputationQuery);

			stmt.setInt(1, measureId);
			stmt.setInt(2, surveyInstanceId);

			resScoreComputation = stmt.executeQuery(); 

			double schoolScore= -1;
			int numberOfParticipant= 0;

			float cutoff = 0.0f;

			while (resScoreComputation.next()) {

				schoolScore = resScoreComputation.getDouble(1);
				numberOfParticipant = resScoreComputation.getInt(2);

				cutoff = resScoreComputation.getFloat(3);

				System.out.println("schoolScore (in resultSet): " + schoolScore  + " ,  numberOfParticipant: "+ Integer.toString(numberOfParticipant)); 

			}

			if ( schoolScore != -1 && numberOfParticipant!=0  ){

				schoolScore = schoolScore/numberOfParticipant;

				System.out.println("schoolScore: " + schoolScore 
						+ " \n  numberOfParticipant: "+ numberOfParticipant 
						+  "\n  cutoff: " + cutoff); 
			}
			else {
				System.out.println("No computation Score (Student): " + numberOfParticipant);
				System.out.println();
			}


		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (resScoreComputation != null) {
				try {
					resScoreComputation.close();
					resScoreComputation = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}	
		}
	}
*/

}
