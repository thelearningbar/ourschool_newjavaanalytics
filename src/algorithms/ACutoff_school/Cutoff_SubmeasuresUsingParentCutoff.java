package algorithms.ACutoff_school;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureInfo;
import utilities.Utils;

public class Cutoff_SubmeasuresUsingParentCutoff {


	private DataStore_school dataStoreSchool;
	private DataStore_student dataStoreStudent;
	private boolean debugPrint = true;
	
	public Cutoff_SubmeasuresUsingParentCutoff(Connection conn , DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool) {
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent = dataStoreStudent;
	}
	

	
	public void compute( String measureId , String surveyInstanceId ){
		
		MeasureInfo minfo =  MeasureInfo.getInstance();
		
		//get cutoff of parent measure
		String cutof = minfo.getCutoff(measureId);
		
		//use the cutoff on each submeasure
		List<String> submeasures = minfo.getSubMeasures(measureId);
		for( String submeasure : submeasures){
			computeAlg( submeasure , surveyInstanceId , Float.parseFloat(cutof) );
			dataStoreSchool.setMeasureComputed(submeasure);
		}
		
		dataStoreSchool.setMeasureComputed(measureId);
		
	}
	
	
	
	
	

	public void computeAlg( String measureId , String surveyInstanceId , float cutoff  ){

		if(debugPrint){
			System.out.println(this.getClass().getName() + "  "  + measureId + "   SurveyInstanceId: "+ surveyInstanceId);
		}

		String cutof = MeasureInfo.getInstance().getCutoff(measureId);
		cutoff = Float.parseFloat(cutof);


		//debug arrays
		ArrayList< Double > scoresAboveCutoff  = new ArrayList<Double>();
		ArrayList< Double > scoresBelowCutoff  = new ArrayList<Double>();
		
		
		if( cutoff>-1 ){
			//do not try to compute if dont have a cutoff value

			//list of al student ids
			List<String> students = dataStoreStudent.getAllStudents();


			for( String student : students){


				//	821626377  {1217=[0, 0, 1], 1216=[0, 0, 1], 1215=[7.5, 2, 1], 1214=[0.0, 2, 1]}
				HashMap<String, List<String>> studentResults = dataStoreStudent.getStudentResults(student);

				//821626377  {1=3, 2=10, 42=5, 43=3}
				HashMap<String, String> studentCharacteristics  = dataStoreStudent.getStudentCharacteristics(student);

				//results for this combination of characteristics
				//   measureId , [active pop,passed pop ,noQ pop ,notEnough pop, active scor,passed score]
				HashMap<String, List<String>>  schoolResults = dataStoreSchool.getCharacteristicResults( studentCharacteristics ); 

				int populationActive =0;
				int populationPassed =0;
				int popSkipped=0 ;
				int popNotEnoughQuestionsAnswered=0 ;

				double sumScoreActive = 0;
				double sumScorePassed =0;	


				if( schoolResults != null){

					//[0, 0, 1, 0, 0.0, 0.0] 
					List<String> schoolMeasureResult  = schoolResults.get(measureId);

					//there is already data for this combo-measure in the datastore at school lvl, find it to add to it
					if( schoolMeasureResult != null){

						populationActive = Integer.parseInt( schoolMeasureResult.get( 0 ) );
						populationPassed = Integer.parseInt( schoolMeasureResult.get( 1 ) );
						popSkipped = 		Integer.parseInt( schoolMeasureResult.get( 2 ) );
						popNotEnoughQuestionsAnswered= Integer.parseInt( schoolMeasureResult.get( 3 ) );
						sumScoreActive=  Double.parseDouble( schoolMeasureResult.get( 4 ) );
						sumScorePassed=  Double.parseDouble( schoolMeasureResult.get( 5 ) );

					}				

				}

				
				if( studentResults!=null && studentResults.containsKey(measureId)  ){				

					//list of results for measure which beloongs to a student
					// 1217=[9.0625, 8, 0]
					List<String> studentList = studentResults.get(measureId);

					//data for this student ----
					Double score = 		Double.parseDouble( studentList.get(0) );
					int questionsAnswered = Integer.parseInt( studentList.get(1) );
					String supressed = studentList.get(2); 
					//------------------


					
					//------------
					//ALG
					if( supressed.equals("0")){
						// score was not supressed for this user

						if( Utils.round(score, 2) >= cutoff ){
							// student is above cutoff

							// ie  student is active and passed, (passed is subset of active	
							scoresAboveCutoff.add(Utils.round(score, 2));	//debug
							sumScoreActive+=Utils.round(score, 2);
							sumScorePassed+=Utils.round(score, 2);
							populationActive++;			
							populationPassed++;

						}
						else{
							// student is under cutoff, but still active because they were not suppressed

							//debug
							scoresBelowCutoff.add(Utils.round(score, 2));
							sumScoreActive+=Utils.round(score, 2);
							populationActive++;
						}

					}
					else{
						//suppressed flag=1
						// user is supressed for not enough/any questions

						if( questionsAnswered >=1 ){
							// did not answer enough questions
							popNotEnoughQuestionsAnswered++;
						}
						else{
							// did not answer any questions
							popSkipped++;
						}
					}


				}
				
				
				//------------------------------------------------
				//update the data for combination-measure

				List<String> result = new ArrayList<String>();

				result.add(Integer.toString(populationActive));
				result.add(Integer.toString(populationPassed));
				result.add(Integer.toString(popSkipped));
				result.add(Integer.toString(popNotEnoughQuestionsAnswered));

				result.add(Double.toString(sumScoreActive));
				result.add(Double.toString(sumScorePassed));

				//overwrite whatever was already in the datastore					
				dataStoreSchool.putCharacteristicResults(studentCharacteristics, measureId, result);
				//------------------------------------------


				if( debugPrint ){
					//System.out.println(studentCharacteristics.toString() + "  " + measureId + "-" + result.toString());
				}


			}



		}





	}

}
