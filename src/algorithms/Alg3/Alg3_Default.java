package algorithms.Alg3;

public class Alg3_Default {

	public static String Algorithm3 = "Select sid, score from( " +
			"Select " +
			"a.surveyParticipantId as sid,     " +
			"(lad.value-1) as score, if (COUNT( a.questionId ) >= m.minimumAnswersRequired, 1, 0) as flag " +
			"From Answer4Rpt As a " +
			"Inner Join LikertAnswerDetail As lad On lad.id = a.answerDetailId " +
			"Inner Join MeasureQuestion As mq On mq.questionId = a.questionId " +
			"And mq.measureId = ? " +
			"Inner Join Measure As m On m.id = mq.measureId " +
			"Where a.surveyInstanceId= ? " +
			"Group By a.surveyParticipantId ) as mamun where flag =1 ";
	
	public Alg3_Default() {
		
	}
	
	

}
