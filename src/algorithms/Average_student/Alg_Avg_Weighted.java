package algorithms.Average_student;
import java.util.ArrayList;
import java.util.List;

import utilities.DataStore_student;
import utilities.MeasureInfo;

public class Alg_Avg_Weighted {

	

	DataStore_student dataStoreStudent;
	
	
	/*
	 * weighted mean: 
	 * 
	 * (s_1 * q_1 + s_2 * q_2)/ q_1 +  q_2
	 * 
	 * 
	 * 
	 */
	
	
	
	public Alg_Avg_Weighted( DataStore_student dataStoreStudent ) {

		this.dataStoreStudent=dataStoreStudent;
	}

	

	public void ComputeAvg( String measureId , List<String> childMeasures ) {
		
		
		
		
		List<String> users = dataStoreStudent.getAllStudents();
				
		for(String user : users){
			
			Double numeratr=0.0;
			Double denomonator=0.0;
			
			int questionsAnswered = 0;
			
			for( String measure : childMeasures ){
				
				String score =  dataStoreStudent.getStudentResultsMeasureValue( user, measure, 0 );
				
				String countquestion =  dataStoreStudent.getStudentResultsMeasureValue( user, measure, 1 );
				
				if( score != null && countquestion != null ){
					
					denomonator += Double.parseDouble( countquestion );
					numeratr    += Double.parseDouble( countquestion ) * Double.parseDouble( score );
					questionsAnswered +=  Integer.parseInt( countquestion );
					
				}
				
			}
			
			//check denomonator >0 to avoid div by zero
			if( denomonator > 0){
				
				int minimumAnsersReq = Integer.parseInt( MeasureInfo.getInstance().getMinimumAnswer(measureId) );
				
				//complete PMS result for student,  [ score , questionsAnswered , supressedfalg ]
				List<String> result = new ArrayList<String>();
				
				result.add( Double.toString( numeratr/denomonator) ); 
				result.add( Integer.toString( questionsAnswered ) );
				

				if(questionsAnswered>= minimumAnsersReq){
					result.add( Integer.toString( 0 ) );
				}
				else{
					//supreessed flag=1 for user,  did not answer enough questions
					result.add( Integer.toString( 1 ) );
				}
				
				
				
				
				dataStoreStudent.addStudentResult(user, measureId, result);
				
			}
			else{
				
				
				List<String> result = new ArrayList<String>();
				result.add( Double.toString( 0 ) );
				result.add( Integer.toString( questionsAnswered ) );
				result.add( Integer.toString( 1 ) );
				dataStoreStudent.addStudentResult(user, measureId, result);
			}
			
			
			
			
		}
		
		
		
	}


	
	
	
	
	
	
}






