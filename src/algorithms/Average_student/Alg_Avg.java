package algorithms.Average_student;
import java.util.ArrayList;
import java.util.List;
import utilities.DataStore_student;

public class Alg_Avg {


	DataStore_student dataStoreStudent;
	
	public Alg_Avg( DataStore_student dataStoreStudent ) {
		this.dataStoreStudent=dataStoreStudent;
	}
	
	
	
	
	public void ComputeAvg( String measureId , int minimumAnsReq, boolean useMinimumAnsReq ,  List<String> childMeasures ){

		List<String> users =dataStoreStudent.getAllStudents();
				
		for(String user : users){
			
			
			
			Double top=0.0;  //ie  X in  X/Y
			Double count=0.0;  //ie Y in X/Y    //number of questions answered, which are to be counted toward te average
			
			int totalAnswers = 0; //keep a count of total questions answered, regardless of suppressed
			
			for( String measure : childMeasures ){
				
				//int minimumAnswersRequired = Integer.parseInt(  MeasureInfo.getInstance().getMinimumAnswer(measure) );
								
				if( dataStoreStudent.studentHasResultsForMeasure(user, measure) ){
					
					String score   =dataStoreStudent.getStudentResultsMeasureValue( user, measure, 0);
					String answers = dataStoreStudent.getStudentResultsMeasureValue( user, measure, 1);
					String supressed  = dataStoreStudent.getStudentResultsMeasureValue( user, measure, 2);
										
					if( score != null ){
						
						if( supressed.equals("0")){
							//the measure was not supressed for the user
							
							Double d = Double.parseDouble( score );
							count+=1.0; 
							totalAnswers+=Integer.parseInt(answers);
							top+=d;
						}
						else{
							totalAnswers+=Integer.parseInt(answers);
						}
						
					}
					
				}
				
				if( count > 0){
					//avoid divide by zero
					
					Double avg = top/count;
					List<String> result = new ArrayList<String>();
					result.add( Double.toString( avg) );
					result.add( Integer.toString( totalAnswers ) );
					result.add( Integer.toString( 0 ) );
					dataStoreStudent.addStudentResult(user, measureId, result);
					
				}
				else{
					
					List<String> result = new ArrayList<String>();
					result.add( Double.toString( 0 ) );
					result.add( Integer.toString( totalAnswers ) );
					result.add( Integer.toString( 1 ) );
					dataStoreStudent.addStudentResult(user, measureId, result);
				
				}
				
				
			}
		}
	}

	
	//Without using supressed flag.., using minimum answrs required
	/*
	Double d = Double.parseDouble( score );
	if( answers != null){
		Double ans = Double.parseDouble(answers);
		totalAnswers+=ans.intValue();
		if( useMinimumAnsReq ){
			if( ans>=minAns ){
				count+=1.0;
				top+=d;
			}
		}
	}
	else{
		count+=1.0;
		top+=d;
	}
	*/
	
}
