package visitables.student;
import java.util.HashMap;
import java.util.List;

import algorithms.Average_student.Alg_Avg;
import utilities.DataStore_student;
import utilities.MeasureInfo;
import utilities.MeasureRegister;
import utilities.Utils;
import visitables.IVisitable;
import visitorPickers.VisitorPickerFactory;


public class Visitable_Avg_student implements IVisitable{

	VisitorPickerFactory visitorPicker;
	

	DataStore_student dataStoreStudent;
	
	public boolean debugPrint = false;
	
	public Visitable_Avg_student( VisitorPickerFactory visitorPickerFactory	, DataStore_student dataStoreStudent) {
		
		this.visitorPicker = visitorPickerFactory;
		this.dataStoreStudent=dataStoreStudent;
	}
	
	
	@Override
	public void handleRequest(String measureId, HashMap<String, String> parameters) {
		if( debugPrint)
			System.out.println(this.getClass().getName() + " " + measureId);
		
		
		
		List<String> childmeasures = MeasureRegister.getInstance().getChildMeasuresStudentLvl(measureId);
		
		
		//only compute this measure if it has not already been computed
		if(  !dataStoreStudent.isComputed(measureId) ){
			
			
			
			if( debugPrint)
				Utils.pr(measureId + " not already computed\n");
			
									
			
			
			
			
			Alg_Avg avg = new Alg_Avg(dataStoreStudent);
			
			
			//first compute any prerequisites
			if( childmeasures != null){
				for( String child : childmeasures ){
					
					//compute child measure if not already computed
					if( !dataStoreStudent.isComputed(child) ){
						
						if( debugPrint)
							Utils.pr("compute "+ child + " for "+ measureId);
						
						
						
						
						//FIND the uncomputed child measure
						visitorPicker.getVisitorAlgo( MeasureRegister.getInstance().getAlgStudentLvl(child)).chooseVisitor(child,  parameters);
						
						//check it off
						dataStoreStudent.setStudentLevelMeasureComputed(child);
						
						
						
						
						
						if( debugPrint)
							Utils.pr("");
					}
					else{
						if( debugPrint)
							Utils.pr(">> child " +  child + " is already computed");
					}
				}
			}
			
			MeasureInfo measureInfo = MeasureInfo.getInstance();
			measureInfo.saveMeasureInfo(measureId);
			
			//get minimum answers required for measure
			int minimumAns = Integer.parseInt( measureInfo.getMinimumAnswer(measureId) );
			
			//run algorithm
			avg.ComputeAvg( measureId , minimumAns , true  , childmeasures );
			
			//mark measure as already computed at student level
			dataStoreStudent.setStudentLevelMeasureComputed(measureId);
			
			
			
			
			
			
			
			
			if( debugPrint)
				System.out.println("done " + measureId);
			
		}
		else{
			if( debugPrint)
				Utils.pr("> "+measureId + " is already computed\n");
		}
	}



}