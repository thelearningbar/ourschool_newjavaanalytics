package visitables.student;

import java.sql.Connection;
import java.util.HashMap;

import algorithms.Bullying_student.SubmeasuresLADvalueToScore_student;
import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;

public class Visitable_SubmeasuresLADvalueToScore implements IVisitable {

	private DataStore_student dataStoreStudent;
	private DataStore_school dataStoreSchool;
	private Connection conn;
	
	public boolean debugPrint =false;
	
	
	public Visitable_SubmeasuresLADvalueToScore(Connection conn ,DataStore_student dataStoreStudent , DataStore_school dataStoreSchool){
		this.dataStoreStudent=dataStoreStudent;
		this.dataStoreSchool= dataStoreSchool;
		this.conn=conn;
	}

	@Override
	public void handleRequest(String measureId, HashMap<String, String> surveyParameters) {

		
		String surveyInstanceId = surveyParameters.get("surveyInstanceId");
		SubmeasuresLADvalueToScore_student alg = new SubmeasuresLADvalueToScore_student(conn, dataStoreStudent, dataStoreSchool);		
		alg.compute(measureId, surveyInstanceId);
		
	}

	
	
}
