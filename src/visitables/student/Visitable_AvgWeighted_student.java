package visitables.student;
import java.util.HashMap;
import java.util.List;

import algorithms.Average_student.Alg_Avg_Weighted;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureRegister;
import utilities.Utils;
import visitables.IVisitable;
import visitorPickers.VisitorPickerFactory;


public class Visitable_AvgWeighted_student implements IVisitable{
	
	private VisitorPickerFactory visitorPicker;
	private DataStore_student dataStoreStudent;
	public boolean debugPrint =false;
	
	
	public Visitable_AvgWeighted_student( VisitorPickerFactory visitorPickerFactory , DataStore_student dataStoreStudent, 	DataStore_school dataStoreSchool) {
		
		this.visitorPicker = visitorPickerFactory;
		
		this.dataStoreStudent=dataStoreStudent;
	}
	
	
	@Override
	public void handleRequest(String measureId, HashMap<String, String> parameters) {
		if( debugPrint)
			System.out.println(this.getClass().getName() + " " + measureId);
		
		
		
		
		List<String> children = MeasureRegister.getInstance().getChildMeasuresStudentLvl(measureId);
		
		if(  !dataStoreStudent.isComputed(measureId) ){
			
			if( debugPrint)
				Utils.pr(measureId + " not already computed\n");
					
			
			Alg_Avg_Weighted avg = new Alg_Avg_Weighted(dataStoreStudent);
			
			if( children != null){
				for( String child : children ){
										
					if( !dataStoreStudent.isComputed(child) ){
						
						if( debugPrint)
							Utils.pr("compute "+ child + " for "+ measureId);
						
												
						visitorPicker.getVisitorAlgo( MeasureRegister.getInstance().getAlgStudentLvl(child)).chooseVisitor(child,  parameters);
						dataStoreStudent.setStudentLevelMeasureComputed(child);
								
						
						
						if( debugPrint)
							Utils.pr("");
					}
					else{
						if( debugPrint)
							Utils.pr(">> child " +  child + " is already computed");
					}
				}
			}
			
			avg.ComputeAvg( measureId , children );
			dataStoreStudent.setStudentLevelMeasureComputed(measureId);
		}
		else{
			if( debugPrint)
				Utils.pr("> "+measureId + " is already computed\n");
		}
	}




	
}