package visitables.student;
import java.sql.Connection;
import java.util.HashMap;

import algorithms.Alg2_student.Alg2Default;
import algorithms.Alg2_student.IAlg2;
import utilities.DataStore_student;
import visitables.IVisitable;

/**
 * Deals with variants of Alg_1_11m
 * 
 * 
 * 
 * QMotiveConstant=3.75
QMotiveMax=10


#Canada=5, Australia=7
QSkillCutOff=5
ChallengeCutoff=5
 */
public class Visitable_Alg2_student implements IVisitable{
	
	//DEFAULT VALUES
	private	double normalizeConst;
	private	int maximumMotivationScore ;
	private int scale;
	
	private Connection conn;
	private DataStore_student dataStoreStudent;

	public Visitable_Alg2_student( Connection conn ,DataStore_student dataStoreStudent ){
		this.normalizeConst = 3.75;
		this.maximumMotivationScore = 10;
		this.scale = 10;
		
		this.conn=conn;
		this.dataStoreStudent=dataStoreStudent;
	}

	@Override
	public void handleRequest(String measureId,  HashMap<String, String> parameters) {
		
		//System.out.println(this.getClass().getName() + " " + measureId);
		
		IAlg2 alg;
		
		
		String surveyInstanceId = parameters.get("surveyInstanceId");
		
		int measure = Integer.parseInt(measureId);
		int surveyInstance= Integer.parseInt(  surveyInstanceId );
		
		
		/*
		System.out.println("flags are not default, load parameters from database");
		//override default values with database values ---
		normalizeConst = Utils.doubleFromMap("normalizeConst", parameters);
		maximumMotivationScore =  Utils.intFromMap("maximumMotivationScore" ,parameters ) ;		
		*/
		
		//System.out.println("compute");
		
		
		alg = new Alg2Default( conn, dataStoreStudent );
		
		
		//alg = new Alg2Custom( DBconnection.getConnection() );
		/*
		computeMeasureScoreAlgorithmId_1_11_m(int scaleFactor, double normalizeConst, 
				int maximumMotivationScore, int measureId, int surveyInstanceId ) {
		*/
		alg.computeMeasureScoreAlgorithmId_1_11_m(scale, normalizeConst, maximumMotivationScore, measure, surveyInstance , null);
		
		//MeasureRegister.setMeasureComputed(measureId);
		dataStoreStudent.setStudentLevelMeasureComputed(measureId);
		
	}


	
}