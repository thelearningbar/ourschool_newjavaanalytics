package visitables.student.bullying;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import algorithms.Bullying_student.BullyPopulation_student;
import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitorPickers.VisitorPickerFactory;

public class Visitable_BullyPopulation_student implements IVisitable {
	

	private DataStore_student dataStoreStudent;
	private Connection conn;
	

	private boolean debugPrint = false;

	
	
	public Visitable_BullyPopulation_student(   VisitorPickerFactory visitorPickerFactory ,  
			Connection conn , DataStore_student dataStoreStudent , DataStore_school dataStoreSchool) {
		
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
		
	}

	@Override
	public void handleRequest(String measureId, HashMap<String, String> surveyParameters  ) {
		if( debugPrint)
			System.out.println(this.getClass().getName());

		String surveyInstanceId = surveyParameters.get("surveyInstanceId");
		//String schooltype = surveyParameters.get("schooltype");
		
		String basicOrExtended = surveyParameters.get("Bully-Category");
		
		
		//maps a question to which measure to store its population under
		//some measures ask  'choose a,b,or c' 
		//population of participants who answered a are stored in measure1
		//population of participants who answered b are stored in measure2
		//population of participants who answered c are stored in measure3
		Map<String,String> questionNewMeasure= new HashMap<String,String>();
				
		
		
		//CUSTOMIZATION for elementry/2ndary schools	
		//same questions are mapped to different support measures

		if( basicOrExtended!=null){
			if( basicOrExtended.equals("4") ){
				//4  Basic

				//Primary school
				//Bully-Category Basic

				//ex measure 1120

				questionNewMeasure.put("443", "1695");
				questionNewMeasure.put("444", "1698");
				questionNewMeasure.put("445", "1701");
				questionNewMeasure.put("446", "1704");

				questionNewMeasure.put("1290", "1695");
				questionNewMeasure.put("1291", "1698");
				questionNewMeasure.put("1292", "1701");
				questionNewMeasure.put("1293", "1704");
			}
			else{
				// 1 Exteneded

				//Secondary school
				//Bully-Category Extended

				//ex measure 12


				questionNewMeasure.put("443", "1751");
				questionNewMeasure.put("444", "1756");
				questionNewMeasure.put("445", "1761");
				questionNewMeasure.put("446", "1766");

				questionNewMeasure.put("1290", "1751");
				questionNewMeasure.put("1291", "1756");
				questionNewMeasure.put("1292", "1761");
				questionNewMeasure.put("1293", "1766");
			}
		}
		else{
			System.out.println("flag not found");
		}
		
		//in this case, algorithm will handle setting measure and submeasures status to computed
		// in the student level list
		
		BullyPopulation_student alg = new BullyPopulation_student(conn, dataStoreStudent);
		alg.compute(measureId, surveyInstanceId );
		
		
		

	}
	
	
	
	

}
