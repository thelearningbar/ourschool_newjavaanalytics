package visitables.student.bullying;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import algorithms.Bullying_student.BullyPop_QMeasure_Validate;
import algorithms.Bullying_student.QtoM_QFilter_school;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureArchitecture;
import utilities.MeasureRegister;
import visitables.IVisitable;
import visitorPickers.VisitorPickerFactory;


/**
 * 
 * See also
 * BullyPop_QMeasure_Validate
 * 
 * @author Evan K
 *
 */
public class Visitable_BullyPop_QMeasure_Validate implements IVisitable {


	private VisitorPickerFactory visitorPickerFactory ;
	private DataStore_student dataStoreStudent;
	private DataStore_school dataStoreSchool;
	private Connection conn;
	private boolean debugPrint = false;
	
	
	public Visitable_BullyPop_QMeasure_Validate( VisitorPickerFactory visitorPickerFactory ,  
			Connection conn , DataStore_student dataStoreStudent ,DataStore_school dataStoreSchool) {
		this.dataStoreSchool= dataStoreSchool;
		this.visitorPickerFactory=visitorPickerFactory;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
	}

	@Override
	public void handleRequest(String measureId, HashMap<String, String> surveyParameters  ) {
		
		if( debugPrint)	System.out.println(this.getClass().getName());
		
		
		MeasureArchitecture arch = MeasureArchitecture.getInstance();
		MeasureRegister measureRegister = MeasureRegister.getInstance();
		
		String surveyInstanceId = surveyParameters.get("surveyInstanceId");
		String pm = surveyParameters.get("parentMeasure");
		int parentMeasure = Integer.parseInt(pm);
		
		
		//compute parent measure  ie measure used for filtering
		List<Integer> computingMeasures = arch.getComputingMeasure(parentMeasure,  new ArrayList<Integer>());
		String parentMeasureComputingMeasure = Integer.toString( computingMeasures.get(0) );
		String parentComputingMeasureAlgorithmName =  measureRegister.getAlgSchoolLvl(parentMeasureComputingMeasure);
		if(  parentComputingMeasureAlgorithmName != null ){							
			visitorPickerFactory.getVisitorAlgo( parentComputingMeasureAlgorithmName ).chooseVisitor(parentMeasureComputingMeasure,  surveyParameters);
			dataStoreSchool.setMeasureComputed(parentMeasureComputingMeasure);
		}
				
		//Do algorithm		
		BullyPop_QMeasure_Validate alg = new BullyPop_QMeasure_Validate(conn, dataStoreStudent, dataStoreSchool);
		alg.compute(measureId, surveyInstanceId, pm, 1.5f);
		
		dataStoreStudent.setStudentLevelMeasureComputed(measureId);
		
	}
	
	
	

}
