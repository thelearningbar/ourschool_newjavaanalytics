package visitables.student.bullying;

import java.sql.Connection;
import java.util.HashMap;

import algorithms.Bullying_student.QtoM_QFilter_student;
import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitorPickers.VisitorPickerFactory;

public class V_QtoM_QFilter_student implements IVisitable {



	//private VisitorPickerFactory visitorPickerFactory;
	private DataStore_student dataStoreStudent;
	private DataStore_school dataStoreSchool;
	private Connection conn;

	public boolean debugPrint = false;
	
	
	public V_QtoM_QFilter_student(VisitorPickerFactory vpf, Connection conn, DataStore_student dataStoreStudent, DataStore_school dataStoreSchool) {

		//this.visitorPickerFactory = vpf;
		this.dataStoreSchool=dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
	}

	@Override
	public void handleRequest(String measureId, HashMap<String, String> surveyParameters) {
		if( debugPrint)	System.out.println(this.getClass().getName());		

		String surveyInstanceId = surveyParameters.get("surveyInstanceId");
		
		
		//TODO make dynamic
		int filterQuestionId = 3623;
		
		//student level, main measure has question to be used as filter
		QtoM_QFilter_student alg = new QtoM_QFilter_student(conn, dataStoreStudent, dataStoreSchool);
		alg.compute(measureId, surveyInstanceId, filterQuestionId);
		
	}


}
