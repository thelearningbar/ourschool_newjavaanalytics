package visitables.student.bullying;

import java.sql.Connection;
import java.util.HashMap;

import algorithms.Bullying_student.QtoM_NoFilter_student;
import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitorPickers.VisitorPickerFactory;


public class V_QtoM_NoFilter_student implements IVisitable {


	//private VisitorPickerFactory visitorPickerFactory ;
	private DataStore_student dataStoreStudent;
	private DataStore_school dataStoreSchool;
	private Connection conn;
	public boolean debugPrint = false;
	
	
	public V_QtoM_NoFilter_student(VisitorPickerFactory visitorPickerFactory ,  
			Connection conn , DataStore_student dataStoreStudent ,DataStore_school dataStoreSchool) {
		
		//this.visitorPickerFactory=visitorPickerFactory;
		this.dataStoreSchool= dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
	}

	
	
	@Override
	public void handleRequest(String measureId, HashMap<String, String> surveyParameters) {
		if( debugPrint)	System.out.println(this.getClass().getName());		

		String surveyInstanceId = surveyParameters.get("surveyInstanceId");
				
		//student level
		QtoM_NoFilter_student alg = new QtoM_NoFilter_student(conn, dataStoreStudent, dataStoreSchool);
		alg.compute(measureId, surveyInstanceId);
		
	}

}
