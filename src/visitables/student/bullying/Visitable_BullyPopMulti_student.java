package visitables.student.bullying;

import java.sql.Connection;
import java.util.HashMap;

import algorithms.Bullying_student.BullyPop_Ques_to_Measure_student;
import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitorPickers.VisitorPickerFactory;

public class Visitable_BullyPopMulti_student implements IVisitable {


	private DataStore_student dataStoreStudent;
	private Connection conn;
	

	//private boolean debugPrint = false;
	
	
	
	public Visitable_BullyPopMulti_student( VisitorPickerFactory visitorPickerFactory ,  
			Connection conn , DataStore_student dataStoreStudent ,DataStore_school dataStoreSchool) {
		
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
		
	}

	@Override
	public void handleRequest(String measureId, HashMap<String, String> surveyParameters) {


		String surveyInstanceId = surveyParameters.get("surveyInstanceId");
		
		BullyPop_Ques_to_Measure_student alg = new BullyPop_Ques_to_Measure_student(conn, dataStoreStudent);

		alg.compute(measureId, surveyInstanceId);
		
	}
	
	
	
	
	
	

}
