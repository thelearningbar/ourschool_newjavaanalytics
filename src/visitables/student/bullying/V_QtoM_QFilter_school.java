package visitables.student.bullying;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import algorithms.Bullying_student.QtoM_QFilter_school;
import algorithms.Bullying_student.QtoM_QFilter_student;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureRegister;
import utilities.Utils;
import visitables.IVisitable;
import visitorPickers.VisitorPickerFactory;


/**
 * 
 * See also
 * Quest_to_Measure_QFilter
 * 
 * 
 * @author Evan K
 *
 */
public class V_QtoM_QFilter_school implements IVisitable {

	private VisitorPickerFactory visitorPickerFactory ;
	private DataStore_student dataStoreStudent;
	private DataStore_school dataStoreSchool;
	private Connection conn;
	public boolean debugPrint = true;
	
	public V_QtoM_QFilter_school( VisitorPickerFactory visitorPickerFactory ,  
			Connection conn , DataStore_student dataStoreStudent ,DataStore_school dataStoreSchool) {
		
		this.visitorPickerFactory=visitorPickerFactory;
		this.dataStoreSchool= dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
	}

	@Override
	public void handleRequest(String measureId, HashMap<String, String> surveyParameters) {
		if( debugPrint)	System.out.println(this.getClass().getName());		

		String surveyInstanceId = surveyParameters.get("surveyInstanceId");
				
		
		//Check measure has already been computed at student lvl, 
		//Find it if not 
		if(  !dataStoreStudent.isComputed(measureId) ){
			
			if( debugPrint) Utils.pr("computing "+ measureId + " student lvl for: "+ measureId + " school lvl ");
			
			
			//compute measure for students
			visitorPickerFactory.getVisitorAlgo( MeasureRegister.getInstance().getAlgStudentLvl(measureId)).chooseVisitor(measureId,  surveyParameters);
			
			//set computed for students in list 
			dataStoreStudent.setStudentLevelMeasureComputed(measureId);
			
			
			
			if( debugPrint)	Utils.pr("");
		}
		
		
		
		QtoM_QFilter_school qq = new QtoM_QFilter_school(conn, dataStoreStudent, dataStoreSchool);
		qq.compute(measureId, surveyInstanceId);
		
		
		/*
		List<String> childmeasures = MeasureRegister.getInstance().getChildMeasuresSchoolLvl(measureId);
					
		//also save and compute child measures at school lvl
		if( childmeasures != null){
							
			for( String child : childmeasures ){
				
				//compute child measure if not already computed
				if( !dataStoreSchool.isComputed(child) ){
					
					if( debugPrint)	Utils.pr("compute "+ child + " for "+ measureId);
					
					
					visitorPickerFactory.getVisitorAlgo( MeasureRegister.getInstance().getAlgSchoolLvl(child)).chooseVisitor(child,  surveyParameters);
					dataStoreSchool.setMeasureComputed(child);
					
					
					if( debugPrint)	Utils.pr("");
				}
				else{
					if( debugPrint)	Utils.pr(">> child " +  child + " is already computed");
				}
			}
		}
		*/
		
		
		
	}
	
	
	
	
	
}
	
	
