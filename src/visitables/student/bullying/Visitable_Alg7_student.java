package visitables.student.bullying;

import java.sql.Connection;
import java.util.HashMap;

import algorithms.Bullying_student.Alg7_Default;
import utilities.DataStore_school;
import utilities.DataStore_student;
import visitables.IVisitable;
import visitorPickers.VisitorPickerFactory;

public class Visitable_Alg7_student implements IVisitable {

	private DataStore_student dataStoreStudent;
	private Connection conn;


	private boolean debugPrint = true;
	
	
	public Visitable_Alg7_student(    VisitorPickerFactory visitorPickerFactory ,  
			Connection conn , DataStore_student dataStoreStudent ,DataStore_school dataStoreSchool) {
		
		this.dataStoreStudent=dataStoreStudent;
		this.conn=conn;
		
	}

	
	
	@Override
	public void handleRequest(String measureId, HashMap<String, String> parameters) {
		if( debugPrint )
			System.out.println(this.getClass().getName() + " " + measureId);
		
		//TODO add check for already computed
		
		

		Alg7_Default alg = new Alg7_Default(conn, dataStoreStudent );
		alg.compute(measureId, parameters.get("surveyInstanceId")  );
				
		if( debugPrint ){
			System.out.println("before");
			dataStoreStudent.printStudents();
			System.out.println("====================");
		}
	}
	
	
		
	
	

}













