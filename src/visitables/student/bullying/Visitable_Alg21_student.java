package visitables.student.bullying;

import java.sql.Connection;
import java.util.HashMap;

import algorithms.Bullying_student.Alg21;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.Utils;
import visitables.IVisitable;
import visitorPickers.VisitorPickerFactory;

public class Visitable_Alg21_student implements IVisitable {


	private DataStore_student dataStoreStudent;
	private DataStore_school dataStoreSchool;
	private Connection conn;
	
	
	public boolean debugPrint = false;
	
	
	
	public Visitable_Alg21_student(VisitorPickerFactory visitorPickerFactory , 
			Connection conn , DataStore_student dataStoreStudent , DataStore_school dataStoreSchool) {
		
		this.dataStoreStudent=dataStoreStudent;
		this.dataStoreSchool =dataStoreSchool;
		this.conn=conn;
		
	}
	
	
	
	
	@Override
	public void handleRequest(String measureId, HashMap<String, String> surveyParameters) {

		if( debugPrint)
			System.out.println(this.getClass().getName() + " " + measureId);
		
		
		String surveyInstanceId = surveyParameters.get("surveyInstanceId");
		
		
		if(  !dataStoreSchool.isComputed(measureId)  ){
			
			double questionCutoff = 0.0;
			
			Alg21 alg = new Alg21(conn, dataStoreStudent, dataStoreSchool);
			alg.compute(   surveyInstanceId , measureId , questionCutoff  );
			
		}
		else{
			
			
			if( debugPrint ) 
				Utils.pr("> "+measureId + " is already computed\n");	
		}
		
		
	}

}
