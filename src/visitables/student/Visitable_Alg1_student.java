package visitables.student;
import java.sql.Connection;
import java.util.HashMap;

import algorithms.Alg1_student.Alg1Default;
import algorithms.Alg1_student.IAlg1;

import utilities.DataStore_student;
import utilities.ParameterRetriever;
import visitables.IVisitable;

public class Visitable_Alg1_student implements IVisitable{
	
	Connection conn;
	DataStore_student dataStoreStudent;
	
	public boolean debugPrint = false;
	
	
	public Visitable_Alg1_student( Connection conn , DataStore_student dataStoreStudent){
		this.conn=conn;
		this.dataStoreStudent = dataStoreStudent;
	}

	@Override
	public void handleRequest( String measureId, HashMap<String, String> parameters ) {
		
		if( debugPrint )
			System.out.println(this.getClass().getName() + " " + measureId);
		
		
		
		int measure = Integer.parseInt(measureId);
		int surveyInstance = Integer.parseInt( parameters.get("surveyInstanceId") );
		
		String country = parameters.get("country");
		String district = parameters.get("district");
		
		IAlg1 alg;
		
		//DEFAULT IMPLEMENTATION UNLESS SOME CONDITION MET
		alg = new Alg1Default(  conn, dataStoreStudent );

		
		//CUSTOMIZATION	EXAMPLE	
		int scale =-1;
				
		int scaleDefault =  Integer.parseInt(  ParameterRetriever.getProperty( "Alg1_student_scale" ));
		
		if( country!=null &&  country.equals("ca")){
				
				if( district!=null && district.equals("nb") ){
					
					String scaleNew = ParameterRetriever.getProperty("measure"+measureId+"_ca_nb_scale");
					
					if(scaleNew!=null)
						scale = Integer.parseInt( scaleNew);
		
				}
		}
		
		
		if( scale<0){
			scale = scaleDefault;
		}
		
		alg.computeResults( measure, surveyInstance, scale, null );
		
		dataStoreStudent.setStudentLevelMeasureComputed(measureId);
	}

	
	
}