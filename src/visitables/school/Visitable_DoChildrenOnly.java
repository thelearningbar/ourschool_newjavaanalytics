package visitables.school;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureRegister;
import utilities.Utils;
import visitables.IVisitable;
import visitorPickers.VisitorPickerFactory;



/**
 * A node which does not do any computation of its own, but runs child nodes at school and student level
 * 
 * @author Evan K
 *
 */
public class Visitable_DoChildrenOnly implements IVisitable {
	
	DataStore_student dataStoreStudent;
	DataStore_school dataStoreSchool;
	private VisitorPickerFactory visitorPicker;
	Connection conn;
	
	private boolean debugPrint = true;

	
	
	
	public Visitable_DoChildrenOnly( VisitorPickerFactory visitorPickerFactory ,  Connection conn ,
			DataStore_school dataStoreSchool , DataStore_student dataStoreStudent ) {
		
		this.visitorPicker = visitorPickerFactory;
		this.conn=conn;
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
	}

	@Override
	public void handleRequest(String measureId, HashMap<String, String> surveyParameters) {
		
		if( debugPrint){
			System.out.println(this.getClass().getName() );
		}

		//Check measure has been computed at school lvl
		if(  !dataStoreSchool.isComputed(measureId)){
			
			//do any child measures at student and school level
			computePrerequisites( measureId , surveyParameters);	
			
			//mark measure as computed at school level
			dataStoreSchool.setMeasureComputed(measureId);
		}

	}
	
	

	

	/**
	 * compute lower level measures at school lvl and student lvl
	 * @param measureId
	 * @param parameters
	 */
	private void computePrerequisites(String measureId,  HashMap<String, String> parameters){
				
		MeasureRegister measureRegister =  MeasureRegister.getInstance();

		//Check measure has already been computed at student lvl, 
		//Find it if not already computed
		if(  !dataStoreStudent.isComputed(measureId) ){
			
			if( debugPrint)
				Utils.pr("computing "+ measureId + " student lvl for: "+ measureId + " school lvl ");
			//--------------------------------------
			
			
			String name_alg_to_use =  measureRegister.getAlgStudentLvl(measureId);
			
			if( debugPrint){
				System.out.println("measure: " + measureId + "   algorithm student lvl:" + name_alg_to_use);
			}
			
			if(  name_alg_to_use != null ){

				//compute measure for students
				visitorPicker.getVisitorAlgo( name_alg_to_use ).chooseVisitor(measureId,  parameters);

				//set computed for students in list 
				dataStoreStudent.setStudentLevelMeasureComputed(measureId);

			}
			
			
			//------------------------------
			if( debugPrint)
				Utils.pr("");
		}
		
		
		
		
		List<String> childmeasures = measureRegister.getChildMeasuresSchoolLvl(measureId);
					
		//also save and compute child measures at school lvl
		if( childmeasures != null){
							
			for( String child : childmeasures ){
				
				//compute child measure if not already computed
				if( !dataStoreSchool.isComputed(child) ){
					
					if( debugPrint)
						Utils.pr("compute "+ child + " for "+ measureId);
					
					
					//----------------				
					
					String name_alg_to_use =  measureRegister.getAlgSchoolLvl(child);
					
					if( debugPrint){
						System.out.println("measure: " + measureId + "   algorithm school lvl:" + name_alg_to_use);
					}
					
					
					if(  name_alg_to_use != null ){							
						visitorPicker.getVisitorAlgo( name_alg_to_use ).chooseVisitor(child,  parameters);
						dataStoreSchool.setMeasureComputed(child);
					}
					
					
					
					//------------------
					
					
					if( debugPrint)
						Utils.pr("");
				}
				else{
					if( debugPrint)
						Utils.pr(">> child " +  child + " is already computed");
				}
			}
		}
	}


}
