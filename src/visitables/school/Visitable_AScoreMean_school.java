package visitables.school;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import algorithms.AScoreMean_school.Alg_AScoreMean;

import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureRegister;
import utilities.Utils;
import visitables.IVisitable;
import visitorPickers.VisitorPickerFactory;

public class Visitable_AScoreMean_school implements IVisitable {

	DataStore_student dataStoreStudent;
	DataStore_school dataStoreSchool;
	private VisitorPickerFactory visitorPicker;
	Connection conn;
	
	private boolean debugPrint = false;
	
	
	
	
	public Visitable_AScoreMean_school(  VisitorPickerFactory visitorPickerFactory ,  Connection conn ,
			DataStore_school dataStoreSchool , DataStore_student dataStoreStudent ) {
		
		this.visitorPicker = visitorPickerFactory;
		this.conn=conn;
		this.dataStoreSchool =  dataStoreSchool;
		this.dataStoreStudent=dataStoreStudent;
		
	}
	

	@Override
	public void handleRequest(String measureId , HashMap<String, String> parameters) {
		
		if( debugPrint )  
			System.out.println(this.getClass().getName() + " " + measureId);
		
		
		String surveyInstanceId = parameters.get("surveyInstanceId");
		
		Alg_AScoreMean alg;

		//DEFAULT IMPLEMENTATION UNLESS SOME CONDITION MET
		alg = new Alg_AScoreMean(  conn, dataStoreStudent, dataStoreSchool );
		

		//Check measure has been computed at school lvl
		if(  !dataStoreSchool.isComputed(measureId)){
			
			computePrerequisites( measureId , parameters);	
			
			alg.compute(measureId, surveyInstanceId  );
			

			//mark measure as computed at school level
			dataStoreSchool.setMeasureComputed(measureId);
			
			
		}
		else{
			if( debugPrint ) 
				Utils.pr("> "+measureId + " is already computed\n");
		}
		
		
		
	}
	
	
	

	/**
	 * compute lower level measures at school lvl and student lvl
	 * @param measureId
	 * @param parameters
	 */
	private void computePrerequisites(String measureId,  HashMap<String, String> parameters){

		//Check measure has already been computed at student lvl, 
		//Find it if not 
		if(  !dataStoreStudent.isComputed(measureId) ){
			
			if( debugPrint)
				Utils.pr("computing "+ measureId + " student lvl for: "+ measureId + " school lvl ");
			
			
			//compute measure for students
			visitorPicker.getVisitorAlgo( MeasureRegister.getInstance().getAlgStudentLvl(measureId)).chooseVisitor(measureId,  parameters);
			
			//set computed for students in list 
			dataStoreStudent.setStudentLevelMeasureComputed(measureId);
			
			
			if( debugPrint)
				Utils.pr("");
		}
		
		List<String> childmeasures = MeasureRegister.getInstance().getChildMeasuresSchoolLvl(measureId);
					
		//also save and compute child measures at school lvl
		if( childmeasures != null){
							
			for( String child : childmeasures ){
				
				//compute child measure if not already computed
				if( !dataStoreSchool.isComputed(child) ){
					
					if( debugPrint)
						Utils.pr("compute "+ child + " for "+ measureId);
					
					
					
					
					
					
					visitorPicker.getVisitorAlgo( MeasureRegister.getInstance().getAlgSchoolLvl(child)).chooseVisitor(child,  parameters);
					dataStoreSchool.setMeasureComputed(child);
					
					
					
					
					
					if( debugPrint)
						Utils.pr("");
				}
				else{
					if( debugPrint)
						Utils.pr(">> child " +  child + " is already computed");
				}
			}
		}
	}

}

























