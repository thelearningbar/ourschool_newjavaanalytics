package visitables.school.cutoff;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import algorithms.ACutoff_school.Alg_ACutoff;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureRegister;
import utilities.Utils;
import visitables.IVisitable;
import visitorPickers.VisitorPickerFactory;

public class Visitable_ACutoff_school implements IVisitable {

	private DataStore_student dataStoreStudent;
	private DataStore_school dataStoreSchool;
	private VisitorPickerFactory visitorPicker;
	private Connection conn;
	private boolean debugPrint = false;
	
	
	
	public Visitable_ACutoff_school( VisitorPickerFactory visitorPickerFactory , 
			Connection conn , DataStore_student dataStoreStudent , DataStore_school dataStoreSchool) {
		
		this.visitorPicker = visitorPickerFactory;
		this.dataStoreStudent=dataStoreStudent;
		this.dataStoreSchool=dataStoreSchool;
		this.conn=conn;
		
	}

	
	@Override
	public void handleRequest(String measureId,  HashMap<String, String> surveyParameters) {
		if( debugPrint)
			System.out.println(this.getClass().getName() + " " + measureId);
		
		
		String surveyInstanceId = surveyParameters.get("surveyInstanceId");
		
		Alg_ACutoff alg;
		
		//DEFAULT IMPLEMENTATION UNLESS SOME CONDITION MET
		alg = new Alg_ACutoff(  conn, dataStoreStudent, dataStoreSchool );
		
		//find prerequisites even if already have the requested measure
		computePrerequisites( measureId , surveyParameters);
		
		
		//Check if measure has already been computed at school lvl
		if(  !dataStoreSchool.isComputed(measureId)  ){
			
			
			//computePrerequisites( measureId , parameters);		
										
			alg.compute(measureId, surveyInstanceId  );
									
			//set computed for school
			dataStoreSchool.setMeasureComputed(measureId);
			
		}
		else{
			if( debugPrint ) 
				Utils.pr("> "+measureId + " is already computed\n");	
		}
	}
	
	
	/**
	 * compute lower level measures at school lvl and student lvl
	 * @param measureId
	 * @param parameters
	 */
	private void computePrerequisites(String measureId,  HashMap<String, String> parameters){

		MeasureRegister measureRegister = MeasureRegister.getInstance();
		
		
		//Check measure has already been computed at student lvl, 
		//Find it if not 
		if(  !dataStoreStudent.isComputed(measureId) ){
			
			//name of algorithm to use with this child measure
			String nameAlg =  measureRegister.getAlgStudentLvl(measureId); 
			

			if( debugPrint)
				Utils.pr("computing "+ measureId + " student lvl for: "+ measureId + " school lvl with alg:" + nameAlg );
			
				
			if( nameAlg!= null ){
				//compute measure for students
				visitorPicker.getVisitorAlgo(nameAlg).chooseVisitor(measureId,  parameters);
			}
			
			
			//set computed for students
			dataStoreStudent.setStudentLevelMeasureComputed(measureId);
			
			
			
			if( debugPrint)
				Utils.pr("");
		}
		
		
		List<String> childmeasures = measureRegister.getChildMeasuresSchoolLvl(measureId);
					
		//also save and compute child measures at school lvl
		if( childmeasures != null){
							
			for( String child : childmeasures ){
				
				//compute child measure if not already computed
				if( !dataStoreSchool.isComputed(child) ){
					
					
					
					//name of algorithm to use with this child measure
					String nameAlg =  measureRegister.getAlgStudentLvl(measureId); 
					
					if( debugPrint)
						Utils.pr("computing "+ measureId + " school lvl for: "+ measureId + " school lvl with alg:" + nameAlg );
					
					visitorPicker.getVisitorAlgo( nameAlg).chooseVisitor(child,  parameters);
					dataStoreSchool.setMeasureComputed(child);
					
					
					
					if( debugPrint)
						Utils.pr("");
				}
				else{
					if( debugPrint)
						Utils.pr(">> child " +  child + " is already computed");
				}
			}
			
		}
		
		
		
	}
	
	


}
