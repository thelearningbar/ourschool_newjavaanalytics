package visitables.school.cutoff;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import algorithms.ACutoff_school.Alg_ACutoffDouble_Default;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureRegister;
import utilities.Utils;
import visitables.IVisitable;
import visitorPickers.VisitorPickerFactory;

public class Visitable_ACutoffDouble_school implements IVisitable {

	private VisitorPickerFactory visitorPicker;
	private DataStore_student dataStoreStudent;
	private DataStore_school dataStoreSchool;
	private Connection conn;
	
	
	public boolean debugPrint = true;
	
	
	public Visitable_ACutoffDouble_school( VisitorPickerFactory visitorPickerFactory , 
			Connection conn , DataStore_student dataStoreStudent , DataStore_school dataStoreSchool) {
		
		this.visitorPicker  = visitorPickerFactory;
		this.dataStoreStudent=dataStoreStudent;
		this.dataStoreSchool =dataStoreSchool;
		this.conn=conn;
		
	}

	
	/**
	 * 
	 * Customized cutoff based on schooltype
	 * 
	 */
	@Override
	public void handleRequest(String measureId, HashMap<String, String> surveyParameters) {
				
		if( debugPrint){
			System.out.println(this.getClass().getName() + " " + measureId);
		}
		
		String surveyInstanceId = surveyParameters.get("surveyInstanceId");
		
		
		Alg_ACutoffDouble_Default alg = new Alg_ACutoffDouble_Default(  conn, dataStoreStudent, dataStoreSchool );
		
				
		//Check if measure has already been computed at school lvl
		if(  !dataStoreSchool.isComputed(measureId)  ){
			//it was not checked off as computed, therefore must find it
			
			
			Double cutoff0 = 0.0;
			Double cutoff1 = 0.0;
			
			//customize cutoff based on Elementary/Secondary school
			if( surveyParameters.containsKey("schooltype") ){
				String type = surveyParameters.get("schooltype");
				
				if( debugPrint)  System.out.println( "schooltype: " + type );
				
				
				if( type.equals("4")){
					//BullyCategory Basic
					cutoff0 = 1.5;
					cutoff1 = 3.0;	//TODO maybe should be 2.5 ?			
				}
				else{
					//BullyCategory Extended
					cutoff0 = 0.9;
					cutoff1 = 2.0;	
				}
			}
			
			computePrerequisites( measureId , surveyParameters);
			
			alg.compute(  measureId , surveyInstanceId , cutoff0 , cutoff1  );
			
		}
		else{
			if( debugPrint ) 
				Utils.pr("> "+measureId + " is already computed\n");	
		}
		
	}
	

	/**
	 * compute lower level measures at school lvl and student lvl
	 * @param measureId
	 * @param parameters
	 */
	private void computePrerequisites(String measureId,  HashMap<String, String> parameters){

		//Check measure has already been computed at student lvl, 
		//Find it if not 
		if(  !dataStoreStudent.isComputed(measureId) ){
			
			if( debugPrint)
				Utils.pr("computing "+ measureId + " student lvl for: "+ measureId + " school lvl ");
			
			
			String algorithmName = MeasureRegister.getInstance().getAlgStudentLvl(measureId);
			
			if( debugPrint) System.out.println("algorithmName "+algorithmName);
			
			//compute measure for students
			visitorPicker.getVisitorAlgo( algorithmName ).chooseVisitor(measureId,  parameters);
			
			//set as computed for students
			dataStoreStudent.setStudentLevelMeasureComputed(measureId);
			
			
			
			
			if( debugPrint)
				Utils.pr("");
			
		}
		
		List<String> childmeasures = MeasureRegister.getInstance().getChildMeasuresSchoolLvl(measureId);
		
		
		//also save and compute child measures at school lvl
		if( childmeasures != null){
							
			for( String child : childmeasures ){
				
				//compute child measure if not already computed
				if( !dataStoreSchool.isComputed(child) ){
					
					if( debugPrint)
						Utils.pr("compute "+ child + " for "+ measureId);
					
					visitorPicker.getVisitorAlgo( MeasureRegister.getInstance().getAlgSchoolLvl(child)).chooseVisitor(child,  parameters);
					dataStoreSchool.setMeasureComputed(child);
										
					if( debugPrint)
						Utils.pr("");
				}
				else{
					if( debugPrint)
						Utils.pr(">> child " +  child + " is already computed");
				}
			}
			
		}
		
		
		
	}

}
