package visitables;
import java.util.HashMap;

public interface IVisitable {


	void handleRequest(String measureId , HashMap<String, String> surveyParameters);

}
