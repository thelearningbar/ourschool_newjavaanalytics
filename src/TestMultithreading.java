import java.util.ArrayList;
import java.util.List;

import utilities.MeasureInfo;

public class TestMultithreading {

	

	//school level measure Ids to compute for each survey instance
	public String[] measuresToDo = {"1214", "1022" , "1023" , "1049" , "1029" , "1521"};
	
	//survey instances to compute,  each measure is computed for each survey instance
	public int[] surveyIdsToDo = {
			72025,72041,72091,72099,72104,72115,72123,72137,72145,72148,
			72165,72167,72178,72187,72239,72249,72252,72269,72277,72282,
			72283,72333,72339,72346,72385,72401,72450,72452,72463,72467,
			72473,72495,72498,72503,72509,72515,72517,72518,72524,72533,
			72573,72609,72655,72660,72671,72673,72692,72693,72748,72753,
			72832,72837,72851,72856,72891,72901,72921,72926,72962,72967,
			72983,72984,73050,73084,73175,73246,73247,73254,73313,73342,
			73351,73370,73393,73403,73429,73434,73437,73453,73456,73458,
			73460,73492,73506,73509,73511,73543,73571,73574,73580,73581,
			73597,73600,73601,73614,73620,73623,73624,73638,73641,73643,
			73649,73651,73654,73660,73675,73682,73685,73687,73688,73690,
			73691,73694,73698,73699,73700,73707,73714,73717,73736,73740,
			73747,73774,73791,73793,73804,73809,73811,73818,73824,73830,
			73834,73841,73866,73868,73870,73871,73873,73875,73877,73881,
			73882,73893,73907,73910,73916,73917,73923,73925,73932,73933,
			73952,73959,73965,73988,73994,74007,74013,74014,74016,74023,
			74025,74033,74034,74038,74045,74046,74056,74065,74066,74068,
			74069,74075,74080,74095,74112,74119,74128,74132,74133,74139,
			74146,74149,74150,74151,74154,74155,74156,74158,74160,74162,
			74163,74167,74168,74175,74180,74185,74187,74190,74203,74223};
	
	//,77297,77253, 77300, 77320 , 77249,77206, 77944, 77138
	
	//,77297,77253, 77064, 77084 , 77249,77206, 77944, 77138, 77204
	
	
	public TestMultithreading() {
		
		MeasureInfo.getInstance();
		
		List<Thread> threads = new ArrayList<Thread>();
		
		
		long startTime = System.currentTimeMillis();
		
		//CREATE AND START THREADS
		for(int y=0; y<surveyIdsToDo.length; y++){
			SurveyInstanceComputation comp = new SurveyInstanceComputation( surveyIdsToDo[y] , measuresToDo );
			
			Thread thread1 = new Thread(comp);
			
			threads.add(thread1);
			
			thread1.start();
			
		}
		
		// and now wait for all of them to complete
		for (Thread t : threads) {
		    try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		

		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println("Total execution time was: " + totalTime/1000 + " seconds");
		
		
		System.out.println("\n---END----------");
		
	}
	
	
	

}
