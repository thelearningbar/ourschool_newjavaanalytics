import java.util.HashMap;

import utilities.DBconnection;
import utilities.DataSaver;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureInfo;
import utilities.MeasureRegister;
import utilities.ParticipantCharacteristicRetriever;
import utilities.SurveyInfoFinder;
import utilities.TempTableCreator;
import utilities.Utils;
import visitorPickers.VisitorPickerFactory;


/**
 * test multiple survey instances in seriessingle thread
 * 
 * @author Evan K
 *
 */
public class Test6 {
	
	/*
	//school level measure Ids to compute for each survey instance
	public String[] measuresToDo = {"1214", "1022" , "1023" , "1049" , "1029" , "1521"};
	*/
	
	
	
	
	/*
	//survey instances to compute,  each measure is computed for each survey instance
	public int[] surveyIdsToDo = {77300 ,77297,77253, 77064, 77084 , 77249,77206, 77944, 77138, 77204  };
	*/
	
	public boolean saveToDB = true;
	
	public String[] measuresToDo = {"12"};
	public int[] surveyIdsToDo = {74984};
	
	//bullying instances 75736 , 74984
	
	//bullying measures  12, 1688,1689
	
	/*
	public int[] surveyIdsToDo = {
			72025,72041,72091,72099,72104,72115,72123,72137,72145,72148,
			72165,72167,72178,72187,72239,72249,72252,72269,72277,72282,
			72283,72333,72339,72346,72385,72401,72450,72452,72463,72467,
			72473,72495,72498,72503,72509,72515,72517,72518,72524,72533,
			72573,72609,72655,72660,72671,72673,72692,72693,72748,72753,
			72832,72837,72851,72856,72891,72901,72921,72926,72962,72967,
			72983,72984,73050,73084,73175,73246,73247,73254,73313,73342,
			73351,73370,73393,73403,73429,73434,73437,73453,73456,73458,
			73460,73492,73506,73509,73511,73543,73571,73574,73580,73581,
			73597,73600,73601,73614,73620,73623,73624,73638,73641,73643,
			73649,73651,73654,73660,73675,73682,73685,73687,73688,73690,
			73691,73694,73698,73699,73700,73707,73714,73717,73736,73740,
			73747,73774,73791,73793,73804,73809,73811,73818,73824,73830,
			73834,73841,73866,73868,73870,73871,73873,73875,73877,73881,
			73882,73893,73907,73910,73916,73917,73923,73925,73932,73933,
			73952,73959,73965,73988,73994,74007,74013,74014,74016,74023,
			74025,74033,74034,74038,74045,74046,74056,74065,74066,74068,
			74069,74075,74080,74095,74112,74119,74128,74132,74133,74139,
			74146,74149,74150,74151,74154,74155,74156,74158,74160,74162,
			74163,74167,74168,74175,74180,74185,74187,74190,74203,74223};
	*/
	
	//
	//77300 ,77297,77253, 77064, 77084 , 77249,77206, 77944, 77138, 77204
	

	public Test6() {
		
		//init measureInfo
		MeasureInfo.getInstance();
					
		//start timer
		long startTime = System.currentTimeMillis();
		
		
		//perform computation
		testInstances();
		
		
		
		//print elapsed time to complete
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println("Total execution time was: " + totalTime/1000 + " seconds");
		
		
		
		
		Utils.pr("\n----END---------");
		
	}
	
	
	/**
	 * loop through all instances, find results for each
	 */
	public void testInstances(){
		for(int y=0; y<surveyIdsToDo.length; y++){
			doMeasuresForInstance( surveyIdsToDo[y] , measuresToDo );
		}
	}
	
	
	/**
	 * Find measure results for a certain survey instance
	 * @param surveyInstanceId id of srvey to find results for
	 * @param measuresToDo measure ids to find for this surveuy instance
	 */
	public void doMeasuresForInstance( int surveyInstanceId , String[] measuresToDo){
				
		DataStore_school dataStoreSchool = new DataStore_school();
		
		DataStore_student dataStoreStudent = new DataStore_student();;
		
		
		DBconnection db = new DBconnection();
		
		TempTableCreator tempTableCreator = new TempTableCreator(db.getConnection());
		tempTableCreator.createTempAnswerTable( Integer.toString( surveyInstanceId ) );
		
		
		//Get participant characteristics
		ParticipantCharacteristicRetriever characteristics = 
				new ParticipantCharacteristicRetriever( db.getConnection(), dataStoreStudent );
		
		characteristics.getAllParticipantInfo( Integer.toString( surveyInstanceId ) );
		
		
		
		//construct input
		HashMap<String,String> Inputmap = new HashMap<String,String>();
		Inputmap.put("surveyInstanceId", Integer.toString( surveyInstanceId ) );
				
		
		for(int x=0; x<measuresToDo.length; x++){
			
			String measureId = measuresToDo[x];
			
			//DO computations------
			//Utils.pr("\nSchool lvl");
			VisitorPickerFactory vpf = new VisitorPickerFactory( db.getConnection(), dataStoreStudent, dataStoreSchool );
			vpf.getVisitorAlgo( MeasureRegister.getInstance().getAlgSchoolLvl( measureId )).chooseVisitor( measureId , Inputmap);
			//-------------------
			
		}
				

		//----------------------------------------------------------------------------------
		Utils.pr("\n---SAVE DATA---- ");
		
		//find  ouId
		SurveyInfoFinder find  = new SurveyInfoFinder( db.getConnection() );
		String org = find.getOuId( Integer.toString(surveyInstanceId) );
		
		DataSaver saver = new DataSaver( db.getConnection(), dataStoreStudent, dataStoreSchool );	
		
		//Change this line to choose what to write(or overwrite) to DB
		//										    StudentLevelScores , CharacteristicData (school lvl)
		saver.saveData( Integer.toString(surveyInstanceId) ,org, saveToDB , saveToDB );
		//----------------------------------------------------------------------------------
		
		
		tempTableCreator.dropTempAnswerTable();
		
		
		//dataStoreStudent.printStudents();
		
		dataStoreStudent.clearAllData();
		dataStoreSchool.clearAllData();
		
		db.close();
		
	}
	
	
	
  
}



