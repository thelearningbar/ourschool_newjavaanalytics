

import java.util.HashMap;

import utilities.DBconnection;
import utilities.DataSaver;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureInfo;
import utilities.MeasureRegister;
import utilities.ParticipantCharacteristicRetriever;
import utilities.SurveyInfoFinder;
import utilities.TempTableCreator;
import utilities.Utils;
import visitorPickers.VisitorPickerFactory;

public class Test7 {

	public boolean saveToDB=true;
	
	
	/*
	 * test 1 survey instance
	 */
	public Test7() {
		
		//76318 1413,1420
		//76418 1420
		//77663 1420 1411 1412
		//76675 1420 1411 1412
		
		int surveyInstanceId = 76675;
		String measureId    =  "1412";
		
		HashMap<String,String> inputmap = new HashMap<String,String>();
		inputmap.put( "surveyInstanceId",  Integer.toString( surveyInstanceId ) );	
		inputmap.put( "parentMeasure", "1690" );
		
		
		runOneSurveyInstance(  measureId , surveyInstanceId , inputmap  );
		
		
		System.out.println("END");
	}
	
	
	
	
	public void runOneSurveyInstance( String measureId , int surveyInstanceId , HashMap<String,String> inputMap ){
		
		DBconnection db= new DBconnection();

		SurveyInfoFinder sifind  = new SurveyInfoFinder( db.getConnection() );
		String schooltype = sifind.getSchoolType( Integer.toString( surveyInstanceId ) );
		
		
		System.out.println("schooltype " + schooltype);
		inputMap.put( "schooltype", schooltype);
		
		
		
		@SuppressWarnings("unused")
		MeasureInfo minfo = MeasureInfo.getInstance();

		TempTableCreator tempTableCreator = new TempTableCreator(db.getConnection());
		tempTableCreator.createTempAnswerTable(   Integer.toString( surveyInstanceId )  );


		DataStore_school dataStoreSchool = new DataStore_school();
		DataStore_student dataStoreStudent = new DataStore_student();;


		//construct input
		HashMap<String,String> Inputmap = inputMap;

		//Get participant characteristics
		ParticipantCharacteristicRetriever characteristics = new ParticipantCharacteristicRetriever( db.getConnection(), dataStoreStudent );
		characteristics.getAllParticipantInfo( Integer.toString( surveyInstanceId ) );


		String alg = MeasureRegister.getInstance().getAlgSchoolLvl( measureId );
		System.out.println("alg to use: "  + alg);


		// FIND RESULTS--------
		VisitorPickerFactory vpf = new VisitorPickerFactory( db.getConnection(), dataStoreStudent, dataStoreSchool );
		vpf.getVisitorAlgo(alg).chooseVisitor( measureId , Inputmap);
		//----------------------
		
		
		dataStoreSchool.printCharacteristicResults();
		System.out.println("");
		dataStoreStudent.printStudents2();
		
		
		
		
		//----------------------------------------------------------------------------------
		Utils.pr("\n---SAVE DATA---- ");

		//find  ouId
		String org = sifind.getOuId( Integer.toString(surveyInstanceId) );

		DataSaver saver = new DataSaver( db.getConnection(), dataStoreStudent, dataStoreSchool );	

		//Change this line to choose what to write(or overwrite) to DB
		//										    StudentLevelScores , CharacteristicData (school lvl)
		saver.saveData( Integer.toString(surveyInstanceId) ,org, saveToDB , saveToDB );
		//----------------------------------------------------------------------------------

		tempTableCreator.dropTempAnswerTable();
		dataStoreStudent.clearAllData();
		dataStoreSchool.clearAllData();

		db.close();

	}
	
	
	
	


}




/*
 * 

		DBconnection db= new DBconnection();

		TempTableCreator tempTableCreator = new TempTableCreator(db.getConnection());
		tempTableCreator.createTempAnswerTable(   "74984"  );

		DataStore_school dataStoreSchool = new DataStore_school();
		DataStore_student dataStoreStudent = new DataStore_student();;

		String measureId =  "12";
		int surveyInstanceId = 74984;

		//construct input
		HashMap<String,String> Inputmap = new HashMap<String,String>();
		Inputmap.put("surveyInstanceId", Integer.toString( surveyInstanceId ) );


		String alg = MeasureRegister.getInstance().getAlgSchoolLvl( measureId );
		System.out.println("alg to use: "  + alg);

		VisitorPickerFactory vpf = new VisitorPickerFactory( db.getConnection(), dataStoreStudent, dataStoreSchool );
		vpf.getVisitorAlgo(alg).chooseVisitor( measureId , Inputmap);



		//Alg7_Default alg = new Alg7_Default( db.getConnection() ,  dataStoreStudent);
		//alg.compute("12", "74984");

		dataStoreStudent.printStudents();
 * */
