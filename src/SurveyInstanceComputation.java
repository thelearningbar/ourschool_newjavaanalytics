import java.util.HashMap;

import utilities.DBconnection;
import utilities.DataSaver;
import utilities.DataStore_school;
import utilities.DataStore_student;
import utilities.MeasureRegister;
import utilities.ParticipantCharacteristicRetriever;
import utilities.SurveyInfoFinder;
import utilities.TempTableCreator;
import visitorPickers.VisitorPickerFactory;

public class SurveyInstanceComputation implements Runnable {

	private String[] measuresToDo;
	private int surveyInstanceId;
	
	private Thread t;
	private String threadName;
	
	public SurveyInstanceComputation( int surveyInstanceId , String[] measuresToDo ) {

		this.surveyInstanceId = surveyInstanceId;
		this.measuresToDo=measuresToDo;
		
		this.threadName = "surveyInstanceId"+Integer.toString(surveyInstanceId);
		
	}

	@Override
	public void run() {

		doMeasuresForInstance( surveyInstanceId , measuresToDo );

	}

	public void start(){
		System.out.println("Starting thread for surveyInstanceId " +  surveyInstanceId );
		if (t == null) {
			t = new Thread (this, threadName);
			t.start ();
		}
		
		System.out.println("Thread " + threadName + " exiting");
		
	}



	
	
	public void doMeasuresForInstance( int surveyInstanceId , String[] measuresToDo){
		
		DataStore_school dataStoreSchool = new DataStore_school();
		
		DataStore_student dataStoreStudent = new DataStore_student();;
		
		
		DBconnection db = new DBconnection();
		
		TempTableCreator tempTableCreator = new TempTableCreator(db.getConnection());
		tempTableCreator.createTempAnswerTable( Integer.toString( surveyInstanceId ) );
		
		
		//Get participant characteristics
		ParticipantCharacteristicRetriever characteristics = new ParticipantCharacteristicRetriever( db.getConnection(), dataStoreStudent );
		characteristics.getAllParticipantInfo( Integer.toString( surveyInstanceId ) );
		
		//construct input
		HashMap<String,String> Inputmap = new HashMap<String,String>();
		Inputmap.put("surveyInstanceId", Integer.toString( surveyInstanceId ) );
				
		
		for(int x=0; x<measuresToDo.length; x++){
			
			String measureId = measuresToDo[x];
			
			//DO computations------
			//Utils.pr("\nSchool lvl");
			VisitorPickerFactory vpf = new VisitorPickerFactory( db.getConnection(), dataStoreStudent, dataStoreSchool );
			vpf.getVisitorAlgo( MeasureRegister.getInstance().getAlgSchoolLvl( measureId )).chooseVisitor( measureId , Inputmap);
			//-------------------
			
		}
				

		//----------------------------------------------------------------------------------
		//Utils.pr("\n---SAVE DATA---- ");
		
		//find  ouId
		SurveyInfoFinder find  = new SurveyInfoFinder( db.getConnection() );
		String org = find.getOuId( Integer.toString(surveyInstanceId) );
		
		DataSaver saver = new DataSaver( db.getConnection(), dataStoreStudent, dataStoreSchool );	
		
		//Change this line to choose what to write(or overwrite) to DB
		//										    StudentLevelScores , CharacteristicData (school lvl)
		saver.saveData( Integer.toString(surveyInstanceId) ,org, true , true );
		//----------------------------------------------------------------------------------
		
		
		tempTableCreator.dropTempAnswerTable();
		
		
		dataStoreStudent.clearAllData();
		dataStoreSchool.clearAllData();
		
		db.close();
		
	}
	
	

}
